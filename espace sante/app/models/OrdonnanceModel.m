//
//  OrdonnanceModel.m
//  espace sante
//
//  Created by chbab Mohamed on 26/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "OrdonnanceModel.h"

@implementation OrdonnanceModel

- (CGFloat)prixTotal
{
    CGFloat total = 0;
    
    for (MedicamentModel *medicament in self.medicaments) {
        total += ([medicament.prix floatValue] * [medicament.qte integerValue]);
    }
    
    return total;
}

- (NSString *)name
{
    if(_name == nil) {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setLocale:[NSLocale localeWithLocaleIdentifier:@"fr_FR"]];
        [format setDateFormat:@"dd-MM-yyyy HH:mm"];
        
        NSDate *now = [[NSDate alloc] init];
        NSString *date = [format stringFromDate:now];
        
        return [@"Ordonnance " stringByAppendingString:date];
    }
    
    return _name;
}

- (void)addMedicaments:(MedicamentModel *)obj
{
    if(self.medicaments.count == 0 || self.medicaments == nil)
        self.medicaments = [NSMutableArray array];
    
    [self.medicaments addObject:obj];
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.medicaments forKey:@"medicaments"];
    [encoder encodeFloat:self.prixTotal forKey:@"prixTotal"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super init]){
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.medicaments = [aDecoder decodeObjectForKey:@"medicaments"];
        self.prixTotal = [aDecoder decodeFloatForKey:@"prixTotal"];
    }
    return self;
}

@end
