//
//  MedicamentModel.h
//  espace sante
//
//  Created by chbab Mohamed on 26/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MedicamentModel : NSObject

@property (nonatomic,strong) NSString *nom;
@property (nonatomic,strong) NSString *presentation;
@property (nonatomic,strong) NSString *dosage;
@property (nonatomic,strong) NSNumber *qte;
@property (nonatomic,strong) NSNumber *prix;

- (id)copyWithZone:(MedicamentModel *)obj;

@end
