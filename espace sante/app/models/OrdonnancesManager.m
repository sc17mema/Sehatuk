//
//  OrdonnancesManager.m
//  espace sante
//
//  Created by chbab Mohamed on 26/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "OrdonnancesManager.h"

static OrdonnancesManager *sharedOrdonnance = nil;

@implementation OrdonnancesManager

+ (OrdonnancesManager *)sharedSingleton
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ordonnances.data"];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        if ([NSKeyedUnarchiver unarchiveObjectWithFile:path] == nil)
            sharedOrdonnance = [[self alloc] init];
        else
            sharedOrdonnance = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    });
    
    return sharedOrdonnance;
}

- (void)saveOrdonnances
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ordonnances.data"];
    [NSKeyedArchiver archiveRootObject:self toFile:path];
    
    //NSLog(@"ORDOS :: %d",_ordonnances.count);
}

- (void)addOrdonnance:(OrdonnanceModel *)ordonnance {
    if (self.ordonnances == nil) {
        self.ordonnances = [[NSMutableArray alloc] init];
    }
    
    [self.ordonnances addObject:ordonnance];
    NSLog(@"ORDOS :: %d ",self.ordonnances.count);
    [self saveOrdonnances];
}

- (void)removeOrdonnance:(NSInteger)index{
    [self.ordonnances removeObjectAtIndex:index];
    [self saveOrdonnances];
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.ordonnances forKey:@"ordonnances"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super init]){
        self.ordonnances = [aDecoder decodeObjectForKey:@"ordonnances"];
    }
    return self;
}

@end
