#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "WS.h"
#import "Cache.h"
#import "File.h"
#import "WSDrugs.h"

@interface DownloadEngine : NSObject

@property (atomic, readonly) BOOL syncInProgress;

+ (DownloadEngine *)sharedEngine;
- (void)start;

@end
