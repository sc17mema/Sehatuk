//
//  MedicamentModel.m
//  espace sante
//
//  Created by chbab Mohamed on 26/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "MedicamentModel.h"

@implementation MedicamentModel

- (id)copyWithZone:(MedicamentModel *)obj
{
    MedicamentModel *m = [[MedicamentModel alloc] init];
    m.nom = obj.nom;
    m.prix = obj.prix;
    m.qte = obj.qte;
    m.presentation = obj.presentation;
    m.dosage = obj.dosage;
    
    return m;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.nom forKey:@"nom"];
    [encoder encodeObject:self.prix forKey:@"prix"];
    [encoder encodeObject:self.qte forKey:@"qte"];
    [encoder encodeObject:self.presentation forKey:@"presentation"];
    [encoder encodeObject:self.dosage forKey:@"dosage"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super init]){
        self.nom = [aDecoder decodeObjectForKey:@"nom"];
        self.prix = [aDecoder decodeObjectForKey:@"prix"];
        self.qte = [aDecoder decodeObjectForKey:@"qte"];
        self.presentation = [aDecoder decodeObjectForKey:@"presentation"];
        self.dosage = [aDecoder decodeObjectForKey:@"dosage"];
    }
    return self;
}

@end
