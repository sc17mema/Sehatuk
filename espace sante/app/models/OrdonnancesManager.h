//
//  OrdonnancesManager.h
//  espace sante
//
//  Created by chbab Mohamed on 26/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OrdonnanceModel.h"

#import "Cache.h"
#import "File.h"

@interface OrdonnancesManager : NSObject<NSCoding>

@property (nonatomic,strong) NSMutableArray *ordonnances;

+ (OrdonnancesManager *)sharedSingleton;
- (void)addOrdonnance:(OrdonnanceModel *) ordonnance;
- (void)removeOrdonnance:(NSInteger)index;
- (void)saveOrdonnances;

@end


