//
//  OrdonnanceModel.h
//  espace sante
//
//  Created by chbab Mohamed on 26/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedicamentModel.h"

@interface OrdonnanceModel : NSObject<NSCoding>

@property (nonatomic,assign) CGFloat prixTotal;
@property (nonatomic,strong) NSMutableArray *medicaments;
@property (nonatomic,strong) NSString *name;

- (void)addMedicaments:(MedicamentModel *)obj;

@end
