#import "DownloadEngine.h"

@implementation DownloadEngine

@synthesize syncInProgress = _syncInProgress;

#pragma mark -
#pragma mark singelton

+ (DownloadEngine *)sharedEngine {
    static DownloadEngine *sharedEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedEngine = [[DownloadEngine alloc] init];
    });
    
    return sharedEngine;
}

- (void)start {
    if (!self.syncInProgress) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            if([Cache get:@"anam.json"] == nil && [Cache get:@"doctissimo.json"] == nil) {
                
                [self willChangeValueForKey:@"syncInProgress"];
                _syncInProgress = YES;
                [self didChangeValueForKey:@"syncInProgress"];
                
                NSLog(@"Starting...");
                [self monitorDownloadProgress];
            }
        });
    }
}

- (void)executeSyncCompletedOperations {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EngineCompleted" object:nil];
        
        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = NO;
        [self didChangeValueForKey:@"syncInProgress"];
    });
}

- (void)monitorDownloadProgress
{
    
    NSArray* filesToDownload = @[@"anam.json", @"doctissimo.json"];
    NSMutableArray *operations = [NSMutableArray array];
    
    for (NSString *file in filesToDownload)
    {
        NSMutableURLRequest *q = [[WSDrugs api] requestWithMethod:@"GET" path:file parameters:nil];
        
        AFHTTPRequestOperation *operation = [[WSDrugs api] HTTPRequestOperationWithRequest:q success:^(AFHTTPRequestOperation *operation, id responseObject)
                                             {
                                                 if([Cache get:file] == nil) {
                                                     [Cache setObject:responseObject forKey:file withExpires:86400];
                                                 }
                                                 
                                             } failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                             {
                                                 NSLog(@"Request for class %@ failed %@", file, error.description);
                                                 [File deleteFile:file];
                                             }];
        
        
        [operation setCacheResponseBlock:^NSCachedURLResponse *(NSURLConnection *connection, NSCachedURLResponse *cachedResponse) {
            return nil;
        }];
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        }];
        
        [operations addObject:operation];
    }
    
    [[WSDrugs api] enqueueBatchOfHTTPRequestOperations:operations progressBlock:^(NSUInteger numberOfCompletedOperations, NSUInteger totalNumberOfOperations){
        NSLog(@"completed operation %d", numberOfCompletedOperations);
    } completionBlock:^(NSArray *operations) {
        [self executeSyncCompletedOperations];
    }];
}

@end
