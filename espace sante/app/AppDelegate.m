//
//  AppDelegate.m
//  espace sante
//
//  Created by abdel ali on 23/09/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

+ (void)initialize
{
    // Configure iRate
    [iRate sharedInstance].daysUntilPrompt = 5;
    [iRate sharedInstance].usesUntilPrompt = 15;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Flurry integration
    //[Flurry setCrashReportingEnabled:YES];
    [Flurry setAppVersion:@"1.9"];
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"G3ZJWZ47YJRZMCP6G2JS"];
    [Flurry logEvent:@"App launched"];
    
    // Integrate PSUpdate
    [PSUpdateApp startWithAppID:@"781409756"];
    [[PSUpdateApp sharedPSUpdateApp] setStrategy:RemindStrategy];
    [[PSUpdateApp sharedPSUpdateApp] setDaysUntilPrompt:5];
    
    // Enable PUSH
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
#ifdef __IPHONE_8_0
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
#endif
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    
    [[DownloadEngine sharedEngine] start];
    
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:10 * 1024 * 1024
                                                         diskCapacity:50 * 1024 * 1024
                                                             diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
    
    NSDictionary *remoteNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotif) {
        [self application:application didReceiveRemoteNotification:remoteNotif];
    }
    
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[PSUpdateApp sharedPSUpdateApp] detectAppVersion:nil];
    [FBAppCall handleDidBecomeActive];
}

#pragma mark - did register for remote notifications with device token

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)_deviceToken
{
    NSString *TOKEN = [[[[_deviceToken description] stringByReplacingOccurrencesOfString:@"<" withString:@""]
                        stringByReplacingOccurrencesOfString:@">" withString:@""]
                       stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // Keychain
    NSString *UID = [SSKeychain passwordForService:@"781409756" account:@"UID"];
    
    if (UID == nil)
    {
        NSString *tmp  = [NSString stringWithFormat:@"%@%f", [UIDevice currentDevice].uniqueDeviceIdentifier, [[NSDate date] timeIntervalSince1970]];
        
        // Save newly created uid to keychain
        [SSKeychain setPassword:[tmp stringFromMD5] forService:@"781409756" account:@"UID"];
        UID = [SSKeychain passwordForService:@"781409756" account:@"UID"];
    }
    
    NSDictionary *PARAMS = @{@"device": @{@"uid" : UID, @"token" : TOKEN, @"os" : @"IOS"}};
    
    [Flurry logEvent:@"Device info" withParameters:PARAMS];
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://ns29646.ovh.net:8001/"]];
    [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [client setDefaultHeader:@"Authorization" value:@"Token token=\"afbadb4ff8485c0adcba486b4ca90cc4\""];
    [client postPath:@"devices.json" parameters:PARAMS
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             }];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

#pragma mark - did receive remote notification

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                    message:[apsInfo objectForKey:@"alert"]
                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - after authentication the app will be called back with the session information.

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication
                    fallbackHandler:^(FBAppCall *call) {
                        NSLog(@"In fallback handler");
                    }];
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

@end
