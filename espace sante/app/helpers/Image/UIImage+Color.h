//
//  UIImage+Color.h
//  espace sante
//
//  Created by abdel ali on 11/12/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
