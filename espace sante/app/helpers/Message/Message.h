//
//  message.h
//  espace sante
//
//  Created by abdel ali on 30/08/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface Message : NSObject

+ (void)show:(UIViewController *)controller
     message:(NSString *)msg
      detail:(NSString*)detail
       delay:(int)delay;

+ (void)load:(UIViewController *)controller
     message:(NSString *)msg
      detail:(NSString*)detail
        view:(UIView*)view
       delay:(int) delay;

+ (void)success:(UIViewController *)controller
        message:(NSString *)msg
          delay:(int)delay;

+ (void)failed:(UIViewController *)controller
       message:(NSString *)msg
         delay:(int)delay;

+ (void)warn:(UIViewController *)controller
     message:(NSString *)msg
       delay:(int)delay;
 
+ (void)hide;
+ (void)reset;

@end
