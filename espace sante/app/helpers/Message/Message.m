//
//  Message.m
//  espace sante
//
//  Created by abdel ali on 30/08/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Message.h"

static MBProgressHUD *HUD;

@implementation Message

+ (void)show:(UIViewController *)controller message:(NSString *)msg detail:(NSString*)detail delay:(int)delay{
    if(HUD == nil)
        HUD = [[MBProgressHUD alloc] initWithView:controller.view];
    
    [Message reset];
    
    [controller.view addSubview:HUD];
    HUD.delegate = (id<MBProgressHUDDelegate>)controller;
    
    if(msg!=nil)
        HUD.labelText = msg;
    if(detail!=nil){
        HUD.detailsLabelText = detail;
        HUD.square = YES;
    }
    
    HUD.customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    HUD.mode = MBProgressHUDModeText;
    
	[HUD show:YES];
    if(delay > 0){
        [HUD hide:YES afterDelay:delay];
    }
}

+ (void)load:(UIViewController *)controller message:(NSString *)msg detail:(NSString*)detail view:(UIView*)view delay:(int)delay{
    if(HUD == nil)
        HUD = [[MBProgressHUD alloc] initWithView:controller.view];
    
    [Message reset];
    
    [controller.view addSubview:HUD];
    
    HUD.delegate = (id<MBProgressHUDDelegate>)controller;
    if(msg!=nil)
        HUD.labelText = msg;
    if(detail!=nil){
        HUD.detailsLabelText = detail;
        HUD.square = YES;
    }
    
    if(view!=nil){
        HUD.customView = view;
        HUD.mode = MBProgressHUDModeCustomView;
    }
    
	[HUD show:YES];
    
    if(delay > 0){
        [HUD hide:YES afterDelay:delay];
    }
}

+ (void)success:(UIViewController *)controller message:(NSString *)msg delay:(int)delay{
    if(HUD == nil)
        HUD = [[MBProgressHUD alloc] initWithView:controller.view];
    
    [Message reset];
    
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkMark.png"]];
    HUD.mode = MBProgressHUDModeCustomView;
    
    [controller.view addSubview:HUD];
    HUD.delegate = (id<MBProgressHUDDelegate>)controller;
    
    if(msg!=nil)
        HUD.labelText = msg;
    
	[HUD show:YES];
    
    if(delay > 0){
        [HUD hide:YES afterDelay:delay];
    }
}

+ (void)failed:(UIViewController *)controller message:(NSString *)msg delay:(int)delay{
    if(HUD == nil)
        HUD = [[MBProgressHUD alloc] initWithView:controller.view];
    
    [Message reset];
    
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error.png"]];
    HUD.mode = MBProgressHUDModeCustomView;
    
    [controller.view addSubview:HUD];
    HUD.delegate = (id<MBProgressHUDDelegate>)controller;
    
    if(msg!=nil)
        HUD.labelText = msg;
    
	[HUD show:YES];
    
    if(delay > 0){
        [HUD hide:YES afterDelay:delay];
    }
}

+ (void)warn:(UIViewController *)controller message:(NSString *)msg delay:(int)delay{
    if(HUD == nil)
        HUD = [[MBProgressHUD alloc] initWithView:controller.view];
    
    [Message reset];
    
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"warning.png"]];
    HUD.mode = MBProgressHUDModeCustomView;
    
    [controller.view addSubview:HUD];
    HUD.delegate = (id<MBProgressHUDDelegate>)controller;
    
    if(msg!=nil)
        HUD.labelText = msg;
    
	[HUD show:YES];
    
    if(delay > 0){
        [HUD hide:YES afterDelay:delay];
    }
}

+ (void)hide{
	[HUD hide:YES];
}

+ (void)reset{
    HUD.labelText = nil;
    HUD.detailsLabelText = nil;
    HUD.square = NO;
    HUD.customView = nil;
    HUD.mode = MBProgressHUDModeIndeterminate;
    [HUD hide:NO];
}

@end
