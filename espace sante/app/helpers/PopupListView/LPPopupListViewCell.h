#import <UIKit/UIKit.h>


@interface LPPopupListViewCell : UITableViewCell

@property (nonatomic, strong) UIColor *highlightColor;
@property (nonatomic, strong) UIImageView *rightImageView;

@end
