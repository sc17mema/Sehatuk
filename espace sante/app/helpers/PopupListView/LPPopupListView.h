#import <UIKit/UIKit.h>
#import "LPPopupListViewCell.h"


@protocol LPPopupListViewDelegate;

@interface LPPopupListView : UIView <UITableViewDataSource,UITableViewDelegate> 

@property (nonatomic, weak) id <LPPopupListViewDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *selectedList;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIView *navigationBarView;
@property (nonatomic, strong) UIView *separatorLineView;
@property (nonatomic, assign) BOOL closeAnimated;
@property (nonatomic, strong) UIColor *cellHighlightColor;

- (id)initWithTitle:(NSString *)title list:(NSArray *)list selectedList:(NSArray *)selectedItemsList point:(CGPoint)point size:(CGSize)size multipleSelection:(BOOL)multipleSelection;
- (void)showInView:(UIView *)view animated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;

@end


#pragma mark - Delegate Protocol

@protocol LPPopupListViewDelegate <NSObject>

@optional

- (void)popupListView:(LPPopupListView *)popupListView didSelectedIndex:(NSInteger)index;
- (void)popupListViewDidHide:(LPPopupListView *)popupListView selectedList:(NSArray *)list;

@end