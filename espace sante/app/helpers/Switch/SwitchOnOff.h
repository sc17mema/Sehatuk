//
//  Switch.h
//  WAFAASSURANCE
//
//  Created by abdel ali on 20/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Switch.h"

@interface SwitchOnOff : Switch {
	UILabel *onText;
	UILabel *offText;	
}

@end
