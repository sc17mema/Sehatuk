//
//  Switch.h
//  WAFAASSURANCE
//
//  Created by abdel ali on 20/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "SwitchOnOff.h"


@implementation SwitchOnOff

- (void)initCommon
{
	[super initCommon];
    
	onText = [UILabel new];
	onText.text = @"";
	
	offText = [UILabel new];
	offText.text = @"";
}


- (void)drawUnderlayersInRect:(CGRect)aRect withOffset:(float)offset inTrackWidth:(float)trackWidth
{
	{
		CGRect textRect = [self bounds];
        textRect.size = CGSizeMake(63, 23);
		textRect.origin.x += 14.0 + (offset - trackWidth);
        textRect.origin.y = textRect.origin.y;
		[onText drawTextInRect:textRect];	
	}
	
	{
		CGRect textRect = [self bounds];
        textRect.size = CGSizeMake(63, 23);
        textRect.origin.y = textRect.origin.y;
		textRect.origin.x += (offset + trackWidth) - 12.0;
		[offText drawTextInRect:textRect];
	}	
}

@end
