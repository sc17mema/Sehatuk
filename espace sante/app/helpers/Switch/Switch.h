//
//  Switch.h
//  WAFAASSURANCE
//
//  Created by abdel ali on 20/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Switch : UIControl
{
	UIImage *knobImage;
	UIImage *knobImagePressed;
	
	UIImage *sliderOff;
	UIImage *sliderOn;
	
	float percent, oldPercent;
	float knobWidth;
	float endcapWidth;
	CGPoint startPoint;
	float scale;
    float drawHeight;
	float animationDuration;
	
	CGSize lastBoundsSize;
	
	NSDate *endDate;
	BOOL mustFlip;
    
}

- (void)initCommon;
- (void)regenerateImages;
- (void)drawUnderlayersInRect:(CGRect)aRect withOffset:(float)offset inTrackWidth:(float)trackWidth;
- (void)setOn:(BOOL)aBool animated:(BOOL)animated;


@property(readwrite,assign) float knobWidth;
@property(readwrite,assign,getter=isOn) BOOL on;

@end
