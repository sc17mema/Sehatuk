//
//  File.h
//  espace sante
//
//  Created by abdel ali on 30/08/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface File : NSObject

+(void)appendData:(NSDictionary *)data toFile:(NSString *)file;
+(void)appendData:(NSDictionary *)data toKey:(NSString *)key intoFile:(NSString *)file;
+(void)prependData:(NSDictionary *)data toFile:(NSString *)file;
+(NSMutableArray *)getDataFromFile:(NSString *)file;
+(NSMutableDictionary *)getBookMarksFromFile:(NSString *)file;
+(void)deleteData:(NSDictionary *)data ofFile:(NSString *)file;
+(NSMutableArray *)getLocalDataFromFile:(NSString *)file;
+(void)deleteValue:(NSDictionary *)data withKey:(NSString *)key ofFile:(NSString *)file;
+(void)deleteFile:(NSString *)file;

@end
