//
//  File.m
//  espace sante
//
//  Created by abdel ali on 30/08/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "File.h"

@implementation File

+(void)appendData:(NSDictionary *)data toFile:(NSString *)file{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
	if ([paths count] > 0){
        
		NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:file];
        
		if(![fileManager fileExistsAtPath:path]){
			
            NSMutableArray *fileData = [[NSMutableArray alloc] init];
            
            if([fileData writeToFile:path atomically:YES]){
                
            }
		}
        
        NSMutableArray *fileData = [[NSMutableArray alloc] initWithContentsOfFile:path];
        [fileData addObject:data];
        if([fileData writeToFile:path atomically:YES]){
            
        }
	}
}

+(void)appendData:(NSDictionary *)data toKey:(NSString *)key intoFile:(NSString *)file
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
	if ([paths count] > 0){
        
		NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:file];
        
		if(![fileManager fileExistsAtPath:path]){
			
            NSMutableDictionary *fileData = [[NSMutableDictionary alloc] init];
            
            if([fileData writeToFile:path atomically:YES]){
                
            }
		}
        
        NSMutableDictionary *fileData = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        
        if([fileData valueForKey:key] == nil)
            [fileData setValue:[NSMutableArray array] forKey:key];
        
        [fileData[key] addObject:data];
        
        if([fileData writeToFile:path atomically:YES]){
            
        }
	}
}

+(void)prependData:(NSDictionary *)data toFile:(NSString *)file{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	if ([paths count] > 0){
		NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:file];
		if(![fileManager fileExistsAtPath:path]){
			NSMutableArray *fileData = [[NSMutableArray alloc] init];
            if([fileData writeToFile:path atomically:YES]){
                
            }
		}
        NSMutableArray *fileData = [[NSMutableArray alloc] initWithContentsOfFile:path];
        if(![fileData containsObject:data]){
            [fileData insertObject:data atIndex:0];
            if([fileData writeToFile:path atomically:YES]){
                
            }
        }
	}
}

+(NSMutableArray *)getDataFromFile:(NSString *)file{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSMutableArray *fileData = nil;
	if ([paths count] > 0){
		NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = file;//[[paths objectAtIndex:0] stringByAppendingPathComponent:file];
        
		if(![fileManager fileExistsAtPath:path]){
			return nil;
		}
        
        NSMutableArray *fileData = [[NSMutableArray alloc] initWithContentsOfFile:path];
        
//        NSError * error=nil;
//        NSString *json = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
//        
//        NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
//        
//        NSMutableArray *fileData = [[NSMutableArray alloc] initWithArray:[NSJSONSerialization JSONObjectWithData:data options:0 error:&error]];
        
        return  fileData;
	}
    
    return fileData;
}

+(NSMutableArray *)getLocalDataFromFile:(NSString *)file{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSMutableArray *fileData = nil;
	if ([paths count] > 0){
		NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:file];
		if(![fileManager fileExistsAtPath:path]){
			return nil;
		}
        
        NSMutableArray *fileData = [[NSMutableArray alloc] initWithContentsOfFile:path];
        return  fileData;
	}
    
    return fileData;
}

+(NSMutableDictionary *)getBookMarksFromFile:(NSString *)file{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSMutableDictionary *fileData = nil;
	if ([paths count] > 0){
		NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:file];
		if(![fileManager fileExistsAtPath:path]){
			return nil;
		}
        
        NSMutableDictionary *fileData = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        return  fileData;
	}
    
    return fileData;
}

+(void)deleteData:(NSDictionary *)data ofFile:(NSString *)file{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	if ([paths count] > 0){
		NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:file];
		if([fileManager fileExistsAtPath:path]){
			NSMutableArray *fileData = [[NSMutableArray alloc] initWithContentsOfFile:path];
            [fileData removeObject:data];
            
            if([fileData writeToFile:path atomically:YES]){
            }
		}
	}
}

+(void)deleteValue:(NSDictionary *)data withKey:(NSString *)key ofFile:(NSString *)file{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	if ([paths count] > 0){
		NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:file];
		if([fileManager fileExistsAtPath:path]){
            
			NSMutableDictionary *fileData = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
            [fileData[key] removeObject:data];
            
            if([fileData[key] count] == 0)
                [fileData removeObjectForKey:key];
            
            if([fileData writeToFile:path atomically:YES]){
            }
		}
	}
}

+(void)deleteFile:(NSString *)file{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	
    if ([paths count] > 0){
		NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:file];
		if([fileManager fileExistsAtPath:path]){
            
			[fileManager removeItemAtPath:path error:nil];
		}
	}
}



@end
