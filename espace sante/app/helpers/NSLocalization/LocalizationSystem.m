//
//  LocalizationSystem.m
//  espace sante
//
//  Created by abdel ali on 8/23/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "LocalizationSystem.h"

@implementation LocalizationSystem

//Singleton instance
static LocalizationSystem *_sharedLocalSystem = nil;
static NSBundle *bundle = nil;

+ (LocalizationSystem *)sharedLocalSystem
{
	@synchronized([LocalizationSystem class])
	{
		if (!_sharedLocalSystem){
			[[self alloc] init];
		}
		return _sharedLocalSystem;
	}
	// to avoid compiler warning
	return nil;
}

+ (id)alloc
{
	@synchronized([LocalizationSystem class])
	{
		NSAssert(_sharedLocalSystem == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedLocalSystem = [super alloc];
		return _sharedLocalSystem;
	}
	// to avoid compiler warning
	return nil;
}

- (id)init
{
    if ((self = [super init])) 
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:LocalizationGetLanguage ofType:@"lproj"];
        
        if (path != nil)
            bundle = [NSBundle bundleWithPath:path];
        else
            bundle = [NSBundle mainBundle];
	}
    return self;
}

// Gets the current localized string as in NSLocalizedString.
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment
{
	return [bundle localizedStringForKey:key value:comment table:@"Localizable"];
}

// Sets the desired language of the ones you have.
- (void) setLanguage:(NSString*) l
{
	NSString *path = [[NSBundle mainBundle] pathForResource:l ofType:@"lproj"];
	
	if (path == nil)
		[self resetLocalization];
	else {
		bundle = [NSBundle bundleWithPath:path];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults removeObjectForKey:@"AppleLanguages"];
        
        NSArray *sysLangugages = [defaults arrayForKey:@"AppleLanguages"];
        NSString *sysLanguage = [sysLangugages objectAtIndex:0];
        
        NSArray *array = [NSArray arrayWithObjects:l, sysLanguage, nil];
        [defaults setObject:array forKey:@"AppleLanguages"];
    }
}

// Just gets the current setted up language.
- (NSString*) getLanguage
{
	NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    
    if([languages[0]  isEqual: @"fr"] || [languages[0]  isEqual: @"en"] || [languages[0]  isEqual: @"ar"])
        return languages[0];
    else
        return [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
}

// Resets the localization system, so it uses the OS default language.
- (void) resetLocalization
{
	bundle = [NSBundle mainBundle];
}

- (NSString *)getCountry
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"LocalCountry"];
}

- (void)setCountry:(NSString *)c
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:c forKey:@"LocalCountry"];
}


@end
