//
//  LocalizationSystem.h
//  espace sante
//
//  Created by abdel ali on 8/23/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SLocalizedString(key, comment) [[LocalizationSystem sharedLocalSystem] localizedStringForKey:(key) value:(comment)]
#define LocalizationSetLanguage(language) [[LocalizationSystem sharedLocalSystem] setLanguage:(language)]
#define LocalizationGetLanguage [[LocalizationSystem sharedLocalSystem] getLanguage]
#define LocalizationReset [[LocalizationSystem sharedLocalSystem] resetLocalization]

#define LocalizationSetCountry(c) [[LocalizationSystem sharedLocalSystem] setCountry:(c)]
#define LocalizationGetCountry [[LocalizationSystem sharedLocalSystem] getCountry]

@interface LocalizationSystem : NSObject
{
	NSString *language;
    NSString *country;
}

+ (LocalizationSystem *)sharedLocalSystem;
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;
- (void) setLanguage:(NSString*) language;
- (NSString* )getLanguage;
- (void) resetLocalization;

- (void)setCountry:(NSString*)c;
- (NSString *)getCountry;

@end
