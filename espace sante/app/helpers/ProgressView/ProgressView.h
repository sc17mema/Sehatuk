#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ProgressView : UIView
{
    CGFloat _progress;
    CGFloat _animationTime;
}

@property (nonatomic, readonly) CAGradientLayer *gradientLayer;

@property (nonatomic, copy) void(^didFinishAnimation)(void);

// an array of UIColors for the gradient
@property (nonatomic, readwrite) NSArray *colors UI_APPEARANCE_SELECTOR;
@property (nonatomic, getter = isHorizontal) BOOL horizontal;

// simple colors
@property (nonatomic, readwrite) UIColor *backColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, readwrite) UIColor *frontColor UI_APPEARANCE_SELECTOR;

@property (nonatomic, strong) UIImage *maskingImage UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat progress;

+ (instancetype)progressViewWithFrame:(CGRect)frame andMaskingImage:(UIImage *)maskingImage;
- (instancetype)initWithFrame:(CGRect)frame andMaskingImage:(UIImage *)maskingImage;

- (void)setAnimationTime:(CGFloat)time;
- (void)setProgress:(CGFloat)progress animated:(BOOL)animated;

@end
