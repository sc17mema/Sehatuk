//
//  RotaryWheelSector.m
//  espace sante
//
//  Created by abdel ali on 09/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "RotaryWheelSector.h"

@interface RotaryWheelSector ()

@end

@implementation RotaryWheelSector

- (NSString *) description
{
    return [NSString stringWithFormat:@"%i | %f, %f, %f", self.sector, self.minValue, self.midValue, self.maxValue];
}

@end
