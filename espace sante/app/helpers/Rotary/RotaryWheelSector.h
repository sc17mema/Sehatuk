//
//  RotaryWheelSector.h
//  espace sante
//
//  Created by abdel ali on 09/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RotaryWheelSector : UIViewController

@property float minValue;
@property float maxValue;
@property float midValue;
@property int sector;

@end
