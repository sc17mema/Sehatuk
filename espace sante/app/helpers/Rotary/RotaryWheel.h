//
//  RotaryWheel.h
//  espace sante
//
//  Created by abdel ali on 09/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RotaryWheelDelegate.h"
#import "RotaryWheelSector.h"

@interface RotaryWheel : UIControl

@property (weak) id <RotaryWheelDelegate> delegate;
@property (nonatomic, strong) UIView *container;
@property int numberOfSections;
@property CGAffineTransform startTransform;
@property (nonatomic, strong) NSMutableArray *sectors;
@property int currentSector;

- (id) initWithFrame:(CGRect)frame andDelegate:(id)delegate withSections:(int)sectionsNumber;
- (void)drawWheel;

- (void) buildSectorsEven;
- (void) buildSectorsOdd;

@end
