//
//  RotaryWheel.m
//  espace sante
//
//  Created by abdel ali on 09/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "RotaryWheel.h"

static float deltaAngle;

@implementation RotaryWheel

- (id) initWithFrame:(CGRect)frame andDelegate:(id)delegate withSections:(int)sectionsNumber
{
    if ((self = [super initWithFrame:frame]))
    {
        self.numberOfSections = sectionsNumber;
        self.delegate = delegate;
        self.currentSector = 0;
    
        [self drawWheel];
	}
    return self;
}

- (void) drawWheel
{
    self.container = [[UIView alloc] initWithFrame:self.frame];
    
    CGFloat angleSize = 2*M_PI/_numberOfSections;
    
    for (int i = 0; i < _numberOfSections; i++)
    {
        UILabel *baton = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
        baton.layer.anchorPoint = CGPointMake(1.0f, 0.5f);
        baton.layer.position = CGPointMake(_container.bounds.size.width/2.0, _container.bounds.size.height/2.0);
        baton.transform = CGAffineTransformMakeRotation(angleSize * i);
        baton.clipsToBounds = NO;
        baton.backgroundColor = [UIColor clearColor];
        baton.tag = i;
        
        if(i==0)
        {
            UIImageView *indicateur = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 20, 20)];
            indicateur.contentMode = UIViewContentModeScaleAspectFit;
            indicateur.image = [UIImage imageNamed:@"indicateur_molette.png"];
            [baton addSubview:indicateur];
        }
        
        [self.container addSubview:baton];
    }
    
    self.container.userInteractionEnabled = NO;
    [self addSubview:self.container];
    
    self.sectors = [NSMutableArray arrayWithCapacity:self.numberOfSections];
    
    if (_numberOfSections % 2 == 0)
        [self buildSectorsEven];
    else
        [self buildSectorsOdd];
    
    [self.delegate wheelDidChangeValue:[NSString stringWithFormat:@"%d", self.currentSector]];
    
    
    CGRect frame = self.frame;
    frame.size.width = frame.size.width - 150;
    frame.size.height = frame.size.height - 150;
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:frame];
	bg.image = [UIImage imageNamed:@"molette.png"];
    bg.contentMode = UIViewContentModeScaleAspectFit;
    bg.center = self.center;
	[self.container addSubview:bg];
}

#pragma mark - adding rotation

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint touchPoint = [touch locationInView:self];
    
    float dist = [self calculateDistanceFromCenter:touchPoint];
    
    if (dist < 40)
    {
        NSLog(@"ignore tap");
        return NO;
    }
    
    float dx = touchPoint.x - _container.center.x;
    float dy = touchPoint.y - _container.center.y;
    
    deltaAngle = atan2(dy,dx);
    
    self.startTransform = _container.transform;
    
    return YES;
}

- (BOOL)continueTrackingWithTouch:(UITouch*)touch withEvent:(UIEvent*)event
{
    CGPoint pt = [touch locationInView:self];
    
    float dist = [self calculateDistanceFromCenter:pt];
    
    if (dist < 40)
    {
        NSLog(@"ignore tap");
        return NO;
    }
    
    float dx = pt.x  - _container.center.x;
    float dy = pt.y  - _container.center.y;
    float ang = atan2(dy,dx);
    
    float angleDifference = deltaAngle - ang;
    self.container.transform = CGAffineTransformRotate(_startTransform, -angleDifference);
    
    return YES;
}

- (void)endTrackingWithTouch:(UITouch*)touch withEvent:(UIEvent*)event
{
    CGFloat radians = atan2f(_container.transform.b, _container.transform.a);
    
    CGFloat newVal = 0.0;
    
    for (RotaryWheelSector *s in self.sectors)
    {
        if (s.minValue > 0 && s.maxValue < 0) {
            
            if (s.maxValue > radians || s.minValue < radians) {
                
                if (radians > 0)
                    newVal = radians - M_PI;
                else
                    newVal = M_PI + radians;
                
                self.currentSector = s.sector;
            }
        }
        else if (radians > s.minValue && radians < s.maxValue)
        {
            newVal = radians - s.midValue;
            self.currentSector = s.sector;
			
            break;
        }
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    CGAffineTransform t = CGAffineTransformRotate(_container.transform, -newVal);
    self.container.transform = t;
    [UIView commitAnimations];
    
    [self.delegate wheelDidChangeValue:[NSString stringWithFormat:@"%d", self.numberOfSections - self.currentSector]];
}


#pragma mark - helper

- (float) calculateDistanceFromCenter:(CGPoint)point
{
    CGPoint center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    float dx = point.x - center.x;
    float dy = point.y - center.y;
    return sqrt(dx*dx + dy*dy);
}

- (void) buildSectorsOdd
{
	CGFloat lenght = M_PI*2/_numberOfSections;
    
    CGFloat mid = 0;
	
    for (int i = 0; i < _numberOfSections; i++)
    {
        RotaryWheelSector *sector = [[RotaryWheelSector alloc] init];
		
        sector.midValue = mid;
        sector.minValue = mid - (lenght/2);
        sector.maxValue = mid + (lenght/2);
        sector.sector = i;
        
        mid -= lenght;
        
        if (sector.minValue < - M_PI)
        {
            mid = -mid;
            mid -= lenght;
        }
        
        NSLog(@"%@", sector);
        [self.sectors addObject:sector];
    }
    
    
}

- (void) buildSectorsEven {
    
    CGFloat lenght = M_PI*2/_numberOfSections;
    
    CGFloat mid = 0;
    
    for (int i = 0; i < _numberOfSections; i++)
    {
        RotaryWheelSector *sector = [[RotaryWheelSector alloc] init];
        
        sector.midValue = mid;
        sector.minValue = mid - (lenght/2);
        sector.maxValue = mid + (lenght/2);
        sector.sector = i;
        
        if (sector.maxValue-lenght < - M_PI)
        {
            mid = M_PI;
            sector.midValue = mid;
            sector.minValue = fabsf(sector.maxValue);
        }
        
        mid -= lenght;
        
        [self.sectors addObject:sector];
    }
}

@end

