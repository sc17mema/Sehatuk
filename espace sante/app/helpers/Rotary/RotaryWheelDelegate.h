//
//  RotaryWheelDelegate.h
//  espace sante
//
//  Created by abdel ali on 09/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RotaryWheelDelegate <NSObject>

- (void) wheelDidChangeValue:(NSString *)newValue;

@end
