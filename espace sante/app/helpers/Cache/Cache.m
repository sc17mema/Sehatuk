//
//  Cache.m
//  espace sante
//
//  Created by abdel ali on 30/08/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Cache.h"

@implementation Cache

+ (void)setObject:(NSData *)data forKey:(NSString *)key withExpires:(int)expires{
	
    NSDate *dt = [NSDate date];
	double now = [dt timeIntervalSince1970];
    
    NSMutableString *expiresString = [[NSMutableString alloc] init];
	
    NSData *dataExpires = [[expiresString stringByAppendingFormat:@"%f",now+expires] dataUsingEncoding:NSUTF8StringEncoding];
	
    [dataExpires writeToFile:[[self getPath:key] stringByAppendingFormat:@"%@",@".expires"] atomically:NO];
    [data writeToFile:[self getPath:key] atomically:NO];
}

+ (NSData *)get:(NSString *)key{
    
	if(![self fileExists:[self getPath:key]] || [self isExpired:[self getPath:key]]) return nil;
	
    NSData *data = [NSData dataWithContentsOfFile:[self getPath:key]];
    
	return data;
}

+ (void) clear{
    
}

+ (NSString *)getPath:(NSString*)key {
    
	NSString *tempPath = NSTemporaryDirectory();
	return [tempPath stringByAppendingPathComponent:[Security md5:key]];
}

+ (BOOL)fileExists:(NSString *)filepath {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	return [fileManager fileExistsAtPath:filepath];
}

+ (BOOL)isExpired:(NSString *)key {
    
	NSData *data = [NSData dataWithContentsOfFile:[key stringByAppendingFormat:@"%@",@".expires"]];
	NSString *expires = [[NSString alloc] initWithData:data  encoding:NSUTF8StringEncoding];
	double exp = [expires doubleValue];
	
    NSDate *dt = [NSDate date];
	double value = [dt timeIntervalSince1970];
	
	if(exp > value){
		return NO;
	}
    
    // remove file
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:key error:NULL];
    [fileManager removeItemAtPath:[key stringByAppendingFormat:@"%@",@".expires"] error:NULL];
    
	return YES;
}

@end
