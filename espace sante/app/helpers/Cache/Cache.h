//
//  Cache.h
//  espace sante
//
//  Created by abdel ali on 30/08/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "security.h"

@interface Cache : NSObject

+ (void)setObject:(NSData *)data forKey:(NSString *)key withExpires:(int)expires;
+ (NSData *)get:(NSString *)key;
+ (void)clear;
+ (NSString *)getPath:(NSString*)key;
+ (BOOL)fileExists:(NSString *)filepath;
+ (BOOL)isExpired:(NSString *)key;

@end
