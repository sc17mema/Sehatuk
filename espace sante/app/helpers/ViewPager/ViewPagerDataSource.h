//
//  ViewPagerDataSource.h
//  espace sante
//
//  Created by abdel ali on 02/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewPagerController.h"

@protocol ViewPagerDataSource <NSObject>

@required

- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager;

- (UIView *)viewPager:(ViewPagerController *)viewPager
    viewForTabAtIndex:(NSUInteger)index;

@optional

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index;

- (UIView *)viewPager:(ViewPagerController *)viewPager contentViewForTabAtIndex:(NSUInteger)index;

@end