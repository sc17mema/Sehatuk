//
//  ViewPagerTab.m
//  espace sante
//
//  Created by abdel ali on 02/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "ViewPagerTab.h"

@implementation ViewPagerTab

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = NO;
    }
    return self;
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    
    // Draw an indicator line if tab is selected
    if (self.selected) {
        
    }
}

@end
