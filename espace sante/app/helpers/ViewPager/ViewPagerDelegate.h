//
//  ViewPagerDelegate.h
//  espace sante
//
//  Created by abdel ali on 02/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewPagerController.h"

@protocol ViewPagerDelegate <NSObject>

@required

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index;
- (void)viewPager:(ViewPagerController *)viewPager didAnimateTab:(CGFloat)position;

- (CGFloat)viewPager:(ViewPagerController *)viewPager
      valueForOption:(ViewPagerOption)option
         withDefault:(CGFloat)value;

@optional

// customize components.
- (UIColor *)viewPager:(ViewPagerController *)viewPager
     colorForComponent:(ViewPagerComponent)component
           withDefault:(UIColor *)color;

@end
