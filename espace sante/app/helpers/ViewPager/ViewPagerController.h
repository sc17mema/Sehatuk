//
//  ViewPagerController.h
//  espace sante
//
//  Created by abdel ali on 28/08/2013.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - Constants

#define DefaultTabHeight 44.0 // Default tab height
#define DefaultTabOffset 60.0 // Offset of the second and further tabs' from left
#define DefaultTabWidth 128.0
#define DefaultTabLocation 1.0 //Meaning of values 1.0: Top, 0.0: Bottom
#define PageViewTag 34
#define DefaultIndicatorColor [UIColor colorWithRed:178.0/255.0 green:203.0/255.0 blue:57.0/255.0 alpha:0.75]
#define DefaultTabsViewBackgroundColor [UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:0.75]
#define DefaultContentViewBackgroundColor [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:0.75]

#pragma mark - 

typedef NS_ENUM(NSUInteger, ViewPagerOption) {
    ViewPagerOptionTabHeight,
    ViewPagerOptionTabOffset,
    ViewPagerOptionTabWidth,
    ViewPagerOptionTabLocation
};

typedef NS_ENUM(NSUInteger, ViewPagerComponent) {
    ViewPagerIndicator,
    ViewPagerTabsView,
    ViewPagerContent
};

@protocol ViewPagerDataSource;
@protocol ViewPagerDelegate;
@class ViewPagerTab;

@interface ViewPagerController : UIViewController<UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate>

@property id<ViewPagerDataSource> dataSource;
@property id<ViewPagerDelegate> delegate;

#pragma mark ViewPagerOptions

@property CGFloat tabHeight;
@property CGFloat tabOffset;
@property CGFloat tabWidth;
@property CGFloat tabLocation;

#pragma mark Colors

@property UIColor *indicatorColor;
@property UIColor *tabsViewBackgroundColor;
@property UIColor *contentViewBackgroundColor;

#pragma mark Properties

@property UIPageViewController *pageViewController;
@property (assign) id<UIScrollViewDelegate> origPageScrollViewDelegate;

@property UIScrollView *tabsView;
@property UIView *contentView;
@property NSMutableArray *tabs;
@property NSMutableArray *contents;
@property NSUInteger tabCount;

@property (getter = isAnimatingToTab, assign) BOOL animatingToTab;
@property (nonatomic) NSUInteger activeTabIndex;

#pragma mark Methods

- (void)reloadData;
- (ViewPagerTab *)tabViewAtIndex:(NSUInteger)index;

@end


