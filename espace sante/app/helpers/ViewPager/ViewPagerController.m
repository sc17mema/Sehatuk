//
//  ViewPagerController.m
//  espace sante
//
//  Created by abdel ali on 28/08/2013.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "ViewPagerController.h"
#import "ViewPagerDataSource.h"
#import "ViewPagerDelegate.h"
#import "ViewPagerTab.h"

@implementation ViewPagerController

#pragma mark initialize

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self defaultSettings];
    }
    return self;
}

#pragma mark view life cycle

- (void)viewDidLoad
{    
    [super viewDidLoad];
    [self reloadData];
}

- (void)viewWillLayoutSubviews { // view will layout subviews
    
    CGRect frame;
    
    frame = _tabsView.frame;
    frame.origin.x = 0.0;
    frame.origin.y = self.tabLocation ? self.tabLocation : 0.0;
    frame.size.width = self.view.bounds.size.width;
    frame.size.height = self.tabHeight;
    _tabsView.frame = frame;
    
    frame = _contentView.frame;
    frame.origin.x = 0.0;
    frame.origin.y = _tabsView.frame.origin.y + _tabsView.frame.size.height;
    frame.size.width = self.view.bounds.size.width;
    frame.size.height = self.view.frame.size.height - frame.origin.y;
    _contentView.frame = frame;
}


- (IBAction)gesture:(id)sender {
    
    self.animatingToTab = YES;
    
    // Get the desired page's index
    UITapGestureRecognizer *tapGestureRecognizer = (UITapGestureRecognizer *)sender;
    UIView *tabView = tapGestureRecognizer.view;
    __block NSUInteger index = [_tabs indexOfObject:tabView];
    
    // Get the desired viewController
    UIViewController *viewController = [self viewControllerAtIndex:index];
    
    // __weak weak reference
    __block UIPageViewController *weakPageViewController = self.pageViewController;
    __weak ViewPagerController *weakSelf = self;
    
    if (index < self.activeTabIndex) {
        
        [self.pageViewController setViewControllers:@[viewController]
                                          direction:UIPageViewControllerNavigationDirectionReverse
                                           animated:YES
                                         completion:^(BOOL completed) {
                                             weakSelf.animatingToTab = NO;
                                             
                                             // Set the current page again to obtain synchronisation between tabs and content
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 [weakPageViewController setViewControllers:@[viewController]
                                                                                  direction:UIPageViewControllerNavigationDirectionReverse
                                                                                   animated:NO
                                                                                 completion:nil];
                                             });
                                         }];
        
    } else if (index > self.activeTabIndex) {
        
        [self.pageViewController setViewControllers:@[viewController]
                                          direction:UIPageViewControllerNavigationDirectionForward
                                           animated:YES
                                         completion:^(BOOL completed) {
                                             weakSelf.animatingToTab = NO;
                                             
                                             // Set the current page again to obtain synchronisation between tabs and content
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 [weakPageViewController setViewControllers:@[viewController]
                                                                                  direction:UIPageViewControllerNavigationDirectionForward
                                                                                   animated:NO
                                                                                 completion:nil];
                                             });
                                         }];
        
        

        
        
    }
    
    self.activeTabIndex = index;
}

#pragma mark setter

- (void)setActiveTabIndex:(NSUInteger)activeTabIndex {
    
    ViewPagerTab *activeTabView;
    
    // set unselected tab to inactive
    activeTabView = [self tabViewAtIndex:self.activeTabIndex];
    activeTabView.selected = NO;
    [(UIImageView *)[activeTabView.subviews lastObject] setImage:[UIImage imageNamed:@"bouton_normal.png"]];
    
    // active selected tab
    activeTabView = [self tabViewAtIndex:activeTabIndex];
    activeTabView.selected = YES;
    [(UIImageView *)[activeTabView.subviews lastObject] setImage:[UIImage imageNamed:@"bouton_actif.png"]];
    
    // current activeTabIndex
    _activeTabIndex = activeTabIndex;
    
    // inform delegate about the change
    if ([self.delegate respondsToSelector:@selector(viewPager:didChangeTabToIndex:)]) {
        [self.delegate viewPager:self didChangeTabToIndex:self.activeTabIndex];
    }
    
    // bring tab to active position
    UIView *tabView = [self tabViewAtIndex:self.activeTabIndex];
    CGRect frame = tabView.frame;
    
    frame.origin.x -= self.tabOffset;
    frame.size.width = self.tabsView.frame.size.width;
    
    [_tabsView scrollRectToVisible:frame animated:YES];
}

#pragma mark settings

- (void)defaultSettings {
    
    // default settings
    _tabHeight = DefaultTabHeight;
    _tabOffset = DefaultTabOffset;
    _tabWidth = DefaultTabWidth;
    _tabLocation = DefaultTabLocation;
    
    // default colors
    _indicatorColor = DefaultIndicatorColor;
    _tabsViewBackgroundColor = DefaultTabsViewBackgroundColor;
    _contentViewBackgroundColor = DefaultContentViewBackgroundColor;
    
    // pageViewController
    _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                          navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                        options:nil];
    
    // setup some forwarding events
    self.origPageScrollViewDelegate = ((UIScrollView*)[_pageViewController.view.subviews objectAtIndex:0]).delegate;
    [((UIScrollView*)[_pageViewController.view.subviews objectAtIndex:0]) setDelegate:self];
    
    _pageViewController.dataSource = self;
    _pageViewController.delegate = self;
    
    self.animatingToTab = NO;

}

- (void)reloadData {
    
    // get settings if provided
    if ([self.delegate respondsToSelector:@selector(viewPager:valueForOption:withDefault:)]) {
        
        _tabHeight = [self.delegate viewPager:self valueForOption:ViewPagerOptionTabHeight withDefault:DefaultTabHeight];
        _tabOffset = [self.delegate viewPager:self valueForOption:ViewPagerOptionTabOffset withDefault:DefaultTabOffset];
        _tabWidth = [self.delegate viewPager:self valueForOption:ViewPagerOptionTabWidth withDefault:DefaultTabWidth];
        _tabLocation = [self.delegate viewPager:self valueForOption:ViewPagerOptionTabLocation withDefault:DefaultTabLocation];
        
    }
    
    // get colors if provided
    if ([self.delegate respondsToSelector:@selector(viewPager:colorForComponent:withDefault:)]) {
        
        _indicatorColor = [self.delegate viewPager:self colorForComponent:ViewPagerIndicator withDefault:DefaultIndicatorColor];
        _tabsViewBackgroundColor = [self.delegate viewPager:self colorForComponent:ViewPagerTabsView withDefault:DefaultTabsViewBackgroundColor];
        _contentViewBackgroundColor = [self.delegate viewPager:self colorForComponent:ViewPagerContent withDefault:DefaultContentViewBackgroundColor];
        
    }
    
    // empty tabs and contents
    [_tabs removeAllObjects];
    [_contents removeAllObjects];
    
    // tabs count
    _tabCount = [self.dataSource numberOfTabsForViewPager:self];
    
    // populate arrays with [NSNull null];
    _tabs = [NSMutableArray arrayWithCapacity:_tabCount];
    for (int i = 0; i < _tabCount; i++) {
        [_tabs addObject:[NSNull null]];
    }
    
    _contents = [NSMutableArray arrayWithCapacity:_tabCount];
    for (int i = 0; i < _tabCount; i++) {
        [_contents addObject:[NSNull null]];
    }
    
    // add tab views
    _tabsView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.tabHeight)];
    _tabsView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _tabsView.backgroundColor = self.tabsViewBackgroundColor;
    _tabsView.showsHorizontalScrollIndicator = NO;
    _tabsView.showsVerticalScrollIndicator = NO;
    _tabsView.clipsToBounds = NO;
    
    [self.view addSubview:_tabsView];
    
    // Add tab views to _tabsView
    CGFloat contentSizeWidth = 0;
    
    for (int i = 0; i < _tabCount; i++) {
        
        UIView *tabView = [self tabViewAtIndex:i];
        
        CGRect frame = tabView.frame;
        frame.origin.x = contentSizeWidth;
        frame.size.width = self.tabWidth;
        tabView.frame = frame;
        
        [_tabsView addSubview:tabView];
        
        contentSizeWidth += tabView.frame.size.width;
        
        // capture tap events
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gesture:)];
        [tabView addGestureRecognizer:tap];
    }
    
    _tabsView.contentSize = CGSizeMake(contentSizeWidth, self.tabHeight);
    
    // Add contentView
    _contentView = [self.view viewWithTag:PageViewTag];
    
    if (!_contentView) {
        
        _contentView = _pageViewController.view;
        _contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _contentView.backgroundColor = self.contentViewBackgroundColor;
        _contentView.bounds = self.view.bounds;
        _contentView.tag = PageViewTag;
        
        [self.view addSubview:_contentView];
    }
    
    // Set first viewController
    UIViewController *viewController = [self viewControllerAtIndex:(_activeTabIndex != 0) ? _activeTabIndex : 0];
    
    if (viewController == nil) {
        viewController = [[UIViewController alloc] init];
        viewController.view = [[UIView alloc] init];
    }
    
    [_pageViewController setViewControllers:@[viewController]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:NO
                                 completion:nil];
    
    // Set activeTabIndex
    self.activeTabIndex = 0;
}

#pragma mark - tabView & contentView data source

- (ViewPagerTab *)tabViewAtIndex:(NSUInteger)index {
    
    if (index >= _tabCount) {
        return nil;
    }
    
    if ([[_tabs objectAtIndex:index] isEqual:[NSNull null]]) {

        // get contentView
        UIView *tabViewContent = [self.dataSource viewPager:self viewForTabAtIndex:index];
        
        // create TabView and subview the content
        ViewPagerTab *tabView = [[ViewPagerTab alloc] initWithFrame:CGRectMake(0.0, 0.0, self.tabWidth, self.tabHeight)];
        [tabView addSubview:tabViewContent];
        [tabView setClipsToBounds:NO];
        [tabView setIndicatorColor:self.indicatorColor];
        
        tabViewContent.center = tabView.center;
        
        // replace the null object with tabView
        [_tabs replaceObjectAtIndex:index withObject:tabView];
    }
    
    return [_tabs objectAtIndex:index];
}

- (NSUInteger)indexForTabView:(UIView *)tabView {
    
    return [_tabs indexOfObject:tabView];
}

//------------------------
- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    if (index >= _tabCount) {
        return nil;
    }
    
    if ([[_contents objectAtIndex:index] isEqual:[NSNull null]]) {
        
        UIViewController *viewController;
        
        if ([self.dataSource respondsToSelector:@selector(viewPager:contentViewControllerForTabAtIndex:)]) {
            
            viewController = [self.dataSource viewPager:self contentViewControllerForTabAtIndex:index];
            
        } else if ([self.dataSource respondsToSelector:@selector(viewPager:contentViewForTabAtIndex:)]) {
            
            UIView *view = [self.dataSource viewPager:self contentViewForTabAtIndex:index];
            
            // adjust view bounds
            UIView *pageView = [self.view viewWithTag:PageViewTag];
            view.frame = pageView.bounds;
            
            viewController = [UIViewController new];
            viewController.view = view;
            
        } else {
            
            viewController = [[UIViewController alloc] init];
            viewController.view = [[UIView alloc] init];
        
        }
        
        [_contents replaceObjectAtIndex:index withObject:viewController];
    }
    
    return [_contents objectAtIndex:index];
}

- (NSUInteger)indexForViewController:(UIViewController *)viewController {
    
    return [_contents indexOfObject:viewController];
}

#pragma mark UIPageViewControllerDataSource && UIPageViewControllerDelegate

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self indexForViewController:viewController];
    index++;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self indexForViewController:viewController];
    index--;
    return [self viewControllerAtIndex:index];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    
    UIViewController *viewController = self.pageViewController.viewControllers[0];
    self.activeTabIndex = [self indexForViewController:viewController];
}

#pragma mark - UIScrollViewDelegate, Responding to Scrolling and Dragging

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ([self.origPageScrollViewDelegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.origPageScrollViewDelegate scrollViewDidScroll:scrollView];
    }
    
    if (![self isAnimatingToTab]) {
        UIView *tabView = [self tabViewAtIndex:self.activeTabIndex];
        
        // Get the related tab view position
        CGRect frame = tabView.frame;
        
        CGFloat movedRatio = (scrollView.contentOffset.x / scrollView.frame.size.width) - 1;

        frame.origin.x += movedRatio * frame.size.width;
        frame.origin.x -= self.tabOffset;
        frame.size.width = self.tabsView.frame.size.width;
        
        float x = frame.origin.x + 60;
        
        if ([self.delegate respondsToSelector:@selector(viewPager:didAnimateTab:)]) {
            [self.delegate viewPager:self didAnimateTab:x];
        }
        
        [_tabsView scrollRectToVisible:frame animated:NO];
    }
}

@end
