//
//  ViewPagerTab.h
//  espace sante
//
//  Created by abdel ali on 02/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewPagerTab : UIView

@property (nonatomic, getter = isSelected) BOOL selected;
@property (nonatomic) UIColor *indicatorColor;

@end
