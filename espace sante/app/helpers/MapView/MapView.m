//
//  MapViewController.m
//  WAFAASSURANCE
//
//  Created by abdel ali on 08/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "MapView.h"

@implementation MapView

@synthesize lineColor;

- (id) initWithFrame:(CGRect) frame
{
	self = [super initWithFrame:frame];
    
	if (self != nil)
    {
		self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
		[self.mapView setDelegate:self];
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading];
        
		[self addSubview:_mapView];
        
        CGRect rect = _mapView.frame;
        
        routeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
		routeView.userInteractionEnabled = NO;
		[self.mapView addSubview:routeView];
		
		self.lineColor = [UIColor colorWithWhite:0.2 alpha:0.5];
        
        CLLocationCoordinate2D centerCoord = { 33.581361,-7.623726 };
        [self.mapView setCenterCoordinate:centerCoord zoomLevel:7 animated:YES];
        
        UIPinchGestureRecognizer *pinch;
        pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
        pinch.delegate = self;
        [self.mapView addGestureRecognizer:pinch];
        
        CLLocationCoordinate2D  points[4];
        points[0] = CLLocationCoordinate2DMake(25.413509,-14.699707);
        points[1] = CLLocationCoordinate2DMake(25.736571,-12.164063);
        points[2] = CLLocationCoordinate2DMake(23.660626,-12.208009);
        points[3] = CLLocationCoordinate2DMake(23.780318,-14.897461);
        MKPolygon* poly = [MKPolygon polygonWithCoordinates:points count:4];
        [self.mapView addOverlay:poly];
        
        points[0] = CLLocationCoordinate2DMake(27.685048,-8.670959);
        points[1] = CLLocationCoordinate2DMake(27.604454,-8.673706);
        points[2] = CLLocationCoordinate2DMake(27.643694,-13.179817);
        points[3] = CLLocationCoordinate2DMake(27.695384,-13.158188);
        MKPolygon *line = [MKPolygon polygonWithCoordinates:points count:4];
        [self.mapView addOverlay:line];
	}
	return self;
}

#pragma mark -
#pragma mark Add notation

- (void)addNotation:(Place *)place
{
    PlaceMark* p = [[PlaceMark alloc] initWithPlace:place];
    [self.mapView addAnnotation:p];
    
    MKCoordinateRegion mapRegion;
    mapRegion.center.latitude = place.latitude;
    mapRegion.center.longitude = place.longitude;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    [self.mapView setRegion:mapRegion animated: YES];
    [self.mapView regionThatFits:mapRegion];
    
    p = nil;
}

#pragma mark -
#pragma mark Zoom to fit annnotation

-(void)zoomToFitMapAnnotations
{
    if([self.mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(PlaceMark* annotation in self.mapView.annotations)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.5;
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.5;
    
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:YES];
}

#pragma mark -
#pragma mark Calculate routes and decode polyline

- (NSArray*)calculateRoutesFrom:(CLLocationCoordinate2D)f to:(CLLocationCoordinate2D)t
{
	NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
	NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
	
	NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
	
    NSError *error;
    NSString *apiResponse = [NSString stringWithContentsOfURL:[NSURL URLWithString:apiUrlStr]
                                                     encoding:NSUTF8StringEncoding error:&error];
    
	NSString* encodedPoints = [apiResponse stringByMatching:@"points:\\\"([^\\\"]*)\\\"" capture:1L];
	
	return [self decodePolyLine:[encodedPoints mutableCopy]];
}

- (NSMutableArray *)decodePolyLine:(NSMutableString *)encoded
{
    // encoded string
	[encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\" options:NSLiteralSearch
                                  range:NSMakeRange(0, [encoded length])];
    
	NSInteger len = [encoded length];
	NSInteger index = 0;
	NSMutableArray *array = [[NSMutableArray alloc] init];
	
    // lat & lng
    NSInteger lat=0;
	NSInteger lng=0;
    
	while (index < len)
    {
		NSInteger b;
		NSInteger shift = 0;
		NSInteger result = 0;
        
		do {
			b = [encoded characterAtIndex:index++] - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lat += dlat;
		shift = 0;
		result = 0;
		
        do {
			b = [encoded characterAtIndex:index++] - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lng += dlng;
		NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
		NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
		
		CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue]
                                                     longitude:[longitude floatValue]];
		[array addObject:loc];
        
        latitude = nil;
        longitude = nil;
        loc = nil;
	}
	
	return array;
}

#pragma mark -
#pragma mark center map

- (void)showRouteFrom:(Place*)f to:(Place*)t
{
    [self.mapView showsUserLocation];
    
	if(routes)
    {
		[self.mapView removeAnnotations:[self.mapView annotations]];
		routes = nil;
	}
	
	PlaceMark* from = [[PlaceMark alloc] initWithPlace:f];
    PlaceMark* to = [[PlaceMark alloc] initWithPlace:t];
	
	//[self.mapView addAnnotation:from];
	[self.mapView addAnnotation:to];
	
	routes = [self calculateRoutesFrom:from.coordinate to:to.coordinate];
	
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateRouteView];
        [self centerMap];
    });
}

- (void)centerMap
{
	MKCoordinateRegion region;

	CLLocationDegrees maxLat = -90;
	CLLocationDegrees maxLon = -180;
	CLLocationDegrees minLat = 90;
	CLLocationDegrees minLon = 180;
    
	for(int idx = 0; idx < routes.count; idx++)
	{
		CLLocation* currentLocation = [routes objectAtIndex:idx];
		if(currentLocation.coordinate.latitude > maxLat)
			maxLat = currentLocation.coordinate.latitude;
		if(currentLocation.coordinate.latitude < minLat)
			minLat = currentLocation.coordinate.latitude;
		if(currentLocation.coordinate.longitude > maxLon)
			maxLon = currentLocation.coordinate.longitude;
		if(currentLocation.coordinate.longitude < minLon)
			minLon = currentLocation.coordinate.longitude;
	}
	
    region.center.latitude     = (maxLat + minLat) / 2;
	region.center.longitude    = (maxLon + minLon) / 2;
	region.span.latitudeDelta  = maxLat - minLat;
	region.span.longitudeDelta = maxLon - minLon;
	
	[self.mapView setRegion:region animated:YES];
}

- (void)updateRouteView
{
	CGContextRef context = 	CGBitmapContextCreate(nil, 
												  routeView.frame.size.width, 
												  routeView.frame.size.height, 
												  8, 
												  4 * routeView.frame.size.width,
												  CGColorSpaceCreateDeviceRGB(),
												  kCGImageAlphaPremultipliedLast);
	
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor);
	CGContextSetRGBFillColor(context, 0.0, 0.0, 1.0, 1.0);
	CGContextSetLineWidth(context, 3.0);
	
	for(int i = 0; i < routes.count; i++) {
		CLLocation* location = [routes objectAtIndex:i];
		CGPoint point = [self.mapView convertCoordinate:location.coordinate toPointToView:routeView];
		
		if(i == 0) {
			CGContextMoveToPoint(context, point.x, routeView.frame.size.height - point.y);
		} else {
			CGContextAddLineToPoint(context, point.x, routeView.frame.size.height - point.y);
		}
	}
	
	CGContextStrokePath(context);
	
	CGImageRef image = CGBitmapContextCreateImage(context);
	UIImage* img = [UIImage imageWithCGImage:image];
	
	routeView.image = img;
    
	CGContextRelease(context);
}

#pragma mark - mapView delegate functions

- (void)mapView:(MKMapView *)mapView_ regionWillChangeAnimated:(BOOL)animated
{
	routeView.hidden = YES;
}

- (void)mapView:(MKMapView *)mapView_ regionDidChangeAnimated:(BOOL)animated
{
	[self updateRouteView];
	routeView.hidden = NO;
	[routeView setNeedsDisplay];
    
    if([_mapView getZoomLevel] < 6)
    {
        [self.mapView setCenterCoordinate:[_mapView centerCoordinate] zoomLevel:6 animated:NO];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKAnnotationView *pinView = nil;
    
    if(annotation != mapView.userLocation)
    {
        static NSString *defaultPin = @"pin";
        
        pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPin];
        
        if ( pinView == nil )
        {
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPin];
        }
        
        UIButton *phone = [UIButton buttonWithType:UIButtonTypeCustom];
        [phone setImage:[UIImage imageNamed:@"telephone.png"] forState:UIControlStateNormal];
        [phone setFrame:CGRectMake(0, 0, 39, 39)];
        [phone addTarget:annotation action:@selector(didTapDisclosureButton) forControlEvents:UIControlEventTouchUpInside];
        
        pinView.canShowCallout = YES;
        pinView.leftCalloutAccessoryView = phone;
        pinView.image = [UIImage imageNamed:@"pin.png"];
    }
    
    return pinView;
}

#pragma mark -
#pragma delegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    MKPolygonView *polygonView = [[MKPolygonView alloc] initWithPolygon:overlay];
    polygonView.lineWidth = 1.0;
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if(version >= 7.0)
        [polygonView setFillColor:[UIColor colorWithRed:249.0/255.0 green:245.0/255.0 blue:237.0/255.0 alpha:1.0]];
    else if(version >= 6.0 && version < 7.0)
        [polygonView setFillColor:[UIColor colorWithRed:248.0/255.0 green:242.0/255.0 blue:223.0/255.0 alpha:1.0]];
    
    return polygonView;
}

#pragma mark -
#pragma mark Gesture

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)pinch:(UIGestureRecognizer *)sender
{
    if([_mapView getZoomLevel] < 7)
    {
        [self.mapView setCenterCoordinate:[_mapView centerCoordinate] zoomLevel:7 animated:NO];
    }
}


@end
