//
//  Place.h
//  WAFAASSURANCE
//
//  Created by abdel ali on 08/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//


#import <Foundation/Foundation.h>

typedef void (^ActionBlock)(void);

@interface Place : NSObject {

	NSString* name;
	NSString* description;
	double latitude;
	double longitude;
    ActionBlock disclosureBlock;
}

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* description;
@property (readwrite, copy) ActionBlock disclosureBlock;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

- (id) initWithAction:(ActionBlock)block;

@end
