//
//  PlaceMark.m
//  WAFAASSURANCE
//
//  Created by abdel ali on 08/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "PlaceMark.h"


@implementation PlaceMark

@synthesize coordinate;
@synthesize place;

-(id) initWithPlace:(Place*)p
{
	self = [super init];
	if (self != nil)
    {
		coordinate.latitude = p.latitude;
		coordinate.longitude = p.longitude;
        
		self.place = p;
	}
	return self;
}

// subtitle of annotation
- (NSString *)subtitle
{
	return self.place.description;
}

// title of annotation
- (NSString *)title
{
	return self.place.name;
}

- (void)didTapDisclosureButton
{
    if (self.place.disclosureBlock)
        self.place.disclosureBlock();
}

@end
