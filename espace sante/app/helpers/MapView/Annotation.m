//
//  Annotation.m
//  espace sante
//
//  Created by abdel ali on 4/16/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "Annotation.h"

@implementation DraggableAnnotationView

@synthesize mapView, fingerPoint;

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.image = [UIImage imageNamed:@"pin.png"];
        self.draggable = YES;
    }
    return self;
}

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent*)event
{
    
    fingerPoint = point;
    return [super hitTest:point withEvent:event];
}


- (void)setDragState:(MKAnnotationViewDragState)newDragState animated:(BOOL)animated
{
    if(mapView){
        id<MKMapViewDelegate> mapDelegate = (id<MKMapViewDelegate>)mapView.delegate;
        [mapDelegate mapView:mapView annotationView:self didChangeDragState:newDragState fromOldState:self.dragState];
    }
    
    CGFloat liftValue = -(fingerPoint.y - self.frame.size.height - kFingerSize);
    
    if (newDragState == MKAnnotationViewDragStateStarting)
    {
        CGPoint endPoint = CGPointMake(self.center.x,self.center.y-liftValue);
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.center = endPoint;
                         }
                         completion:^(BOOL finished){
                             self.dragState = MKAnnotationViewDragStateDragging;
                         }];
        
    }
    else if (newDragState == MKAnnotationViewDragStateEnding)
    {
        __block CGPoint endPoint = CGPointMake(self.center.x,self.center.y-liftValue);
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.center = endPoint;
                         }
                         completion:^(BOOL finished){
                             endPoint = CGPointMake(self.center.x,self.center.y+liftValue);
                             self.dragState = MKAnnotationViewDragStateNone;
                         }];
    }
    else if (newDragState == MKAnnotationViewDragStateCanceling)
    {
        __block CGPoint endPoint = CGPointMake(self.center.x,self.center.y+liftValue);
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.center = endPoint;
                         }
                         completion:^(BOOL finished){
                             self.dragState = MKAnnotationViewDragStateNone;
                         }];
    }
}

@end
