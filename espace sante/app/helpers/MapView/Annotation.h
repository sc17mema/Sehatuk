//
//  Annotation.h
//  espace sante
//
//  Created by abdel ali on 4/16/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <RegexKitLite.h>

#define kFingerSize 15.0
#define METERS_PER_MILE 1609.344


@interface DraggableAnnotationView : MKAnnotationView

@property (nonatomic) CGPoint fingerPoint;
@property (nonatomic, assign) MKMapView* mapView;

@end

