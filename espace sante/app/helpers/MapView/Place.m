//
//  Place.m
//  WAFAASSURANCE
//
//  Created by abdel ali on 08/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Place.h"


@implementation Place

@synthesize name;
@synthesize description;
@synthesize latitude;
@synthesize longitude;
@synthesize disclosureBlock;

- (id) initWithAction:(ActionBlock)block
{
	self = [super init];
	if (self != nil)
    {
		self.disclosureBlock = block;
	}
	return self;
}

@end
