//
//  Resources.h
//  espace sante
//
//  Created by abdel ali on 30/08/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Resources : NSObject

+ (UIImage *)loadImage:(NSString *)name;

+ (NSObject *)getUserDefaults:(NSString *) name;
+ (void)setUserDefaults:(NSObject *)defaults forKey:(NSString *)key;

@end
