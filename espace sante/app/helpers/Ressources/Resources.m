//
//  Resources.m
//  espace sante
//
//  Created by abdel ali on 30/08/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Resources.h"

@implementation Resources

+ (UIImage *)loadImage:(NSString *)name{
    NSString *realName = name;
	NSString *path = [[NSBundle mainBundle] pathForResource:realName ofType:@"png"];
    
	return [[UIImage alloc] initWithContentsOfFile:path];
}

+ (NSObject *)getUserDefaults:(NSString *)name{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:name];
}

+ (void)setUserDefaults:(NSObject *)defaults forKey:(NSString *)key{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:defaults forKey:key];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

@end
