//
//  Security.h
//  espace sante
//
//  Created by abdel ali on 30/08/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Security : NSObject

+(NSString *)md5:(NSString *) str;
+(NSString *)fileMd5:(NSString *) path;

@end
