#import <UIKit/UIKit.h>

@interface StepperView : UIView{
    CGFloat cornerRadious;
    CGFloat value;
    NSInteger minRange;
    NSInteger maxRange;
    UIColor *stepperColor, *stepperDisableColor;
}

#pragma mark - Stepper Configration methods
- (void)setStepperColor:(UIColor *)color withDisableColor:(UIColor *)disColor;
- (void)setTextLabelFont:(UIFont *)font;
- (void)setTextColor:(UIColor *)color;
- (void)setStepperRange:(float)minValue andMaxValue:(float)maxValue;
- (void)setValue:(float)defValue;
- (float)getValue;

@end
