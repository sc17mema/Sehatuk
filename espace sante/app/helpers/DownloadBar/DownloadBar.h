//
//  DownloadBar.h
//  espace sante
//
//  Created by abdel ali on 09/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

//@class UIProgressView;
@protocol DownloadBarDelegate;

@interface DownloadBar : NSObject<NSURLConnectionDataDelegate>
{
	NSMutableURLRequest *_download_request;
	NSURLConnection *_download_connection;
	NSMutableData *_received_data;
    NSString *_file_name;
    
	id<DownloadBarDelegate> _delegate;
	long long _bytes_received;
	long long _expected_bytes;
	
	float percent_complete;
    
    MBProgressHUD *_activity_indicator;
}

- (DownloadBar *)initWithURL:(NSURL *)fileURL timeout:(NSInteger)timeout delegate:(id<DownloadBarDelegate>)delegate_;

@property (nonatomic, readonly) NSMutableData* received_data;
@property (nonatomic, readonly, retain) NSMutableURLRequest* download_request;
@property (nonatomic, readonly, retain) NSURLConnection* download_connection;
@property (nonatomic, assign) id<DownloadBarDelegate> delegate;
@property (nonatomic, readonly) float percent_complete;

@end



