//
//  DownloadBar.m
//  espace sante
//
//  Created by Nimit Parekh on 12/9/12.
//  Copyright 2012 http://sugartin.info. All rights reserved.
//

#import "DownloadBar.h"
#import "DownloadBarDelegate.h"

@implementation DownloadBar

@synthesize download_connection, download_request, received_data, delegate, percent_complete;

- (DownloadBar *)initWithURL:(NSURL *)fileURL timeout:(NSInteger)timeout delegate:(id<DownloadBarDelegate>)delegate_{

	self = [super init];
	
    if(self)
    {
        self.delegate = delegate_;
        
        _bytes_received = _expected_bytes = 0;
        _file_name = [[[fileURL absoluteString] lastPathComponent] copy];
        received_data = [[NSMutableData alloc] initWithLength:0];
        
//        self.backgroundColor = [UIColor clearColor];
//        self.progress = 0.0;
        
        download_request = [[NSMutableURLRequest alloc] initWithURL:fileURL
                                                        cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                    timeoutInterval:timeout];
        [download_request addValue:@"Token token=\"afbadb4ff8485c0adcba486b4ca90cc4\"" forHTTPHeaderField:@"Authorization"];
        
        download_connection = [[NSURLConnection alloc] initWithRequest:download_request delegate:self];
        
        if(!download_connection)
        {
            [self.delegate download:self
                   didFail:[NSError errorWithDomain:@"DownloadBar Error" code:1
                                                    userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"NSURLConnection Failed", NSLocalizedDescriptionKey, nil]]];
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        _activity_indicator = [MBProgressHUD showHUDAddedTo:((UIViewController *)self.delegate).view animated:YES];
        _activity_indicator.mode = MBProgressHUDModeIndeterminate;
        _activity_indicator.labelText = @"Chargement...";
            
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
	}

	return self;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.delegate download:self didFail:error];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [_activity_indicator hide:YES];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _expected_bytes = [response expectedContentLength];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    NSInteger length_received_data = [data length];
    _bytes_received = _bytes_received + length_received_data;
    [received_data appendData:data];
    
    if(_expected_bytes != NSURLResponseUnknownLength)
    {
        //self.progress = ((_bytes_received / (float)_expected_bytes) * 100)/100;
        percent_complete = _activity_indicator.progress * 100;
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self.delegate download:self didFinishWithData:received_data suggestedFilename:_file_name];
    
    //[[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [_activity_indicator hide:YES];
}


@end
