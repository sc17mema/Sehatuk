//
//  DownlaodDelegate.h
//  espace sante
//
//  Created by abdel ali on 24/10/2013.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadBar.h"

@protocol DownloadBarDelegate<NSObject>
@optional
- (void)download:(DownloadBar *)downloadBar didFinishWithData:(NSData *)fileData suggestedFilename:(NSString *)filename;
- (void)download:(DownloadBar *)downloadBar didFail:(NSError *)error;
- (void)updated:(DownloadBar *)downloadBar;
@end
