//
//  UIViewController+PopViewController.h
//  My Wafa
//
//  Created by abdel ali on 2/3/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PopupView;

typedef enum {
    PopupViewAnimationFade = 0,
    PopupViewAnimationSlideBottomTop = 1,
} PopupViewAnimation;


@interface UIViewController (PopViewController)

@property (nonatomic, retain) UIViewController *popupViewController;
@property (nonatomic, retain) PopupView *popupView;

- (void)presentPopupViewController:(UIViewController*)popupViewController animationType:(PopupViewAnimation)animationType;
- (void)presentPopupViewController:(UIViewController*)popupViewController animationType:(PopupViewAnimation)animationType dismissed:(void(^)(void))dismissed;
- (void)dismissPopupViewControllerWithanimationType:(PopupViewAnimation)animationType;

@end

