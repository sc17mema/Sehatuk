#import "AutoCompleteListView.h"

@implementation UIView (UIShadowBorderEffects)

+ (void)shadowBorder:(UIView *)view
{
    view.layer.masksToBounds = NO;
    view.layer.shadowOpacity = 1.0;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    [view.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    view.layer.shadowRadius = 5.0;
}

@end


@implementation AutoCompleteListView

@synthesize tableView = _tableView;
@synthesize boundView = _boundView;
@synthesize isShowing = _isShowing;

- (void)dealloc
{
    self.dataSource = nil;
    self.delegate = nil;
    
    _tableView.dataSource = nil;
    _tableView.delegate = nil;
}

- (id)initWithBoundView:(UIView *)boundView dataSource:(id)datasource delegate:(id)delegate
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGRect frame = CGRectMake(0.0f, 0.0f, screenBounds.size.width, screenBounds.size.height);
    
    self = [super initWithFrame:frame];
    if (self)
    {
        self.dataSource = datasource;
        self.delegate = delegate;
        
        _boundView = boundView;
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.clipsToBounds = YES;
        _tableView.layer.cornerRadius = 10.0f;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        //mTableView.backgroundColor = [UIColor clearColor];
        
        [UIView shadowBorder:_tableView];
        
        [self addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        
        _isShowing = NO;
    }
    
    return self;
}

- (void)bindToView:(UIView *)boundView
{
    if (_boundView) {
        _boundView = nil;
    }
    
    _boundView = boundView;
}

- (void)show
{
    if (_isShowing) {
        return;
    }
    
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *boundViewSuperView = _boundView.superview;
    CGRect boundViewframe = _boundView.frame;
    
    CGPoint origin = (CGPoint){CGRectGetMinX(boundViewframe), CGRectGetMaxY(boundViewframe)};
    origin = [boundViewSuperView convertPoint:origin toView:mainWindow.rootViewController.view];
    
    _tableView.frame = CGRectMake(origin.x, origin.y, CGRectGetWidth(boundViewframe), 0.0f);

    [mainWindow addSubview:self];
    [mainWindow addSubview:_tableView];
    
    [UIView animateWithDuration:kDefalutPopupAnimationDuration
                     animations:^{
//                         _tableView.frame = CGRectMake(CGRectGetMinX(boundViewframe),
//                                                       CGRectGetMaxY(boundViewframe),
//                                                       CGRectGetWidth(boundViewframe),
//                                                       kDefaultPopupListViewHeight);
                         
                         _tableView.frame = CGRectMake(origin.x, origin.y - kDefaultPopupListViewHeight - CGRectGetHeight(_boundView.frame), CGRectGetWidth(boundViewframe), kDefaultPopupListViewHeight);
                     } completion:^(BOOL finished) {
                     }];
    
    _isShowing = YES;
    _tableView.layer.cornerRadius = 10.0f;
    _tableView.clipsToBounds = YES;
    
    [_tableView reloadData];
}

- (void)dismiss
{
    if (!_isShowing) {
        return;
    }
    
    CGRect boundViewframe = _boundView.frame;
    
    [UIView animateWithDuration:kDefalutPopupAnimationDuration
                     animations:^{
                     } completion:^(BOOL finished) {
                         
                         _tableView.frame = CGRectMake(CGRectGetMinX(boundViewframe), CGRectGetMaxY(boundViewframe), CGRectGetWidth(boundViewframe), 0.0f);
            
                         [_tableView removeFromSuperview];
                         [self removeFromSuperview];
                     }];
    
        _isShowing = NO;
}

- (void)reloadListData
{
    [_tableView reloadData];
}

- (void)backTapAction:(id)sender
{
    [self dismiss];
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(itemCellHeight:)]) {
        return [self.dataSource itemCellHeight:indexPath];
    }
    return 0.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(clickedListViewAtIndexPath:)]) {
        [self.delegate clickedListViewAtIndexPath:indexPath];
    }
    [self dismiss];
}

#pragma mark -- UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(numberOfSections)]) {
        [self.dataSource numberOfSections];
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(numberOfRowsInSection:)]) {
        return [self.dataSource numberOfRowsInSection:section];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(itemCell:)]) {
        return [self.dataSource itemCell:indexPath];
    }
    return nil;
}

@end