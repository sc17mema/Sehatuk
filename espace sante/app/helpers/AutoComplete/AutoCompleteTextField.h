#import <UIKit/UIKit.h>

@interface AutoCompleteTextField : UITextField

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
