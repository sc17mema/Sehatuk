#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#define kDefaultPopupListViewHeight 150.0f
#define kCenterPopupListViewHeight 300.0f
#define kCenterPopupListViewWidth  300.0f

#define kDefalutPopupAnimationDuration 0.35f

// border shadow
@interface UIView (UIShadowBorderEffects)
+ (void)shadowBorder:(UIView *)view;
@end

// delegate
@protocol PopupListViewDelegate <NSObject>
@optional
- (void)clickedListViewAtIndexPath:(NSIndexPath *)indexPath;
@end

// datasource
@protocol PopupListViewDataSource <NSObject>
@required
- (UITableViewCell *)itemCell:(NSIndexPath *)indexPath;
- (NSInteger)numberOfRowsInSection:(NSInteger)section;
@optional
- (NSInteger)numberOfSections;
- (CGFloat)itemCellHeight:(NSIndexPath *)indexPath;
- (NSString *)titleInSection:(NSInteger)section;
@end

@interface AutoCompleteListView : UIControl  <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) id<PopupListViewDelegate> delegate;
@property (nonatomic, assign) id<PopupListViewDataSource> dataSource;
@property (nonatomic, retain) UIView *boundView;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, assign) BOOL isShowing;

- (id)initWithBoundView:(UIView *)boundView dataSource:(id)datasource delegate:(id)delegate;
- (void)bindToView:(UIView *)boundView;
- (void)show;
- (void)dismiss;
- (void)reloadListData;

@end
