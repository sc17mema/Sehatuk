//
//  Infos.m
//
//  Created by abdel ali on 31/12/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//


#import "Infos.h"

@interface Infos()
@property (nonatomic, strong) NSArray *arrDatasource;
@end

@implementation Infos {
    id ctrlReference;
    UIImageView *imgMark;
}

- (void)viewDidLoad
{
    [self initView];
    [super viewDidLoad];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"Info Santé");
    [super viewDidAppear:animated];
    
    CGFloat pagerTabWith = CGRectGetWidth(self.view.frame) / _arrDatasource.count;
    
    imgMark = [[UIImageView alloc] initWithFrame:CGRectMake((pagerTabWith / 2) - 10, self.tabLocation + self.tabHeight - 5, 20, 16)];
    [imgMark setImage:[UIImage imageNamed:@"indicateur.png"]];
    [imgMark setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:imgMark];
}


#pragma mark - general settings

- (void)initView
{
    UIImage *imgCtrlIcon = [[UIImage imageNamed:@"actu_sante.png"] imageByScalingProportionallyToSize:(CGSize){40, 40}];
    
    [btnCtrlName.titleLabel setFont:[UIFont fontWithName:@"DroidSans" size:14]];
    [btnCtrlName setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCtrlName setTitle:SLocalizedString(@"Info-Santé", nil) forState:UIControlStateNormal];
    [btnCtrlName setImage:imgCtrlIcon forState:UIControlStateNormal];
    
    // Pager datasource & delegate refs
    self.delegate = self;
    self.dataSource = self;
    
    // Pager datasource array
    self.arrDatasource = @[@{@"id" : @"news", @"title" : @"Actualités", @"image" : @"actu_sante.png"},
                           @{@"id" : @"conseils", @"title" : @"Conseils", @"image" : @"conseils.png"},
                           @{@"id" : @"pre-diagnostic", @"title" : @"Diagnostic", @"image" : @"info-sante.pre-diagnostic.icon.png"}];
}

#pragma mark - pager delegate & datasource funcs

- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager
{
    return _arrDatasource.count;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UIImageView *imgTab = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [imgTab setImage:[UIImage imageNamed:@"bouton_normal.png"]];
    [imgTab setContentMode:UIViewContentModeScaleAspectFit];
    
    UIImageView *imgTabIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [imgTabIcon setImage:[UIImage imageNamed:_arrDatasource[index][@"image"]]];
    [imgTabIcon setContentMode:UIViewContentModeScaleAspectFit];
    
    [imgTab addSubview:imgTabIcon];
    
    return imgTab;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index
{
    UIViewController *ctrl = [self.storyboard instantiateViewControllerWithIdentifier:_arrDatasource[index][@"id"]];
    ctrl.title = SLocalizedString(_arrDatasource[index][@"title"], nil);
    
    return ctrl;
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value
{
    switch (option) {
        case ViewPagerOptionTabLocation:
            return 88;
            break;
        case ViewPagerOptionTabWidth:
            return CGRectGetWidth(self.view.frame) / _arrDatasource.count;
            break;
        case ViewPagerOptionTabHeight:
            return 50;
            break;
        default:
            break;
    }
    
    return value;
}
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerIndicator:
            return [[UIColor redColor] colorWithAlphaComponent:0.64];
            break;
        case ViewPagerTabsView:
            return [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
        default:
            break;
    }
    
    return color;
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index
{
    ctrlReference = viewPager.pageViewController.viewControllers[0];
    NSLog(@"I changed the tab %@", self.arrDatasource[index]);
}

- (void)viewPager:(ViewPagerController *)viewPager didAnimateTab:(CGFloat)position
{
    float x = position - [self.tabsView contentOffset].x - 15;
    
    CGRect frame = imgMark.frame;
    
    [UIView beginAnimations:@"move" context:nil];
    [UIView setAnimationDuration:0.2];
    
    frame.origin.x = x + self.tabOffset;
    imgMark.frame = frame;
    
    [UIView commitAnimations];
}

#pragma mark - actions

- (IBAction)pop:(id)sender
{
    UIViewController *actualSuperView = [self getSuperView:ctrlReference];
    
    if([actualSuperView isEqual:ctrlReference])
    {
        [self.navigationController popViewControllerAnimated:YES];
        [self setActiveTabIndex:0];
        return;
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    transition.delegate = self;
    
    [actualSuperView.view.superview.layer addAnimation:transition forKey:nil];
    [actualSuperView.view removeFromSuperview];
    [actualSuperView willMoveToParentViewController:nil];
    [actualSuperView removeFromParentViewController];
}

- (UIViewController *)getSuperView:(UIViewController *)parent
{
    if(parent.childViewControllers.count > 0)
        return [self getSuperView:[parent.childViewControllers lastObject]];
    
    return parent;
}

@end
