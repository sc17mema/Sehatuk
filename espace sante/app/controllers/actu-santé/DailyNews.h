#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "News.h"
#import "Top5.h"
#import "Resources.h"
#import "Message.h"
#import "WS.h"
#import "DailyNewsDetails.h"
#import "LocalizationSystem.h"

@interface DailyNews : UIViewController<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>
{
    NSInteger _currentPage;
    NSInteger _totalPages;
    NSTimer *animation;
    int counter;
}

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITableView *news;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UIScrollView *top5;
@property (weak, nonatomic) IBOutlet UIPageControl *pagination;

@property (strong, nonatomic) NSMutableArray * views;
@property (strong, nonatomic) NSMutableArray* dailynews;
@property (strong, nonatomic) NSArray* panorama;

@end
