#import <UIKit/UIKit.h>

// Cells
#import "DiseaseCell.h"

#import "WebViewController.h"

@interface Diseases : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITableView *tblDiseases;
    __weak IBOutlet UILabel *lblCtrlName;
}

@property (nonatomic, strong) NSArray *dataList;

@end
