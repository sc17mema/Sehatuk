#import <UIKit/UIKit.h>

@interface Top5 : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *headline;
@property (weak, nonatomic) IBOutlet UILabel *sum;
@end
