//
//  Diseases.m
//  espace sante
//
//  Created by Abdel Ali on 05/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "Diseases.h"

@implementation Diseases

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    lblCtrlName.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    lblCtrlName.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    lblCtrlName.text = self.title;
}

#pragma mark - UITableView Delegates & Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    DiseaseCell *cell = (DiseaseCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[DiseaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.lblDiseaseName.text = self.dataList[indexPath.row][@"nom"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WebViewController *webview = [self.storyboard instantiateViewControllerWithIdentifier:@"webview"];
    webview.html = self.dataList[indexPath.row][@"detail"];
    
    [self addChildViewController:webview];
}

#pragma mark - Utils

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    // Add the view to the parent view and position it if you want
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    CGRect rect = CGRectMake(0, 0, parent.view.frame.size.width, parent.view.frame.size.height);
    self.view.frame = rect;
    
    [[parent view] addSubview:[self view]];
}

@end
