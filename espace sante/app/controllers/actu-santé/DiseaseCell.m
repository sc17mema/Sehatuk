//
//  DiseaseCell.m
//  espace sante
//
//  Created by Abdel Ali on 05/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "DiseaseCell.h"

@implementation DiseaseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    // Initialization code
    
    self.lblDiseaseName.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    self.lblDiseaseName.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
}

- (void)prepareForReuse
{
    self.backgroundColor = [UIColor clearColor];
}

@end
