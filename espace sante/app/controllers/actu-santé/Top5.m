#import "Top5.h"

@implementation Top5

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.headline.font = [UIFont fontWithName:@"DroidSans-Bold" size:13];
    self.sum.font = [UIFont fontWithName:@"DroidSans" size:11];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
