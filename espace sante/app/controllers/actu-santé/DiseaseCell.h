#import <UIKit/UIKit.h>

@interface DiseaseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgDisclosur;
@property (weak, nonatomic) IBOutlet UILabel *lblDiseaseName;

@end
