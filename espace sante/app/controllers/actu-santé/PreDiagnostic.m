//
//  PreDiagnostic.m
//  espace sante
//
//  Created by Abdel Ali on 05/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "PreDiagnostic.h"

@interface PreDiagnostic () {
    
    AutoCompleteListView *dropDownListView;
    NSMutableArray *suggestions;
    NSArray *suggestionsDataList;
    NSArray *diseasesDataList;
    NSUInteger symptomIdentifier;
}

@end

@implementation PreDiagnostic

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
    [self initUI];
    [self asyncSymptoms];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - general settings

- (void)initView
{
    lblCtrlName.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    lblCtrlName.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    lblCtrlName.text = SLocalizedString(@"Diagnostic", nil);
    
    suggestions = [NSMutableArray array];
    diseasesDataList = [NSArray array];
    suggestionsDataList = [NSArray array];
}

- (void)initUI
{
    dropDownListView = [[AutoCompleteListView alloc] initWithBoundView:txtAutoComplete dataSource:self delegate:self];
    [txtAutoComplete addTarget:self action:@selector(textDidChanged:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - AutoCompleteListViewDataSource & AutoCompleteListViewDelegate

- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    return suggestions.count;
}

- (CGFloat)itemCellHeight:(NSIndexPath *)indexPath
{
    return 44.0f;
}

- (void)clickedListViewAtIndexPath:(NSIndexPath *)indexPath
{
    [txtAutoComplete setText:suggestions[indexPath.row][@"nom"]];
    symptomIdentifier = [suggestions[indexPath.row][@"id"] integerValue];
}

- (UITableViewCell *)itemCell:(NSIndexPath *)indexPath
{
    if (suggestions.count == 0) {
        return nil;
    }
    
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.textLabel.text = suggestions[indexPath.row][@"nom"];
    cell.textLabel.font = [UIFont fontWithName:@"DroidSans" size:12];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.numberOfLines = 0;
    
    return cell;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textDidChanged:(id)sender
{
    UITextField *textField = (UITextField *)sender;

    if (![self isNullOrEmpty:textField.text] && suggestionsDataList.count > 0) {
        
        [self createListData:textField.text];
        
        [dropDownListView show];
        [dropDownListView reloadListData];
    }
    
}

#pragma mark - Utils

- (BOOL)isNullOrEmpty:(NSString *)s
{
    if(s == nil || s.length == 0 || [s isEqualToString:@""] || [s isEqualToString:@"(null)"]) {
        return YES;
    }
    
    return NO;
}

- (void)createListData:(NSString*)str
{
    [suggestions removeAllObjects];
    
    if(str.length > 0)
    {
        for(int i=0; i<suggestionsDataList.count; i++)
        {
            if([suggestionsDataList[i][@"nom"] rangeOfString:str options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [suggestions addObject:suggestionsDataList[i]];
            }
        }
    }
}

- (void)setTxtSymptomDetailText:(NSString *)text
{
    txtSymptomDetail.text = text;
    txtSymptomDetail.font = [UIFont fontWithName:@"DroidSans" size:14];
    txtSymptomDetail.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    txtSymptomDetail.backgroundColor = [UIColor clearColor];
    txtSymptomDetail.editable = NO;
}

- (void)asyncSymptoms
{
    // call an asynchronous HTTP request
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *symptomes = [Cache get:@"symptomes.json"];
        
        if (symptomes == nil) {
            NSURL * url = [[NSURL alloc] initWithString:@"http://ns29646.ovh.net:8001/sehatuk/v1/symptomes"];
            symptomes = [NSData dataWithContentsOfURL:url];
        }
    
        if(symptomes != nil)
        {
            NSArray * parsedResults = [NSJSONSerialization JSONObjectWithData:symptomes options:NSJSONReadingMutableContainers error: nil];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                suggestionsDataList = parsedResults;
            });
        }
    });
}

#pragma mark - Actions

- (IBAction)diseases:(id)sender
{
    Diseases *ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"maladies"];
    ctrl.title = lblCtrlName.text;
    ctrl.dataList = diseasesDataList;
    [self addChildViewController:ctrl];
}

- (IBAction)startSearch:(id)sender
{
    NSLog(@"Symptom: %@", txtAutoComplete.text);
    [Message load:self message:nil detail:nil view:nil delay:0];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *path = [NSString stringWithFormat:@"http://ns29646.ovh.net:8001/sehatuk/v1/symptomes/%d", symptomIdentifier];
        NSURL * url = [[NSURL alloc] initWithString:path];
        
        if([NSData dataWithContentsOfURL:url] != nil)
        {
            NSDictionary * parsedResults = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:url]
                                                                           options:NSJSONReadingMutableContainers error: nil];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [Message hide];
                [self setTxtSymptomDetailText:parsedResults[@"description"]];
                diseasesDataList = parsedResults[@"maladies"];
            });
        }
        else {
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [Message hide];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                                message:SLocalizedString(@"Erreur de connexion", nil)
                                                               delegate:nil cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            });
           
        }
    });
}

@end
