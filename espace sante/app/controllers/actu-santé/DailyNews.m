#import "DailyNews.h"

const int kLoadingCellTag = 200;

@implementation DailyNews

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.name.font = [UIFont fontWithName:@"Droid Sans" size:14];
    self.name.text = SLocalizedString(@"Actu-sante", nil);
    
    _currentPage = 1;
    self.dailynews = [NSMutableArray array];
    self.panorama = [NSArray array];
    
    [self fetchDailyNews];
    [Flurry logEvent:@"Actualités" timed:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    animation = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(scrolling:) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if([animation isValid])
    {
        [animation invalidate];
        animation = nil;
    }
    [super viewWillDisappear:animated];
    
}
- (void)scrolling:(NSTimer *)timer
{
    if(counter >= self.panorama.count) counter = 0;
    
    [self.top5 setContentOffset:CGPointMake(self.top5.frame.size.width*counter++, 0) animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - initialize

- (void)initialize
{
    // set up the page control
    self.pagination.currentPage = 0;
    self.pagination.numberOfPages = _panorama.count;
    
    // set up the array to hold the views for each page
    self.views = [NSMutableArray arrayWithCapacity:_panorama.count];
    for (NSInteger i = 0; i < _panorama.count; ++i)
        [self.views addObject:[NSNull null]];
    
    // set up scrollview size
    CGSize size = self.top5.frame.size;
    self.top5.contentSize = CGSizeMake(size.width * _panorama.count, size.height);
    self.top5.pagingEnabled = YES;
    self.top5.showsVerticalScrollIndicator = NO;
    self.top5.showsHorizontalScrollIndicator = NO;
    self.top5.delegate = self;
    
    // tap gesture
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(more:)];
    [self.top5 addGestureRecognizer:gesture];
    gesture = nil;
    
    // load the initial set of pages
    [self loadVisiblePages];
}

#pragma mark - fetch data


- (void)fetchDailyNews
{
    NSMutableURLRequest *request = [[WS api] requestWithMethod:@"get" path:[NSString stringWithFormat:@"actualites/%d.json", _currentPage] parameters:nil];
    
    AFJSONRequestOperation *operation = [[AFJSONRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operation, id responseObject) {
         
         _totalPages = [[responseObject objectForKey:@"total_pages"] intValue];
         
         for (id dic in [responseObject objectForKey:@"result"])
         {
             if (![self.dailynews containsObject:dic]) {
                 [self.dailynews addObject:dic];
             }
         }
         
         [self.news reloadData];
         
         if(_currentPage == 1)
         {
             self.panorama = [responseObject objectForKey:@"result"];
             
             [self initialize];
         }
         
     } failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                         message:SLocalizedString(@"Erreur de connexion", nil)
                                                        delegate:nil cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
         [alert show];
     }];
    
    [operation start];
}

#pragma mark - loading cell

- (UITableViewCell *)loadingCell
{
    UITableViewCell *cell = nil;
    UIActivityIndicatorView *activity = nil;
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activity.center = cell.center;
    [cell addSubview:activity];
    [activity startAnimating];
    
    activity = nil;
    cell.tag = kLoadingCellTag;
    
    return cell;
}

#pragma mark - table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_currentPage == 0)
        return 1;
    
    if (_currentPage < _totalPages)
        return self.dailynews.count + 1;
    
    return self.dailynews.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.dailynews.count)
    {
        static NSString *cellIdentifier = @"newCell";
        
        News *cell = (News *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil)
            cell = [[News alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        
        cell.headline.text = _dailynews[indexPath.row][@"title"];
        cell.body.text = _dailynews[indexPath.row][@"content"];
        cell.date.text = _dailynews[indexPath.row][@"date"];
        
        NSString *link = [@"http://ns29646.ovh.net:8001/images/" stringByAppendingString:_dailynews[indexPath.row][@"image"]];
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:link]];
        request.cachePolicy = NSURLRequestReturnCacheDataElseLoad; // trying explicitly setting cache policy
        [cell.image setImageWithURLRequest:request placeholderImage:[Resources loadImage:@"ic_launcher.png"]
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       
                                           cell.image.image = image;
                                    }
                                   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                   }];
        
        [cell setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        return cell;
    }
    else
    {
        return [self loadingCell];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (cell.tag == kLoadingCellTag)
    {
        _currentPage++;
        [self fetchDailyNews];
    }
    else
    {
        News *c = (News *)cell;
    
        c.headline.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
        c.headline.font = [UIFont fontWithName:@"DroidSans-Bold" size:12];
    
        c.body.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
        c.body.font = [UIFont fontWithName:@"DroidSans" size:11];
    
        c.date.textColor = [UIColor colorWithRed:134/255.0 green:138/255.0 blue:139/255.0 alpha:1.0];
        c.date.font = [UIFont fontWithName:@"DroidSans" size:11];
    
        if(indexPath.row %2 == 0)
            c.background.image = [UIImage imageNamed:@"border-even"];
        else
            c.background.image = [UIImage imageNamed:@"border-odd"];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self itemize:_dailynews[indexPath.row]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}

#pragma mark - navigation

- (IBAction)pop:(id)sender
{
    if(self.childViewControllers.count > 0)
    {
        UIViewController *ref = self.childViewControllers[0];
        
        CGRect rect = ref.view.frame;
        rect.origin.x += 320;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            ref.view.frame = rect;
            
        } completion:^(BOOL finished) {
            [ref.view removeFromSuperview];
            [ref willMoveToParentViewController:nil];
            [ref removeFromParentViewController];
        }];
        
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - scroll view

- (void)loadVisiblePages
{
    CGFloat pageWidth = self.top5.frame.size.width;
    
    NSInteger page = (NSInteger)floor((self.top5.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    counter = page;
    
    NSString *link = [@"http://ns29646.ovh.net:8001/images/" stringByAppendingString:_panorama[page][@"image"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:link]];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    [self.thumbnail setImageWithURLRequest:request placeholderImage:nil
                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                   self.thumbnail.image = image;
                               }
                               failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {}];
    
    
    self.pagination.currentPage = page;
    
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    for (NSInteger i=firstPage; i<=lastPage; i++) [self loadPage:i];
}

- (void)loadPage:(NSInteger)page
{
    if (page < 0 || page >= _panorama.count) return;
    
    UIView *pageView = [self.views objectAtIndex:page];
    
    if ((NSNull*)pageView == [NSNull null])
    {
        CGRect frame = self.top5.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        Top5 *rs = [self.storyboard instantiateViewControllerWithIdentifier:@"top5"];
        rs.view.frame = frame;
        
        rs.headline.text = _panorama[page][@"title"];
        rs.sum.text = _panorama[page][@"content"];
        
        [self.top5 addSubview:rs.view];
        [self.views replaceObjectAtIndex:page withObject:rs.view];
        
        rs = nil;
    }
    
    pageView = nil;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self loadVisiblePages];
}

- (void)purgePage:(NSInteger)page
{
    if (page < 0 || page >= _panorama.count) return;
    
    UIView *pageView = [self.views objectAtIndex:page];
    
    if ((NSNull*)pageView != [NSNull null])
    {
        [self.views replaceObjectAtIndex:page withObject:[NSNull null]];
        
        [pageView removeFromSuperview];
        pageView = nil;
    }
}

#pragma mark - functions

- (void)itemize:(NSDictionary *)data
{
    DailyNewsDetails * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"lemonde"];
    vc.datasource = data;
    
    [self addChildViewController:vc];
}

#pragma mark - more

- (void)more:(UITapGestureRecognizer *)gesture
{
    CGFloat pageWidth = self.top5.frame.size.width;
    NSInteger page = (NSInteger)floor((self.top5.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    [self itemize:_panorama[page]];
}

@end
