#import <UIKit/UIKit.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <FacebookSDK/FacebookSDK.h>
#import <MessageUI/MessageUI.h>
#import "UITextView+PinchZoom.h"
#import "Resources.h"
#import "Message.h"
#import "LocalizationSystem.h"

@interface DailyNewsDetails : UIViewController<UIScrollViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>
{
    UITextView *textView;
}

@property (weak, nonatomic) IBOutlet UILabel *headline;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UITextView *article;
@property (strong, nonatomic) NSDictionary *datasource;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

@end
