#import "DailyNewsDetails.h"

@implementation DailyNewsDetails

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.headline.font = [UIFont fontWithName:@"DroidSans-Bold" size:16];
    self.headline.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.headline.text = _datasource[@"title"];
    
    self.date.font = [UIFont fontWithName:@"DroidSans" size:10];
    self.date.textColor = [UIColor colorWithRed:134/255.0 green:138/255.0 blue:139/255.0 alpha:1.0];
    self.date.text = _datasource[@"date"];
    
    NSString *link = [@"http://ns29646.ovh.net:8001/images/" stringByAppendingString:_datasource[@"image"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:link]];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad; // trying explicitly setting cache policy
    [self.image setImageWithURLRequest:request placeholderImage:[Resources loadImage:@"ic_launcher.png"]
                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                   self.image.image = image;
                               }
                               failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {}];
    
    self.scrollView.maximumZoomScale = 4.0;
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.delegate = self;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    
    [self centerScrollViewContents];
}

- (void)viewWillAppear:(BOOL)animated
{
    if(textView == nil) {
        self.article.hidden = YES;
    
        textView = [[UITextView alloc] initWithFrame:self.article.frame];
        [self.view addSubview:textView];
    
        textView.text = _datasource[@"content"];
        textView.font = [UIFont fontWithName:@"DroidSans" size:14];
        textView.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
        textView.backgroundColor = [UIColor clearColor];
        textView.zoomEnabled = YES;
        textView.minFontSize = 12;
        textView.maxFontSize = 40;
        textView.editable = NO;
    }
    
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that you want to zoom
    return self.image;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so you need to re-center the contents
    [self centerScrollViewContents];
}

- (void)centerScrollViewContents {
    CGSize boundsSize = self.scrollView.bounds.size;
    CGRect contentsFrame = self.image.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.image.frame = contentsFrame;
}

#pragma mark - uiviewcontrolelr container

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    // Add the view to the parent view and position it if you want
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    CGRect rect = CGRectMake(0, 0, parent.view.frame.size.width, parent.view.frame.size.height);
    self.view.frame = rect;
    
    [[parent view] addSubview:[self view]];
}

- (IBAction)share
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:SLocalizedString(@"Annuler", nil)
                                         destructiveButtonTitle:nil otherButtonTitles:@"Facebook", @"E-mail", nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark - action sheet delegate

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex)
    {
        case 0:
            NSLog(@"Share News on Facebook");
            [self facebook];
            break;
        case 1:
            NSLog(@"Share News by email");
            [self email];
            break;
    }
}

#pragma mark - facebook

- (void)facebook
{
    [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"]
                                           defaultAudience:FBSessionDefaultAudienceEveryone
                                              allowLoginUI:YES
                                         completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                             if (!error) {
                                                 [self postToFacebookWall];
                                             }else{
                                                 if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled || [FBErrorUtility errorCategoryForError:error] == FBErrorCategoryPermissions) {
                                                     [Message warn:self message:@"Permission non accordée" delay:2];
                                                 } else {
                                                     [Message warn:self message:@"Quelque chose a mal tourné" delay:2];
                                                 }
                                             }
                                         }];
}

- (void)postToFacebookWall
{
    [Message load:self message:nil detail:nil view:nil delay:0];
    
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       @"http://sehatuk.dialy.net/", @"link",
                                       nil, @"picture",
                                       @"Sehatuk", @"name",
                                       @"Sehatuk est votre espace de santé", @"caption",
                                       @"Sehatuk fait partie d’une nouvelle série d’applications lancées par Dial Technologies.", @"description",
                                       nil];
    [params setObject:[@"A lu : " stringByAppendingString:textView.text] forKey:@"message"];
    [FBRequestConnection  startWithGraphPath:@"/me/feed" parameters:params HTTPMethod:@"POST"
                               completionHandler:^(FBRequestConnection *connection,id result,NSError *error) {
                                   
                                   if (!error) {
                                       [Message success:self message:nil delay:1];
                                   } else {
                                       if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled || [FBErrorUtility errorCategoryForError:error] == FBErrorCategoryPermissions) {
                                           [Message warn:self message:@"Permission non accordée" delay:2];
                                       } else {
                                           [Message warn:self message:@"Quelque chose a mal tourné" delay:2];
                                       }
                                   }
                                   
                               }];
}

- (void)email
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        [mailer setSubject:self.headline.text];
        [mailer setMessageBody:textView.text isHTML:NO];
        
        [self presentViewController:mailer animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:^{
    }];
}


@end
