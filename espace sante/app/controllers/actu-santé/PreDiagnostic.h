#import <UIKit/UIKit.h>

// Localization
#import "LocalizationSystem.h"

// Autocomplete UI
#import "AutoCompleteListView.h"
#import "AutoCompleteTextField.h"

// Message UI
#import "Message.h"

#import "Diseases.h"

#import "Cache.h"

@interface PreDiagnostic : UIViewController<UITextFieldDelegate, PopupListViewDataSource, PopupListViewDelegate>
{
    __weak IBOutlet UILabel *lblCtrlName;
    __weak IBOutlet UIButton *btnSickness;
    __weak IBOutlet UITextView *txtSymptomDetail;
    __weak IBOutlet AutoCompleteTextField *txtAutoComplete;
}

@end
