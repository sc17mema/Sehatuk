//
//  Result.h
//  espace sante
//
//  Created by abdel ali on 04/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Cell.h"
#import "Resources.h"
#import "Place.h"
#import "Map.h"
#import "Message.h"
#import "Detail.h"
#import "math.h"

@interface Result : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
}

@property (nonatomic, strong) NSString *nameValue;
@property (nonatomic, strong) NSMutableArray *datasource;
@property (nonatomic, strong) Place *from;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *num;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) Detail *detail;

- (void)setLikes:(NSDictionary *)data;

@end

