//
//  Pharmacies.m
//  espace sante
//
//  Created by abdel ali on 04/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Result.h"
#import <QuartzCore/QuartzCore.h>

@interface Result ()
@property (nonatomic, strong) Cell *ref;
@end

@implementation Result

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"DATASOURCE :: %@",self.datasource);
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = self.nameValue;
    self.nameValue = nil;
    
    self.num.font = [UIFont fontWithName:@"DroidSans-Bold" size:12];
    self.num.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.num.text = [NSString stringWithFormat:@"%d", _datasource.count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
//    [[DraggableAvatar shared] setVisible:YES];
//    [[DraggableAvatar shared] addObserver:self forKeyPath:@"hold" options:NSKeyValueObservingOptionNew context:nil];
//    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
//    [[DraggableAvatar shared] setVisible:NO];
//    [[DraggableAvatar shared] removeObserver:self forKeyPath:@"hold"];
//    [super viewDidDisappear:animated];
}

#pragma mark - settings

+ (void)willDisplayCell:(Cell *)cell
{
    cell.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:12];
    cell.name.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    
    cell.address.font = [UIFont fontWithName:@"DroidSans" size:11];
    cell.address.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    cell.phone.font = [UIFont fontWithName:@"DroidSans" size:10];
    cell.phone.textColor = [UIColor colorWithRed:187/255.0 green:187/255.0 blue:187/255.0 alpha:1.0];
    
    cell.likes.font = [UIFont fontWithName:@"DroidSans" size:10];
    cell.likes.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    
    cell.distance.font = [UIFont fontWithName:@"DroidSans" size:12];
    cell.distance.textColor = [UIColor colorWithRed:187/255.0 green:187/255.0 blue:187/255.0 alpha:1.0];
    
    cell.call.hidden = NO;
    cell.pin.image = [Resources loadImage:@"map_indicateur_notselected"];
}

+ (void)didSelectCell:(Cell *)cell
{
    cell.name.textColor = [UIColor colorWithRed:248/255.0 green:182/255.0 blue:0/255.0 alpha:1.0];
    cell.address.textColor = [UIColor whiteColor];
    cell.phone.textColor = [UIColor whiteColor];
    
    cell.pin.image = [Resources loadImage:@"map_indicateur_selected"];
    cell.call.hidden = NO;
}

#pragma mark - pharmacies

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    Cell *cell = (Cell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
        cell = [[Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    [Result willDisplayCell:cell];
    
    cell.name.text = _datasource[indexPath.row][@"nom"];
    cell.address.text = _datasource[indexPath.row][@"adresse"];
    
    if([self.name.text isEqualToString:SLocalizedString(@"Medecins", nil)])
    {
        NSString *activite = [_datasource[indexPath.row][@"activites"] stringByReplacingOccurrencesOfRegex:@"^Activités :\\s" withString:@""];
        cell.address.text = [cell.address.text stringByAppendingFormat:@"\n%@ : %@", SLocalizedString(@"Activité", nil), activite];
    }
    
    cell.phone.text = _datasource[indexPath.row][@"tel"];
    cell.likes.text = [NSString stringWithFormat:@"%d", [_datasource[indexPath.row][@"likes"] integerValue]];
    
    NSString *distance = _datasource[indexPath.row][@"distance"];
    
    if([distance floatValue] <= 0.0f)
        cell.distance.text = @"";
    else
        cell.distance.text = convertDistanceToString([distance floatValue]);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Result willDisplayCell:_ref];
    self.ref = (Cell *)[tableView cellForRowAtIndexPath:indexPath];
    [Result didSelectCell:_ref];
    
    [self detailPageFor:indexPath.row];
}

- (void)detailPageFor:(NSInteger )index
{
    Detail *moreInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
    moreInfo.delegate = self;
    moreInfo.title = self.name.text;
    moreInfo.data = [NSDictionary dictionaryWithDictionary:_datasource[index]];
    
    [self addChildViewController:moreInfo];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [UIView new];
    Cell *c = (Cell *)cell;
    
    if(indexPath.row %2 == 0)
        c.border.image = [UIImage imageNamed:@"border-even.png"];
    else
        c.border.image = [UIImage imageNamed:@"border-odd.png"];
    
    if(c.selected) [Result didSelectCell:c];
}

#pragma mark - click

- (IBAction)call:(UIButton *)sender
{
    NSLog(@"Call");
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
        cell = (UITableViewCell *)sender.superview.superview.superview;
    
    NSIndexPath* index = [self.table indexPathForCell:cell];
    [self callPhoneNumber:index.row];
}

- (void)callPhoneNumber:(NSInteger)index
{
    NSString *tel = _datasource[index][@"tel"];
    tel = [tel stringByReplacingOccurrencesOfString:@" " withString:@""];
    tel = [NSString stringWithFormat:@"telprompt:%@", tel];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
}

- (IBAction)route:(UIButton *)sender
{
    NSLog(@"Route");
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
        cell = (UITableViewCell *)sender.superview.superview.superview;
    
    NSIndexPath* index = [self.table indexPathForCell:cell];
    
    Place *to = [[Place alloc] initWithAction:^{
        NSString *tel = _datasource[index.row][@"tel"];
        tel = [NSString stringWithFormat:@"telprompt:%@", tel];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
    }];
    
    to.name = _datasource[index.row][@"nom"];
    to.description = _datasource[index.row][@"adresse"];
    to.latitude = [_datasource[index.row][@"latitude"] doubleValue];
    to.longitude = [_datasource[index.row][@"longitude"] doubleValue];
    
    Map * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"map"];
    vc.to = to;
    [self addChildViewController:vc];
    
    to = nil;
    vc = nil;
}

#pragma mark - all on map

- (IBAction)showResultOnMap
{
    NSLog(@"Title: %@", self.name.text);
    
    NSMutableArray *pins = [NSMutableArray array];
    
    int i=0;
    
    NSArray *tmp = self.datasource;
    
    if(self.datasource.count > 500)
    {
        tmp = [self.datasource subarrayWithRange:NSMakeRange(0, 500)];
    }
    
    for (NSDictionary *obj in tmp)
    {
        if([obj[@"latitude"] integerValue] && [obj[@"longitude"] integerValue])
        {
            Place *to = [[Place alloc] initWithAction:^{
                
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                                  message:nil
                                                                 delegate:nil
                                                        cancelButtonTitle:SLocalizedString(@"Annuler", nil)
                                                        otherButtonTitles:SLocalizedString(@"Appeler", nil), SLocalizedString(@"Plus en détail", nil), nil];
                
                [message setDelegate:self];
                [message setTag:i];
                [message show];
                
            }];
            
            to.name = obj[@"nom"];
            to.description = obj[@"adresse"];
            to.latitude = [obj[@"latitude"] doubleValue];
            to.longitude = [obj[@"longitude"] doubleValue];
            
            [pins addObject:to];
        }
        i++;
    }
    
    Map * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"map"];
    vc.showAll = YES;
    vc.pins = pins;
    
    [self addChildViewController:vc];
    
    pins = nil;
}

#pragma mark - uiviewcontrolelr container

// S'adapter au frame pendant l'ajout
- (void)willMoveToParentViewController:(UIViewController *)parent
{    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    self.view.frame = parent.view.frame;

    [[parent view] addSubview:[self view]];
}


#pragma mark -  properties

- (Detail *)detail
{
    if(_detail == nil)
    {
        self.detail = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
        self.detail.delegate = self;
    }
    
    return _detail;
}

#pragma mark - helper

NSString* convertDistanceToString(float distance)
{
    if (distance < 1)
        return [NSString stringWithFormat:@"%.1f m", distance * 1000];
    else
        return [NSString stringWithFormat:@"%.1f km", distance];
}

- (void)setLikes:(NSDictionary *)data
{
    self.datasource[[_table indexPathForSelectedRow].row ] = data;
    [self.table reloadData];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 1:
            [self callPhoneNumber:alertView.tag];
            break;
        case 2:
            [self detailPageFor:alertView.tag];
            break;
    }
}

@end
