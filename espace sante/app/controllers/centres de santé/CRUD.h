//
//  Crud.h
//  espace sante
//
//  Created by abdel ali on 1/23/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <RegexKitLite.h>
#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import "Message.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "Annotation.h"
#import "LocalizationSystem.h"

@interface Crud : UIViewController<MKMapViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate>
{
    NSMutableDictionary *intialDic;
}

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *container;
@property (weak, nonatomic) IBOutlet UILabel *heading;
@property (weak, nonatomic) IBOutlet UILabel *note;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *address;
@property (weak, nonatomic) IBOutlet UITextField *city;
@property (weak, nonatomic) IBOutlet UITextField *activity;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (nonatomic, strong) NSMutableDictionary *data;

@end

