//
//  Pharmacies.m
//  espace sante
//
//  Created by abdel ali on 04/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Pharmacies.h"
#import <QuartzCore/QuartzCore.h>

@interface Pharmacies ()
@property (nonatomic, strong) NSArray *datasource;
@property (nonatomic, strong) Pharmacy *ref;
@end

@implementation Pharmacies

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self init_settings];
    
    [self.location setAutocompleteWithDataSource:self delegate:self customize:^(ACEAutocompleteInputView *inputView) {
        inputView.font = [UIFont fontWithName:@"DroidSans" size:14];
        inputView.textColor = [UIColor whiteColor];
        inputView.backgroundColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    }];
    
    tmp = self.pharmacies.frame.origin.y;

    [Flurry logEvent:@"Pharmacie de garde" timed:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - settings

- (void)init_settings
{
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = SLocalizedString(@"Pharmacies de garde", nil);
    
    self.around_me.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    self.around_me.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.around_me.text = SLocalizedString(@"Autour de moi", nil);
    
    self.location.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.location.textColor = [UIColor blackColor];
    
    self.pharmacies.delegate = self;
    self.pharmacies.dataSource = self;
    
    self.location.placeholder = SLocalizedString(@"Choisissez une ville", nil);
    
    self.allOnMap.hidden = YES;
    
    self.num.font = [UIFont fontWithName:@"DroidSans-Bold" size:12];
    self.num.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
}

+ (void)willDisplayCell:(Pharmacy *)cell
{
    cell.distance.font = [UIFont fontWithName:@"DroidSans" size:12];
    cell.distance.textColor = [UIColor colorWithRed:187/255.0 green:187/255.0 blue:187/255.0 alpha:1.0];
    cell.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:11];
    cell.name.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    cell.address.font = [UIFont fontWithName:@"DroidSans" size:11];
    cell.address.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    cell.phone.font = [UIFont fontWithName:@"DroidSans" size:12];
    cell.phone.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    cell.call.hidden = YES;
    cell.pin.image = [Resources loadImage:@"map_indicateur_notselected"];
}

+ (void)didSelectCell:(Pharmacy *)cell
{
    cell.distance.textColor = [UIColor whiteColor];
    cell.phone.textColor = [UIColor whiteColor];
    cell.address.textColor = [UIColor whiteColor];
    cell.name.textColor = [UIColor colorWithRed:248/255.0 green:182/255.0 blue:0/255.0 alpha:1.0];
    cell.pin.image = [Resources loadImage:@"map_indicateur_selected"];
    cell.call.hidden = NO;
}

#pragma mark - pharmacies

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"pharmacy";
    
    Pharmacy *cell = (Pharmacy *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
        cell = [[Pharmacy alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    [Pharmacies willDisplayCell:cell];
    
    cell.name.text = _datasource[indexPath.row][@"nom"];
    cell.address.text = _datasource[indexPath.row][@"adresse"];
    cell.phone.text = _datasource[indexPath.row][@"tel"];
    cell.distance.text = @"";
    
    if(_datasource[indexPath.row][@"distance_KM"])
    {
        NSString *distance = _datasource[indexPath.row][@"distance_KM"];
        cell.distance.text = [NSString stringWithFormat:@"%.1f km",  [distance floatValue]];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [Pharmacies willDisplayCell:_ref];
    self.ref = (Pharmacy *)[tableView cellForRowAtIndexPath:indexPath];
    [Pharmacies didSelectCell:_ref];
    
    [self gotoDetail:indexPath.row];
}

- (void)gotoDetail:(NSInteger)index
{
    self.detail.data = nil;
    self.detail.data = (NSDictionary *)_datasource[index];
    self.detail.title = self.name.text;
    
    [self addChildViewController:_detail];
    self.detail = nil;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Pharmacy *p = (Pharmacy *)cell; 
    
    if(indexPath.row %2 == 0)
        p.border.image = [UIImage imageNamed:@"border-even.png"];
    else
        p.border.image = [UIImage imageNamed:@"border-odd.png"];
    
    if(p.selected) [Pharmacies didSelectCell:p];
}

#pragma mark - click

- (IBAction)call:(UIButton *)sender
{
    NSLog(@"Call");
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
        cell = (UITableViewCell *)sender.superview.superview.superview;
    
    NSIndexPath* index = [self.pharmacies indexPathForCell:cell];
    [self callNumIndex:index.row];
}

- (void)callNumIndex:(NSInteger )index
{
    NSString *tel = _datasource[index][@"tel"];
    tel = [NSString stringWithFormat:@"telprompt:%@", tel];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
}

- (IBAction)route:(UIButton *)sender
{
    NSLog(@"Route");
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
        cell = (UITableViewCell *)sender.superview.superview.superview;
    
    NSIndexPath* index = [self.pharmacies indexPathForCell:cell];
    
    Place *to = [[Place alloc] initWithAction:^{
        NSString *tel = _datasource[index.row][@"tel"];
        tel = [NSString stringWithFormat:@"telprompt:%@", tel];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
    }];
    
    to.name = _datasource[index.row][@"nom"];
    to.description = _datasource[index.row][@"adresse"];
    to.latitude = [_datasource[index.row][@"latitude"] floatValue];
    to.longitude = [_datasource[index.row][@"longitude"] floatValue];
    
    Map * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"map"];
    vc.to = to;
    [self addChildViewController:vc];
    
    to = nil;
    vc = nil;
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // Get the rects of the text field being edited and the view that we're going to scroll.
    // We convert everything to window coordinates
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    // So now we have the bounds we need to calculate the fraction between the top and bottom of the middle section for the text field's midline
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    if(_trigger.hidden)
    {
        CGRect rect = self.pharmacies.frame;
        rect.origin.y = tmp;
        rect.size.height -= _trigger.frame.size.height;
        [self.trigger setHidden:NO];
        
        [UIView animateWithDuration:0.5 animations:^{
            [self.pharmacies setFrame:rect];
            
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

#pragma mark - autocomplete

- (void)textField:(UITextField *)textField didSelectObject:(id)object inInputView:(ACEAutocompleteInputView *)inputView
{
    textField.text = object;
    [textField resignFirstResponder];
}

- (NSUInteger)minimumCharactersToTrigger:(ACEAutocompleteInputView *)inputView
{
    return 1;
}

- (void)inputView:(ACEAutocompleteInputView *)inputView itemsFor:(NSString *)query result:(void (^)(NSArray *items))resultBlock;
{
    if (resultBlock != nil) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            NSMutableArray *data = [NSMutableArray array];
            
            NSArray *src = @[@"TETOUAN", @"TEMARA", @"TANGER", @"SKHIRAT", @"SALE", @"RABAT", @"OUJDA", @"MEKNES", @"MARRAKECH", @"KENITRA", @"FES", @"CASABLANCA", @"AGADIR"];
            
            for (NSString *s in src) {
                
                if ([s hasPrefix:[query uppercaseString]]) {
                    [data addObject:s];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                resultBlock(data);
            });
        });
    }
}

#pragma mark - search

- (void)search:(NSDictionary *)params
{
    // hide launch button
    CGRect rect = self.pharmacies.frame;
    rect.origin.y = _trigger.frame.origin.y;
    rect.size.height += _trigger.frame.size.height;
    [self.trigger setHidden:YES];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.pharmacies setFrame:rect];
        
    }];
    
    [Message load:self message:nil detail:nil view:nil delay:0];
    
    
    [[IS api] getPath:@"PharmaciesGardes" parameters:params
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  //NSLog(@"ERROR ####: %@",responseObject);
                  self.datasource = [(NSArray *)responseObject[@"results"] sortedArrayUsingFunction:compare context:nil];
                  [self.pharmacies reloadData];
                  self.num.text = [NSString stringWithFormat:@"%d", _datasource.count];
                  self.allOnMap.hidden = NO;
                  
                  [Message hide];
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  NSLog(@"ERROR ####: %@",error);
                  [Message hide];
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                                  message:SLocalizedString(@"Erreur de connexion", nil)
                                                                 delegate:nil cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                  [alert show];
              }];
}

- (IBAction)launch:(id)sender
{
    if (self.location.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                        message:SLocalizedString(@"Ville est obligatoire", nil)
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    NSDictionary *params = @{@"ville": self.location.text};
    
    NSString *gps;
    if (self.turn_on_gps.on) {
        gps = @"On";
    }else{
        gps = @"Off";
    }
    NSDictionary *searchParamsDict = @{@"Around me": gps, @"City": params[@"ville"]};
    
    //NSLog(@"Search P: %@", searchParamsDict);
    
    [Flurry logEvent:@"Search pharmacy" withParameters:searchParamsDict];
    
    [self search:params];
}

- (IBAction)find_by:(id)position
{
    BOOL isOn = [position isOn];
    
    if (isOn)
    {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted )
        {
            // Turn on location service
            [position setOn:NO animated:YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                            message:SLocalizedString(@"GPS desactivé", nil)
                                                           delegate:nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        else
        {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [locationManager startUpdatingLocation];
            
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (newLocation != nil)
    {
        [manager stopUpdatingLocation];
        manager = nil;
        
        NSDictionary *params = @{@"rayon": @5, @"latitude": @(newLocation.coordinate.latitude),
                                 @"longitude": @(newLocation.coordinate.longitude)};
        
        [self search:params];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [manager stopUpdatingLocation];
    manager = nil;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                    message:SLocalizedString(@"GPS warning", nil)
                                                   delegate:nil cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - helper

NSInteger compare (id num1, id num2, void *context)
{
    int v1 = [[num1 objectForKey:@"distance_KM"] integerValue];
    int v2 = [[num2 objectForKey:@"distance_KM"] integerValue];
    if (v1 < v2)
        return NSOrderedAscending;
    else if (v1 > v2)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}

#pragma mark -  properties

- (Detail *)detail
{
    if(_detail == nil)
    {
        self.detail = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
    }
    
    return _detail;
}

- (IBAction)showResultOnMap
{
    NSLog(@"Route to all pharmacies");
    NSMutableArray *pins = [NSMutableArray array];
    
    int i=0;
    
    for (NSDictionary *obj in self.datasource)
    {
        if(![obj[@"latitude"] isEqualToString:@""] && ![obj[@"longitude"] isEqualToString:@""])
        {
            Place *to = [[Place alloc] initWithAction:^{
                
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                                  message:nil
                                                                 delegate:nil
                                                        cancelButtonTitle:SLocalizedString(@"Annuler", nil)
                                                        otherButtonTitles:SLocalizedString(@"Appeler", nil), SLocalizedString(@"Plus en détail", nil), nil];
                
                [message setDelegate:self];
                [message setTag:i];
                [message show];
                
            }];
            
            to.name = obj[@"nom"];
            to.description = obj[@"adresse"];
            to.latitude = [obj[@"latitude"] doubleValue];
            to.longitude = [obj[@"longitude"] doubleValue];
            
            [pins addObject:to];
        }
        i++;
    }
    
    Map * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"map"];
    vc.showAll = YES;
    vc.pins = pins;
    
    [self addChildViewController:vc];
    
    pins = nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 1:
            [self callNumIndex:alertView.tag];
            break;
        case 2:
            [self gotoDetail:alertView.tag];
            break;
    }
}

@end
