//
//  Hospitals.h
//  espace sante
//
//  Created by abdel ali on 10/31/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ACEAutocompleteBar.h"
#import "Message.h"
#import "WS.h"
#import "Result.h"
#import "Centers.h"
#import "SwitchOnOff.h"

@interface Hospitals : UIViewController<ACEAutocompleteDataSource, ACEAutocompleteDelegate, CLLocationManagerDelegate, UITextFieldDelegate>
{
    CGFloat animatedDistance;
    CLLocationManager *locationManager;
    NSDictionary *params;
    NSArray *value;
}

@property (weak, nonatomic) IBOutlet UITextField *query;
@property (weak, nonatomic) IBOutlet UITextField *location;
@property (weak, nonatomic) IBOutlet UIButton *find_by_location;
@property (weak, nonatomic) IBOutlet UIButton *find_by_position;
@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet UILabel *likes;
@property (weak, nonatomic) IBOutlet SwitchOnOff *find_by_likes;


- (IBAction)mode:(id)sender;

@end
