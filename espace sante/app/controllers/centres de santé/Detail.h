//
//  Detail.h
//  espace sante
//
//  Created by abdel ali on 1/17/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ALRadialMenu.h>
#import <RegexKitLite.h>
#import <FacebookSDK/FacebookSDK.h>
#import <MessageUI/MessageUI.h>
#import "Message.h"
#import "File.h"
#import "Map.h"
#import "WS.h"
#import "SSKeychain.h"
#import "UIDevice+IdentifierAddition.h"
#import "NSString+MD5Addition.h"
#import "Crud.h"
#import "LocalizationSystem.h"

@class Result;

@interface Detail : UIViewController<ALRadialMenuDelegate, UIActionSheetDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *presentation;
@property (weak, nonatomic) IBOutlet UILabel *lbl_address;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *lbl_activity;
@property (weak, nonatomic) IBOutlet UILabel *activity;
@property (weak, nonatomic) IBOutlet UILabel *lbl_contact;
@property (weak, nonatomic) IBOutlet UILabel *contact;
@property (weak, nonatomic) IBOutlet UILabel *likes;
@property (weak, nonatomic) IBOutlet UIButton *options;

@property (nonatomic, strong) NSDictionary *data;
@property (strong, nonatomic) ALRadialMenu *menu;
@property (strong, nonatomic) Crud *crud;
@property (strong, nonatomic) Result *delegate;

@end
