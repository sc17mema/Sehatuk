//
//  Pharmacies.h
//  espace sante
//
//  Created by abdel ali on 04/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SwitchOnOff.h"
#import "Pharmacy.h"
#import "Resources.h"
#import "IS.h"
#import "ACEAutocompleteBar.h"
#import "Message.h"
#import "Place.h"
#import "Map.h"
#import "Centers.h"
#import "Detail.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.1;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface Pharmacies : UIViewController<UITableViewDataSource, UITableViewDelegate, ACEAutocompleteDataSource, ACEAutocompleteDelegate, CLLocationManagerDelegate>
{
    CGFloat animatedDistance;
    CLLocationManager *locationManager;
    float tmp;
}

@property (weak, nonatomic) IBOutlet UILabel *num;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *around_me;
@property (weak, nonatomic) IBOutlet UITextField *location;
@property (weak, nonatomic) IBOutlet UIButton *trigger;
@property (weak, nonatomic) IBOutlet SwitchOnOff *turn_on_gps;
@property (weak, nonatomic) IBOutlet UITableView *pharmacies;
@property (weak, nonatomic) IBOutlet UIButton *allOnMap;

@property (strong, nonatomic) UIViewController *child;
@property (strong, nonatomic) Detail *detail;

- (IBAction)route:(UIButton *)sender;

@end
