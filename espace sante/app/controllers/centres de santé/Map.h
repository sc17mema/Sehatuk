//
//  Map.h
//  WAFAASSURANCE
//
//  Created by abdel ali on 13/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFHTTPClient.h>
#import <CoreLocation/CoreLocation.h>
#import <Reachability/Reachability.h>
#import "MapView.h"
#import "Place.h"
#import "Message.h"
#import "LocalizationSystem.h"

@interface Map : UIViewController<CLLocationManagerDelegate>

@property (nonatomic, strong) Place *from;
@property (nonatomic, strong) Place *to;
@property (nonatomic, strong) MapView* mapView;
@property (nonatomic) BOOL showAll;
@property (nonatomic, strong) NSArray *pins;

@property (nonatomic, strong) CLLocationManager* locationManager;

@end
