//
//  Map.m
//  WAFAASSURANCE
//
//  Created by abdel ali on 13/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Map.h"

@implementation Map

- (void)showRoute
{
    if(!self.showAll)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        [self.locationManager startUpdatingLocation];
        
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
            
        if(CLLocationCoordinate2DIsValid(_locationManager.location.coordinate) && status == kCLAuthorizationStatusAuthorized)
        {
            self.from = [[Place alloc] init];
            self.from.latitude = _locationManager.location.coordinate.latitude;
            self.from.longitude = _locationManager.location.coordinate.longitude;
            self.from.name = SLocalizedString(@"Position Actuelle", nil);
            
            // allocate a reachability object
            Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
            
            // set the blocks
            reach.reachableBlock = ^(Reachability*reach)
            {
                (_to.longitude != 0.0 && _to.latitude != 0.0) ? [self.mapView showRouteFrom:_from to:_to] : [self.mapView addNotation:_from];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Message hide];
                    [self.mapView zoomToFitMapAnnotations];
                });
                
            };
            
            reach.unreachableBlock = ^(Reachability*reach)
            {
                (_to.longitude != 0.0 && _to.latitude != 0.0) ? [self.mapView addNotation:_to] : nil;
                [self.mapView addNotation:_from];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Message hide];
                    [self.mapView zoomToFitMapAnnotations];
                });
            };
            
            // start the notifier, which will cause the reachability object to retain itself!
            [Message load:self message:nil detail:nil view:nil delay:0];
            [reach startNotifier];
        }
        else
        {
            (_to.longitude != 0.0 && _to.latitude != 0.0) ? [self.mapView addNotation:_to] : nil;
        }
    }
    else
    {
        for (Place *pin in self.pins) {
            [self.mapView addNotation:pin];
        }
        [self.mapView.mapView setShowsUserLocation:YES];
        [self.mapView zoomToFitMapAnnotations];
    }
}


#pragma mark - uiviewcontrolelr container

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    // Add the view to the parent view and position it if you want
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    self.view.frame = parent.view.frame;
    
    self.mapView = [[MapView alloc] initWithFrame:parent.view.frame];
    [self.view addSubview:_mapView];
    [[parent view] addSubview:[self view]];
    [self showRoute];
}

@end
