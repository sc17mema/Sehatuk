//
//  Centers.h
//  espace sante
//
//  Created by abdel ali on 23/09/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ViewPagerController.h"
#import "ViewPagerDataSource.h"
#import "ViewPagerDelegate.h"
#import "Message.h"
#import "LocalizationSystem.h"

@interface Centers : ViewPagerController<ViewPagerDataSource, ViewPagerDelegate>
{
    UIImageView *indicator;
}

@property (nonatomic) id ref;
@property (weak, nonatomic) IBOutlet UIView *band;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) NSArray *datasource;

@end
