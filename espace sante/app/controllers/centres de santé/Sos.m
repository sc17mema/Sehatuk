//
//  Sos.m
//  espace sante
//
//  Created by abdel ali on 12/2/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Sos.h"

@interface Sos ()
@property (nonatomic, strong) UITableViewCell *ref;
@property (nonatomic, strong) NSArray *datasource;
@end

@implementation Sos

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = SLocalizedString(@"Numéros utiles", nil);
    
    self.datasource = @[@{@"nom": @"Protection civile", @"tel": @"15", @"image": @"protection_civile"},
                        @{@"nom": @"Police", @"tel": @"19", @"image": @"police"},
                        @{@"nom": @"Ambulance", @"tel": @"0661628282", @"image": @"ambulance"},
                        @{@"nom": @"Gendarmerie", @"tel": @"117", @"image": @"gendarmerie"},
                        @{@"nom": @"Autoroute", @"tel": @"5050", @"image": @"autoroute"}];
    [self.table reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - settings

+ (void)willDisplayCell:(UITableViewCell *)cell
{
    UILabel *name = (UILabel *)[cell viewWithTag:101];
    name.font = [UIFont fontWithName:@"DroidSans-Bold" size:13];
    name.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    
    UILabel *tele = (UILabel *)[cell viewWithTag:102];
    tele.font = [UIFont fontWithName:@"DroidSans" size:11];
    tele.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    UIButton *call = (UIButton *)[cell viewWithTag:103];
    call.hidden = YES;
}

#pragma mark - pharmacies

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"sos";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    [Sos willDisplayCell:cell];
    
    UILabel *name = (UILabel *)[cell viewWithTag:101];
    name.text = _datasource[indexPath.row][@"nom"];;
    
    UILabel *tele = (UILabel *)[cell viewWithTag:102];
    tele.text = _datasource[indexPath.row][@"tel"];
    
    UIImageView *image = (UIImageView *)[cell viewWithTag:100];
    image.image = [Resources loadImage:_datasource[indexPath.row][@"image"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIImageView *image = (UIImageView *)[_ref viewWithTag:104];
    
    NSIndexPath* index = [self.table indexPathForCell:_ref];
    
    if(index.row %2 == 0)
        image.image = [Resources loadImage:@"sos-border-even"];
    else
        image.image = [Resources loadImage:@"sos-border-odd"];
    
    [Sos willDisplayCell:_ref];
    
    self.ref = [tableView cellForRowAtIndexPath:indexPath];
    
    UILabel *name = (UILabel *)[self.ref viewWithTag:101];
    name.textColor = [UIColor colorWithRed:248/255.0 green:182/255.0 blue:0/255.0 alpha:1.0];
    
    UILabel *tele = (UILabel *)[self.ref viewWithTag:102];
    tele.textColor = [UIColor whiteColor];
    
    UIButton *call = (UIButton *)[self.ref viewWithTag:103];
    call.hidden = NO;
    
    image = (UIImageView *)[self.ref viewWithTag:104];
    
    if (indexPath.row % 2 == 0)
        image.image = [Resources loadImage:@"sos-selected-even"];
    else
        image.image = [Resources loadImage:@"sos-selected-odd"];
    
    self.ref.backgroundColor = [UIColor clearColor];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIImageView *image = (UIImageView *)[cell viewWithTag:104];
    
    if(indexPath.row %2 == 0)
        image.image = [Resources loadImage:@"sos-border-even"];
    else
        image.image = [Resources loadImage:@"sos-border-odd"];
    
    
    cell.backgroundView = [UIView new];
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - click

- (IBAction)call:(UIButton *)sender
{
    NSLog(@"Call");
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
        cell = (UITableViewCell *)sender.superview.superview.superview;
    
    NSIndexPath* index = [self.table indexPathForCell:cell];
    
    NSString *tel = _datasource[index.row][@"tel"];
    tel = [tel stringByReplacingOccurrencesOfString:@" " withString:@""];
    tel = [NSString stringWithFormat:@"telprompt:%@", tel];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
}

@end
