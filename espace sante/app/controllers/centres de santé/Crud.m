//
//  Crud.m
//  espace sante
//
//  Created by abdel ali on 1/23/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "Crud.h"

@implementation Crud

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    intialDic = [NSMutableDictionary dictionaryWithDictionary:self.data];
    
    [self.container contentSizeToFit];
    
    self.heading.font = [UIFont fontWithName:@"Droid Sans" size:14];
    self.heading.text = SLocalizedString(@"Mettre à jour", nil);
    
    self.name.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.name.textColor = [UIColor blackColor];
    self.name.text = _data[@"nom"];
    self.name.placeholder = SLocalizedString(@"Nom", nil);
    
    self.address.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.address.textColor = [UIColor blackColor];
    self.address.text = _data[@"adresse"];
    self.address.placeholder = SLocalizedString(@"Adresse", nil);
    
    self.activity.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.activity.textColor = [UIColor blackColor];
    self.activity.text = [_data[@"activites"] stringByReplacingOccurrencesOfRegex:@"^Activités :\\s" withString:@""];
    self.activity.placeholder = SLocalizedString(@"Activité", nil);
    
    self.city.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.city.textColor = [UIColor blackColor];
    self.city.text = _data[@"ville"];
    self.city.placeholder = SLocalizedString(@"Ville", nil);
    
    self.phone.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.phone.textColor = [UIColor blackColor];
    self.phone.text = _data[@"tel"];
    self.phone.placeholder = SLocalizedString(@"Telephone", nil);
    
    self.note.font = [UIFont fontWithName:@"DroidSans-Bold" size:10];
    self.note.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.note.text = SLocalizedString(@"Appuyez longuement sur l'indicateur pour le positionner", nil);
    
    self.container.contentSize = CGSizeMake(320, 370);
    
    CLLocationCoordinate2D mapRegion;
    mapRegion.latitude = [self.data[@"latitude"] doubleValue];
    mapRegion.longitude = [self.data[@"longitude"] doubleValue];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = mapRegion;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(mapRegion, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    [self.map addAnnotation:annotation];
    [self.map setRegion:viewRegion animated: YES];
    
    UILongPressGestureRecognizer *gr = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(longPress:)];
    gr.minimumPressDuration = .5;
    gr.delegate = self;
    [self.map addGestureRecognizer:gr];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - actions

- (IBAction)cancel:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.view.superview.layer addAnimation:transition forKey:nil];
    [self.view removeFromSuperview];
    [self willMoveToParentViewController:nil];
    [self removeFromParentViewController];
}

- (IBAction)validate:(id)sender
{
    UIAlertView *msg = [[UIAlertView alloc] initWithTitle:SLocalizedString(@"Pour une éventuelle vérification", nil)
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:SLocalizedString(@"Non, Merci", nil)
                                            otherButtonTitles:@"Ok", nil];
    [msg setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    [[msg textFieldAtIndex:1] setSecureTextEntry:NO];
    [[msg textFieldAtIndex:0] setPlaceholder:SLocalizedString(@"Nom & Prénom", nil)];
    [[msg textFieldAtIndex:1] setPlaceholder:SLocalizedString(@"Telephone", nil)];
    [[msg textFieldAtIndex:0] setDelegate:self];
    [[msg textFieldAtIndex:1] setDelegate:self];
    
    [msg show];
    
}

#pragma mark - map delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    DraggableAnnotationView *pinView = nil;
    
    if(annotation != mapView.userLocation)
    {
        static NSString *defaultPin = @"pin";
        
        pinView = (DraggableAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPin];
        
        if ( pinView == nil )
        {
            pinView = [[DraggableAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPin];
            pinView.draggable = YES;
            pinView.mapView = self.map;
        }
    }
    
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        
        self.data[@"latitude"] = @(droppedAt.latitude);
        self.data[@"longitude"] = @(droppedAt.longitude);
    }
}

#pragma mark - uitextview delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - gestures

- (void)longPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:_map];
    
    CLLocationCoordinate2D touchMapCoordinate = [self.map convertPoint:touchPoint toCoordinateFromView:_map];
    [(MKPointAnnotation *)_map.annotations[0] setCoordinate:touchMapCoordinate];
}

#pragma mark - uiviewcontrolelr container

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.view.layer addAnimation:transition forKey:nil];
    
    [[parent view] addSubview:[self view]];
}

#pragma mark - alertview delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Terminer"])
    {
        UITextField *firstname = [alertView textFieldAtIndex:0];
        UITextField *tele = [alertView textFieldAtIndex:1];
        
        NSString *rgx = @"^(0)*[0-9]{10}$";
        if(![[NSPredicate predicateWithFormat:@"SELF MATCHES %@", rgx] evaluateWithObject:tele.text])
        {
            [Message failed:self message:nil delay:1];
            return;
        }
        
        rgx = @"[a-zA-Z'\\s]{2,}";
        if(![[NSPredicate predicateWithFormat:@"SELF MATCHES %@", rgx] evaluateWithObject:firstname.text])
        {
            [Message failed:self message:nil delay:1];
            return;
        }
        
        self.data[@"nom"] = self.name.text;
        self.data[@"adresse"] = self.address.text;
        self.data[@"demandeur"] = firstname.text;
        self.data[@"tele_demandeur"] = tele.text;
        self.data[@"activites"] = self.activity.text;
        self.data[@"tel"] = self.phone.text;
        
        NSMutableArray* tmp = [NSMutableArray array];
        
        [tmp addObject:@"//==MODIFICATION==//"];
        
        [_data enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [tmp addObject:[NSString stringWithFormat:@"%@=%@", key, obj]];
        }];
        
        [tmp addObject:@"\n//==ETAT INITIAL==//"];
        
        [intialDic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [tmp addObject:[NSString stringWithFormat:@"%@=%@", key, obj]];
        }];
        
        NSDictionary *params = @{@"txt" :  [tmp componentsJoinedByString:@"\n"], @"obj" : [@"Demande de mise à jour : " stringByAppendingString:self.title], @"from" : @"sehatuk@iphone.com"};
            
        [Message load:self message:nil detail:nil view:nil delay:0];
            
        AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://ns29646.ovh.net:8080/"]];
        [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [client getPath:@"InfoServicesSeha/SendMailSeha" parameters:params
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    [Message success:self message:nil delay:1];
                    [self cancel:nil];
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [Message failed:self message:nil delay:1];
                }];
        
    }

    [alertView resignFirstResponder];
}


@end
