//
//  Sos.h
//  espace sante
//
//  Created by abdel ali on 12/2/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Resources.h"
#import "LocalizationSystem.h"

@interface Sos : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
}

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end
