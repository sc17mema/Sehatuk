//
//  Doctors.h
//  espace sante
//
//  Created by abdel ali on 11/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ACEAutocompleteBar.h"
#import "Message.h"
#import "WS.h"
#import "Result.h"
#import "Centers.h"
#import "SwitchOnOff.h"
#import "LocalizationSystem.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.1;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface Doctors : UIViewController<ACEAutocompleteDataSource, ACEAutocompleteDelegate, CLLocationManagerDelegate, UITextFieldDelegate>
{
    CGFloat animatedDistance;
    CLLocationManager *locationManager;
    NSDictionary *params;
    NSString *url;
}

@property (weak, nonatomic) IBOutlet UITextField *keywords;
@property (weak, nonatomic) IBOutlet UITextField *location;
@property (weak, nonatomic) IBOutlet UITextField *speciality;
@property (weak, nonatomic) IBOutlet UIButton *find_by_location;
@property (weak, nonatomic) IBOutlet UIButton *find_by_position;
@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet UILabel *likes;
@property (weak, nonatomic) IBOutlet SwitchOnOff *find_by_likes;

- (IBAction)mode:(id)sender;

@end
