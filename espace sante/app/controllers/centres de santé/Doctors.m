//
//  Doctors.m
//  espace sante
//
//  Created by abdel ali on 11/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Doctors.h"

@implementation Doctors

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self init_settings];
    
    [self.location setAutocompleteWithDataSource:self delegate:self customize:^(ACEAutocompleteInputView *inputView) {
        inputView.font = [UIFont fontWithName:@"DroidSans" size:14];
        inputView.textColor = [UIColor whiteColor];
        inputView.backgroundColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    }];
    [self.speciality setAutocompleteWithDataSource:self delegate:self customize:^(ACEAutocompleteInputView *inputView) {
        inputView.font = [UIFont fontWithName:@"DroidSans" size:14];
        inputView.textColor = [UIColor whiteColor];
        inputView.backgroundColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    }];
    
    if([self.title isEqualToString:SLocalizedString(@"Medecins", nil)])
    {
        url = @"medecins";
    }
    else if([self.title isEqualToString:SLocalizedString(@"Cliniques", nil)])
    {
        url = @"centres";
    }
    
    [Flurry logEvent:self.title timed:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - settings

- (void)init_settings
{
    self.name.text = self.title;
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    self.keywords.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.keywords.textColor = [UIColor blackColor];
    self.keywords.placeholder = SLocalizedString(@"Mots clé", nil);
    
    self.location.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.location.textColor = [UIColor blackColor];
    self.location.placeholder = SLocalizedString(@"Choisissez une ville", nil);
    
    self.speciality.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.speciality.textColor = [UIColor blackColor];
    self.speciality.placeholder = SLocalizedString(@"Choisissez une spécialité", nil);
    
    self.find_by_position.titleLabel.font = [UIFont fontWithName:@"DroidSans" size:12];
    [self.find_by_position setTitle:SLocalizedString(@"Autour de moi", nil) forState:UIControlStateNormal];
    
    self.find_by_location.titleLabel.font = [UIFont fontWithName:@"DroidSans" size:12];
    self.find_by_location.selected = YES;
    [self.find_by_location setTitle:SLocalizedString(@"Ville", nil) forState:UIControlStateNormal];
    
    self.likes.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    self.likes.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.likes.text = SLocalizedString(@"Les + Recommandés ?", nil);
}

#pragma mark -

- (IBAction)mode:(id)sender
{
    self.find_by_location.selected = !_find_by_location.selected;
    self.find_by_position.selected = !_find_by_position.selected;
    
    NSLog(@"location: %d", self.find_by_location.selected);
    NSLog(@"postion: %d", self.find_by_position.selected);
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // Get the rects of the text field being edited and the view that we're going to scroll.
    // We convert everything to window coordinates
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    // So now we have the bounds we need to calculate the fraction between the top and bottom of the middle section for the text field's midline
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

#pragma mark - autocomplete

- (void)textField:(UITextField *)textField didSelectObject:(id)object inInputView:(ACEAutocompleteInputView *)inputView
{
    textField.text = object;
    [textField resignFirstResponder];
}

- (NSUInteger)minimumCharactersToTrigger:(ACEAutocompleteInputView *)inputView
{
    return 1;
}

- (void)inputView:(ACEAutocompleteInputView *)inputView itemsFor:(NSString *)query result:(void (^)(NSArray *items))resultBlock;
{
    if (resultBlock != nil) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            NSMutableArray *data = [NSMutableArray array];
            
            NSArray *src = nil;
            
            if (inputView.textField == self.location)
            {
   
                if ([LocalizationGetCountry isEqualToString:@"ma"]) {
                    src = @[@"Agadir", @"Casablanca", @"Berrechid",
                            @"Mohammedia", @"Benslimane", @"El Jadida",
                            @"Settat", @"Beni Mellal", @"Khouribga",
                            @"Marrakech", @"Safi", @"Essaouira", @"Ouarzazate",
                            @"Aït Melloul", @"Inezgane", @"Tiznit",
                            @"Tan Tan", @"Guelmim", @"Taroudant", @"Laayoune",
                            @"Rabat", @"Tétouan", @"Meknès",
                            @"Tanger", @"Taza", @"Khénifra", @"Errachidia",
                            @"Fès", @"Séfrou", @"Nador", @"Kenitra"];
                }
                else if ([LocalizationGetCountry isEqualToString:@"TUN"]) {
                    src = @[@"Ariana",@"Beja",@"Ben Arous",@"Bizerte",@"Gabes",
                            @"Gafsa",@"Jendouba",@"Kairouan",@"Kasserine",@"Kebili"
                            ,@"Kef",@"Mahdia",@"Mannouba",@"Medenine",@"Monastir"
                            ,@"Nabeul",@"Sfax",@"Sidi Bouzid",@"Siliana",@"Sousse"
                            ,@"Tataouine",@"Tozeur",@"Tunis",@"Zaghouan"];
                }
                else if ([LocalizationGetCountry isEqualToString:@"ALGE"]) {
                    src = @[@"Adrar",@"Chlef",@"Laghouat",@"Oum El Bouaghi",@"Batna",@"Bejaia",
                            @"Biskra",@"Bechar",@"Blida",@"Bouira",@"Tamanrasset",@"Tebessa",@"Tlemcen",@"Tiaret",
                            @"Tizi Ouzou",@"Alger",@"Djelfa",@"Jijel",@"Setif",@"Saida",@"Skikda",@"Sidi Bel Abbas",
                            @"Annaba",@"Guelma",@"Constantine",@"Medea",@"Mostaganem",@"Msila",@"Mascara",@"Ouargla"
                            ,@"Oran",@"El bayadh",@"Illizi",@"Borj bou arreridj",@"Boumerdes",@"El tarf",@"Tindouf"
                            ,@"Tessemsilt",@"El oued",@"Khenchela",@"Souk ahras",@"Tipaza",@"Mila",@"Ain defla"
                            ,@"Naama",@"Ain Timouchent",@"Ghardaia",@"Relizane"];
                }
                else if ([LocalizationGetCountry isEqualToString:@"SEN"]) {
                    src = @[@"Dakar",@"Diourbel",@"Fatick",@"Kaolack",@"Kolda",@"Louga",@"Matam",@"Saint - Louis",@"Tambacounda",@"Thiès",@"Ziguinchor"];
                }
                
                
                
            }
            else if([self.title  isEqual:SLocalizedString(@"Medecins", nil)])
            {
                
                if ([LocalizationGetCountry isEqualToString:@"ma"]) {
                    
                     src = @[@"chirurgien dentiste", @"gastro", @"chirurgie orthopédique, traumatologie", @"généraliste", @"psychiatrie", @"chirurgie générale", @"pédiatrie maladie des enfants", @"gynécologie", @"urologie", @"endocrinologie et maladie métabolique", @"chirurgie plastique", @"neurologie", @"ophtalmologie", @"radiologie", @"cardiologie et affection vasculaire", @"rhumatologie", @"dermato-vénéréologie", @"allergologie", @"neuropsychiatrie", @"psychologue psychanalyste", @"chirurgie maxillo-faciale", @"oto-rhino-laryngologie", @"pneumologie", @"anesthésie-réanimation", @"diabétologie-nutrition", @"chirurgie infantile", @"généraliste: acupuncture", @"angiologie, artères, veines, lymphatique", @"cancérologie", @"ostéopathie", @"médecine d'urgence", @"sexologue", @"généraliste: homéopathie"];
                
                }
                else if ([LocalizationGetCountry isEqualToString:@"TUN"]) {
                    
                    src = @[@"ANESTHESIE-REANIMAT", @"CARCINOLOGIE MEDICA", @"CARDIOLOGIE", @"CHI. CARDIOVASCULAI", @"CHIRURGIE CARCINOLO", @"CHIRURGIE GENERALE", @"CHIRURGIE INFANTILE", @"CHIRURGIE MAXILLO-F", @"CHIRURGIE ORTHOPEDI", @"CHIRURGIE PLASTIQUE", @"DERMATOLOGIE", @"ENDOCRINOLOGIE", @"GASTRO-HEPATHOLOGIE", @"GYNECOLOGIE-OBSTETR", @"HEMATOLOGIE", @"MALADIES INFECTIEUS", @"MEDECINE GENERALE", @"MEDECINE INTERNE", @"MEDECINE LEGALE", @"MEDECINE NUCLEAIRE", @"MEDECINE PHYSIQUE", @"NEUROCHIRURGIE", @"NEUROLOGIE", @"NUTRITION-DIETETIQU", @"OPHTALMOLOGIE", @"ORL ET STOMATOLOGIE", @"PARASITOLOGIE", @"PEDIATRIE", @"PNEUMOLOGIE", @"PSYCHIATRIE", @"RADIOLOGIE", @"RHUMATOLOGIE", @"UROLOGIE"];
                    
                }
                else if ([LocalizationGetCountry isEqualToString:@"ALGE"]) {
                   
                    src = @[@"Anatomie Pathologique", @"Anesthésie - Réanimation", @"Biochimie", @"Biologie Clinique", @"Biophysique", @"Chirurgie Cardio-Vasculaire", @"Chirurgie dentaire", @"Chirurgie Générale", @"Chirurgie Maxillo-Faciale", @"Chirurgie Orthopédique    ", @"Chirurgie Pédiatrique", @"Chirurgie ", @"Urologique", @"Dermatologie", @"Endocrinologie", @"Epidémiologie", @"Gastro-Entérologie", @"Gynéco-Obstetrique", @"Hematologie", @"Hémobiologie", @"Histo-embryologie ", @"Immunologie", @"Maladies Infectieuses", @"Médecine du sport", @"Médecine du Travail", @"Médecine Générale", @"Médecine Interne", @"Médecine Légale    ", @"Médecine Nucléaire", @"Microbiologie", @"Néphrologie", @"Neuro-Chirurgie", @"Neurologie", @"Oncologie Médicale        Ophtalmologie", @"ORL", @"Orthopédie", @"Parasitologie", @"Pédiatrie", @"PharmacologiePhysiologie", @"Pneumo-Phtisiologie    ", @"Psychiatrie", @"Radiologie", @"Radiothérapie", @"Reeducation et réadaptation fonctionnelle", @"Rhumatologie", @"Urologie"];
                    
                }
                else if ([LocalizationGetCountry isEqualToString:@"SEN"]) {
                    
                    src = @[@"Allergologie", @"Cardiologues", @"Chirurgie Générale et Urologie",
                            @"Chirurgie MF - Stomatologie", @"Chirurgie Orthopédique", @"Chirurgie Viscérale et Vasculaire", @"Economie de la santé", @"Entomologie", @"Gynécologie - Obstétrique", @"Hépato-Gastro-Enterologie", @"Imagerie médicale", @"Maladies Infectieuses", @"Médecine du Travail", @"Médecine Générale", @"Médecine Interne", @"Médecine Légale", @"Médecins Internistes", @"Néphrologie", @"Neurologie", @"Neuropsychiatrie", @"Ophtalmologues", @"ORL", @"Orthopédie-traumalologie", @"Pédiatres", @"Pneumo - Phtisiologie", @"Pneumologie Allergologie", @"Psychiatrie", @"Rhumatologues", @"Sante Publique", @"Sexologues", @"Urgentiste", @"Urologie"
                            ];

                }

               
            }
            
            else
            {
                if ([LocalizationGetCountry isEqualToString:@"ma"]) {
                    
                    src = @[@"Traumatologie", @"orthopédie", @"Vétérinaire", @"Ophalmologique", @"Cardio Vasculaire", @"Neuro-chirurgicale", @"Traumatologique", @"Orthopédique", @"Plastique", @"Esthétique", @"Cardiologique", @"Multidisciplinaire", @"Chirurgie générale", @"Cancerologie", @"Pédiatrique", @"Polyvalente", @"Dentaire", @"Ophtalmologie", @"Ophtalmologique", @"Cardio-vasculaire", @"Gynécologie", @"Polyclinique", @"Accouchement"];
                    
                }
                else if ([LocalizationGetCountry isEqualToString:@"TUN"]) {
                    
                    src = @[@"clinique", @"Infirmerie"];
                    
                }
                else if ([LocalizationGetCountry isEqualToString:@"ALGE"]) {
                    
                    src = @[@"Clinique Médicale", @"Clinique Médico-chirurgicale", @"Clinique en Ambulatoire", @"Centre d'Hémodialyse", @"Centre de Diagnostique", @"Centre de Procréation Médicale Assisté", @"Centre de Réeducation et Réadaptation Fonct..."];
                    
                }
                else if ([LocalizationGetCountry isEqualToString:@"SEN"]) {
                    
                    src = @[@"CENTRE D’EXPLOITATION,DE DIAGNOSTIC ET DE DIALYSE", @"CENTRES  HOSPITALIERS", @"CLINIQUES", @"GENERALISTE", @"LABORATOIRES D’ANALYSES MEDICALES", @"LABORATOIRES ET INDUSTRIES PHARMACEUTIQUES", @"LABORATOIRES VETERINAIRES", @"PEDIATRIQUES", @"PSYCHIATRIQUE"];
                    
                }
                
                
            }
            
            for (NSString *s in src) {
                
                if ([[s lowercaseString] hasPrefix:[query lowercaseString]]) {
                    [data addObject:s];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                resultBlock(data);
            });
        });
    }
}

#pragma mark - search

- (void)search
{
    [Message load:self message:nil detail:nil view:nil delay:0];
    
    if(self.find_by_likes.isOn) {
        NSMutableDictionary *tmp = [NSMutableDictionary dictionaryWithDictionary:params];
        [tmp setValue:@(YES) forKey:@"likes"];
        params = tmp;
    }
    
    [[WS api] postPath:url parameters:params
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   
                       if([(NSArray *)responseObject count] == 0) {
                           
                           [Message hide];
                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                                           message:SLocalizedString(@"Aucun resultat", nil)
                                                                          delegate:nil cancelButtonTitle:@"OK"
                                                                 otherButtonTitles:nil];
                           [alert show];
                           
                           return;
                       }
                       
                       Result * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"result"];
                       vc.datasource = [NSMutableArray arrayWithArray:(NSArray *)responseObject];
                       vc.nameValue = self.name.text;

                       [self addChildViewController:vc];
                   
                   [Message hide];
                   
               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   
                   NSLog(@"%@", error.description);
                   
                   [Message hide];
                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                                    message:SLocalizedString(@"Erreur de connexion", nil)
                                                                   delegate:nil cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                   [alert show];
               }];
}

- (IBAction)launch:(id)sender
{
    if (_find_by_position.selected)
    {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted )
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                            message:SLocalizedString(@"GPS desactivé", nil)
                                                           delegate:nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            NSLog(@"Launch by GPS");
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            
            locationManager = [[CLLocationManager alloc] init];
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            locationManager.delegate = self;
            [locationManager startUpdatingLocation];
            
            
        }
    }
    else
    {
        if(self.location.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                            message:SLocalizedString(@"Ville est obligatoire", nil)
                                                           delegate:nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            return;
        }
        
        if([self.title  isEqual:@"Medecins"])
        {
            NSLog(@"Launch doctors by location");
            params = @{@"q" : @{@"nom_cont": !(self.keywords.text) ? @"" : self.keywords.text,
                                @"ville_eq": [self.location.text capitalizedString],
                                @"mots_clefs_cont" : !(self.speciality.text) ? @"" : self.speciality.text}};
            
        }
        else
        {
            NSLog(@"Launch clinics by location");
            params = @{@"q" : @{@"nom_cont": !(self.keywords.text) ? @"" : self.keywords.text,
                                @"activites_cont" : !(self.speciality.text) ? @"" : self.speciality.text,
                                @"categorie_eq" : @"clinique",
                                @"ville_eq": [self.location.text capitalizedString]}};
        }
        
        [self search];
    }
}

#pragma mark - geo delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (newLocation != nil)
    {
        [locationManager stopUpdatingLocation];
        locationManager = nil;
        locationManager.delegate = nil;
        
        NSString *coordinate = [NSString stringWithFormat:@"%f,%f", newLocation.coordinate.latitude, newLocation.coordinate.longitude];
        
        if([self.title  isEqual:@"Medecins"])
        {
            params = @{@"coordinate": coordinate,
                       @"q" : @{@"nom_cont": !(self.keywords.text) ? @"" : self.keywords.text,
                                @"mots_clefs_cont" :!(self.speciality.text) ? @"" : self.speciality.text}};
        }
        else
        {
            params = @{@"coordinate": coordinate,
                       @"q" : @{@"nom_cont": !(self.keywords.text) ? @"" : self.keywords.text,
                                @"activites_cont" : !(self.speciality.text) ? @"" : self.speciality.text,
                                @"categorie_cont" : @"clinique"}};
        }
        
        [self search];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [manager stopUpdatingLocation];
    manager = nil;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                    message:SLocalizedString(@"GPS warning", nil)
                                                   delegate:nil cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    
}

@end
