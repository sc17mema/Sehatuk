//
//  Detail.m
//  espace sante
//
//  Created by abdel ali on 1/17/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "Detail.h"
#import "Result.h"

@implementation Detail

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = self.title;
    
    self.presentation.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    self.presentation.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.presentation.text = _data[@"nom"];
    
    self.lbl_address.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    self.lbl_address.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.lbl_address.text = SLocalizedString(@"Adresse", nil);
    
    self.address.font = [UIFont fontWithName:@"DroidSans" size:13];
    self.address.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.address.text = _data[@"adresse"];
    
    self.lbl_activity.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    self.lbl_activity.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.lbl_activity.text = ([self.title isEqualToString:SLocalizedString(@"Pharmacies de garde", nil)])?SLocalizedString(@"Ville", nil):SLocalizedString(@"Activités", nil);
    
    self.activity.font = [UIFont fontWithName:@"DroidSans" size:13];
    self.activity.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    if([self.title isEqualToString:SLocalizedString(@"Pharmacies de garde", nil)])
        self.activity.text = _data[@"ville"];
    else
        self.activity.text = [_data[@"activites"] stringByReplacingOccurrencesOfRegex:@"^Activités :\\s" withString:@""];
    
    self.lbl_contact.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    self.lbl_contact.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.lbl_contact.text = SLocalizedString(@"Contact", nil);
    
    self.contact.font = [UIFont fontWithName:@"DroidSans" size:13];
    self.contact.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.contact.text = _data[@"tel"];
    
    self.likes.font = [UIFont fontWithName:@"DroidSans" size:12];
    self.likes.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.likes.text = [NSString stringWithFormat:@"%d", [_data[@"likes"] integerValue]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self options:self.options];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self options:self.options];
    [self.delegate setLikes:self.data];
    
    [super viewWillDisappear:animated];
}

#pragma mark - properties

- (ALRadialMenu *)menu
{
    if(_menu == nil)
    {
        self.menu = [[ALRadialMenu alloc] init];
        self.menu.delegate = self;
    }
    
    return _menu;
}

#pragma mark - uiviewcontrolelr container

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    self.view.frame = parent.view.frame;
    
    [[parent view] addSubview:[self view]];
}

#pragma mark - radial menu delegate methods

- (NSInteger) numberOfItemsInRadialMenu:(ALRadialMenu *)radialMenu {
    return ([self.title isEqualToString:SLocalizedString(@"Pharmacies de garde", nil)]) ? 3 : 4;
}

- (NSInteger) arcSizeForRadialMenu:(ALRadialMenu *)radialMenu {
	
    return -100;
}

- (NSInteger) arcRadiusForRadialMenu:(ALRadialMenu *)radialMenu {
	
    return 80;
}

- (NSInteger) arcStartForRadialMenu:(ALRadialMenu *)radialMenu {
	
    return -40;
}

- (UIImage *) radialMenu:(ALRadialMenu *)radialMenu imageForIndex:(NSInteger) index {
	
    if([self.title isEqualToString:SLocalizedString(@"Pharmacies de garde", nil)])
    {
        switch (index) {
            case 1:
                return [UIImage imageNamed:@"share.png"];
                break;
            case 2:
                return [UIImage imageNamed:@"favorit.png"];
                break;
            case 3:
                return [UIImage imageNamed:@"update.png"];
                break;
        }
    }
    else {
        switch (index) {
            case 1:
                return [UIImage imageNamed:@"like.png"];
                break;
                
            case 2:
                return [UIImage imageNamed:@"share.png"];
                break;
            
            case 3:
                return [UIImage imageNamed:@"favorit.png"];
                break;
                
            case 4:
                return [UIImage imageNamed:@"update.png"];
                break;
        }
    }
    
	
	return nil;
}


- (void) radialMenu:(ALRadialMenu *)radialMenu didSelectItemAtIndex:(NSInteger)index {
	
    if([self.title isEqualToString:SLocalizedString(@"Pharmacies de garde", nil)])
    {
        switch (index) {
            case 1:
                [self share];
                break;
            case 2:
                [self bookmark:_data];
                break;
            case 3:
                [self update];
                break;
        }
    }
    else {
        switch (index) {
            case 1:
                [self vote];
                break;
            case 2:
                [self share];
                break;
            case 3:
                [self bookmark:_data];
                break;
            case 4:
                [self update];
                break;
                
            default:
                break;
        }
    }
    
}

- (float)buttonSizeForRadialMenu:(ALRadialMenu *)radialMenu
{
    return 40.0f;
}

#pragma mark - actions

- (IBAction)options:(UIButton *)sender {
    
	[self.menu buttonsWillAnimateFromButton:sender withFrame:sender.frame inView:self.view];
}

- (IBAction)route:(UIButton *)sender
{
    NSLog(@"Route");
    Place *to = [[Place alloc] init];
    to.name = self.data[@"nom"];
    to.description = self.data[@"adresse"];
    to.latitude = [self.data[@"latitude"] doubleValue];
    to.longitude = [self.data[@"longitude"] doubleValue];
    
    Map * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"map"];
    vc.to = to;
    
    [self addChildViewController:vc];
    
    to = nil;
    vc = nil;
}

#pragma mark - function

- (void)bookmark:(NSDictionary *)item
{
    NSMutableDictionary *bookmarks = [File getBookMarksFromFile:@"bookmarks"];

    if(![bookmarks[self.title] containsObject:item])
        [File appendData:_data toKey:self.title intoFile:@"bookmarks"];
    
    [Message success:self message:SLocalizedString(@"Ajout aux favorix", nil) delay:1];
}

- (void)share
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:SLocalizedString(@"Annuler", nil)
                                         destructiveButtonTitle:nil otherButtonTitles:@"Facebook", @"E-mail", @"SMS", nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark - action sheet delegate

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex)
    {
        case 0:
            [self facebook];
            NSLog(@"Facebook sharing");
            break;
        case 1:
            [self email];
            NSLog(@"Email sharing");
            break;
        case 2:
            [self SMS];
            NSLog(@"SMS sharing");
            break;
        default:
            break;
    }
}

- (void)vote
{
    NSString *user = [SSKeychain passwordForService:@"781409756" account:@"UID"];
    NSString *target = ([self.title isEqualToString:SLocalizedString(@"Medecins", nil)] ? @"medecins" : @"centres");
    NSString *q = [NSString stringWithFormat:@"%@/%@/vote_for/%@", target, user, _data[@"id"]];
    
    [Message load:self message:nil detail:nil view:nil delay:0];
    
    [[WS api] getPath:q parameters:nil
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  if(responseObject != nil)
                  {
                      self.data = nil;
                      self.data = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                      
                      [Message success:self message:nil delay:1];
                      self.likes.text = [NSString stringWithFormat:@"%d", [_data[@"likes"] integerValue]];
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              }];
}

- (void)update
{
    [self.view.window.rootViewController addChildViewController:self.crud];
}

- (Crud *)crud
{
    if(_crud == nil)
    {
        self.crud = [self.storyboard instantiateViewControllerWithIdentifier:@"crud"];
        self.crud.data = [NSMutableDictionary dictionaryWithDictionary:_data];
        self.crud.title = self.title;
    }
    
    
    return _crud;
}

- (IBAction)call:(UIButton *)sender
{
    NSLog(@"Call");
    NSString *tel = [_contact.text stringByReplacingOccurrencesOfString:@" " withString:@""];//[_contact.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    tel = [@"telprompt:" stringByAppendingString:tel];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
}

#pragma mark - facebook

- (NSString *)initialText
{
    NSString *status = @"";
    
    if([self.title isEqualToString:SLocalizedString(@"Hopitaux", nil)]) {
        status = @"l'hopital";
    }
    else if([self.title isEqualToString:SLocalizedString(@"Cliniques", nil)]) {
        status = @"la clinique";
    }
    else if([self.title isEqualToString:SLocalizedString(@"Medecins", nil)]) {
        status = @"le medecin";
    }
    else if([self.title isEqualToString:SLocalizedString(@"Laboratoires", nil)]) {
        status = @"le laboratoire";
    }
    else if([self.title isEqualToString:SLocalizedString(@"Centres de radiologie", nil)]) {
        status = @"le centre de radiologie";
    }
    else if([self.title isEqualToString:SLocalizedString(@"Para-Pharmacies", nil)]) {
        status = @"la parapharmacie";
    }
    else if([self.title isEqualToString:SLocalizedString(@"Pharmacies de garde", nil)]) {
        status = @"la pharmacie";
    }
    
    return [NSString stringWithFormat:@"je vous recommande %@ :\n%@\nsitué à :\n%@\nTel : %@", status, self.presentation.text, self.address.text, self.contact.text];
}

#pragma mark - facebook

- (void)facebook
{
    // if the session is open, then load the data for our view controller
    if (!FBSession.activeSession.isOpen) {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession openActiveSessionWithPublishPermissions:@[@"email, publish_actions"]
                                           defaultAudience:FBSessionDefaultAudienceEveryone
                                              allowLoginUI:YES
                                         completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                             if (!error && status == FBSessionStateOpen) {
                                                 [self postToFacebookWall];
                                             }else{
                                                 if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled || [FBErrorUtility errorCategoryForError:error] == FBErrorCategoryPermissions) {
                                                     [Message warn:self message:@"Permission non accordée" delay:2];
                                                 } else {
                                                     [Message warn:self message:@"Quelque chose a mal tourné" delay:2];
                                                 }
                                             }
                                         }];
        return;
    }

    [self postToFacebookWall];
    
}

- (void)postToFacebookWall
{
    [Message load:self message:nil detail:nil view:nil delay:0];
    
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       @"http://sehatuk.dialy.net/", @"link",
                                       nil, @"picture",
                                       @"Sehatuk", @"name",
                                       @"Sehatuk est votre espace de santé", @"caption",
                                       @"Sehatuk fait partie d’une nouvelle série d’applications lancées par Dial Technologies.", @"description",
                                       nil];
        
        [params setObject:[self initialText] forKey:@"message"];
        
        [FBRequestConnection  startWithGraphPath:@"/me/feed" parameters:params HTTPMethod:@"POST"
                               completionHandler:^(FBRequestConnection *connection,id result,NSError *error) {
                                   
                                   if (!error) {
                                       [Message success:self message:nil delay:1];
                                   } else {
                                       
                                       if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled || [FBErrorUtility errorCategoryForError:error] == FBErrorCategoryPermissions) {
                                           [Message warn:self message:@"Permission non accordée" delay:2];
                                       } else {
                                           [Message warn:self message:@"Quelque chose a mal tourné" delay:2];
                                       }
                                   }
                                   
                               }];
}

- (void)SMS
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
	if([MFMessageComposeViewController canSendText])
	{
		controller.body = [self initialText];
		controller.recipients = nil;
		controller.messageComposeDelegate = self;
		[self.view.window.rootViewController presentViewController:controller animated:YES completion:nil];
	}
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			break;
		case MessageComposeResultFailed:
			break;
		case MessageComposeResultSent:
            
			break;
		default:
			break;
	}
    
	[controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)email
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Sehatuk"];
        [mailer setMessageBody:[self initialText] isHTML:NO];
        
        [self.view.window.rootViewController presentViewController:mailer animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}


@end
