//
//  Centers.m
//  espace sante
//
//  Created by abdel ali on 23/09/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Centers.h"

@implementation Centers

- (void)viewDidAppear:(BOOL)animated
{
    [Flurry logEvent:@"Centers"];
}

- (void)viewDidLoad
{
    self.name.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.name.text = SLocalizedString(@"Centres de santé", nil);
    
    [self datasource];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.dataSource = self;
    self.delegate = self;
    
    [super viewDidLoad];
    
    CGRect frame = CGRectMake(20, self.band.frame.size.height + 25, 20, 16);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        frame.origin.y += 20;
    }
    
    indicator = [[UIImageView alloc] initWithFrame:frame];
    [indicator setImage:[UIImage imageNamed:@"indicateur.png"]];
    [indicator setContentMode:UIViewContentModeScaleAspectFit];
    [self.view insertSubview:indicator atIndex:[[self.view subviews] count]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    indicator = nil;
    self.datasource = nil;
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - parametres

- (NSArray *)datasource
{
    if(_datasource == nil) {
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"datasource" ofType:@"plist"];
        self.datasource = [NSArray arrayWithContentsOfFile:path];
        path = nil;
    }
    
    return _datasource;
}

#pragma mark - tabs datasource

- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return _datasource.count;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UIImageView *tab = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [tab setImage:[UIImage imageNamed:@"bouton_normal.png"]];
    [tab setContentMode:UIViewContentModeScaleAspectFit];
    
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [icon setImage:[UIImage imageNamed:_datasource[index][@"image"]]];
    [icon setContentMode:UIViewContentModeScaleAspectFit];
    
    [tab addSubview:icon];
    
    icon = nil;
    
    return tab;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    
        
    NSString *title = self.datasource[index][@"titre"];
    
    UIViewController *cvc =[[UIViewController alloc] init];
    UIImageView * back = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fond_ecran_secondaire.png"]];
    back.frame=cvc.view.frame;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(60, 150, 200, 60)];
    label.numberOfLines =2;
    label.textAlignment = NSTextAlignmentCenter;
    label.text =SLocalizedString(@"Prochainement dans une version ultérieure", nil);
    label.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    label.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    [cvc.view addSubview:back];
    [cvc.view addSubview:label];
    
    if ([LocalizationGetCountry isEqualToString:@"ma"]) {
        
        cvc = [self.storyboard instantiateViewControllerWithIdentifier:self.datasource[index][@"id"]];
        cvc.title = SLocalizedString(_datasource[index][@"titre"], nil);
                
    }
    else if ([LocalizationGetCountry isEqualToString:@"TUN"]) {
        
        if ([title isEqualToString:@"Pharmacies"] || [title isEqualToString:@"Para-Pharmacies"] || [title isEqualToString:@"SOS"] ) {
            
            return  cvc;
            
        }else{
            
            cvc = [self.storyboard instantiateViewControllerWithIdentifier:self.datasource[index][@"id"]];
            cvc.title = SLocalizedString(_datasource[index][@"titre"], nil);
        }
        
    }
    else if ([LocalizationGetCountry isEqualToString:@"ALGE"]) {
        
        if ([title isEqualToString:@"Pharmacies"] || [title isEqualToString:@"Para-Pharmacies"] || [title isEqualToString:@"SOS"] || [title isEqualToString:@"Laboratoires"] || [title isEqualToString:@"Centres de radiologie"]) {
            
            return cvc;
        }else{
            cvc = [self.storyboard instantiateViewControllerWithIdentifier:self.datasource[index][@"id"]];
            cvc.title = SLocalizedString(_datasource[index][@"titre"], nil);
            
        }
    }
    else if ([LocalizationGetCountry isEqualToString:@"SEN"]) {
        
        
        if ([title isEqualToString:@"Pharmacies"] || [title isEqualToString:@"Para-Pharmacies"] || [title isEqualToString:@"SOS"] || [title isEqualToString:@"Centres de radiologie"]) {
            return cvc;
        }else{
            cvc = [self.storyboard instantiateViewControllerWithIdentifier:self.datasource[index][@"id"]];
            cvc.title = SLocalizedString(_datasource[index][@"titre"], nil);
        }
        
    }

    
    return  cvc;

}

#pragma mark - Tabs delegate

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value
{
    float y = _band.frame.origin.y + _band.frame.size.height;
    
    switch (option) {
        case ViewPagerOptionTabLocation:
            return y;
            break;
        case ViewPagerOptionTabWidth:
            return 60;
            break;
        case ViewPagerOptionTabHeight:
            return 50;
            break;
        default:
            break;
    }
    
    return value;
}
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerIndicator:
            return [[UIColor redColor] colorWithAlphaComponent:0.64];
            break;
        case ViewPagerTabsView:
            return [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
        default:
            break;
    }
    
    return color;
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index
{
    self.ref = viewPager.pageViewController.viewControllers[0];
    NSDictionary *centerTabDict = self.datasource[index];
    [Flurry logEvent:centerTabDict[@"titre"]];
    //NSLog(@"I changed the tab %@", centerTabDict[@"titre"]);
}

- (void)viewPager:(ViewPagerController *)viewPager didAnimateTab:(CGFloat)position
{
    float x = position - [self.tabsView contentOffset].x;
    
    CGRect frame = indicator.frame;
    
    [UIView beginAnimations:@"move" context:nil];
    [UIView setAnimationDuration:0.2];
    
    frame.origin.x = x;
    frame.origin.x += 20;
    indicator.frame = frame;
    
    [UIView commitAnimations];
}

#pragma mark - navigation

//- (IBAction)pop:(id)sender
//{
//    if([self.ref respondsToSelector:@selector(didAddViewToParentController:)])
//    {
//        if(![self.ref didAddViewToParentController:self])
//        {
//            [self.tabsView setContentOffset:CGPointMake(0, 0)];
//            [self.navigationController popViewControllerAnimated:YES];
//        }
//    }
//    else
//    {
//        [self.tabsView setContentOffset:CGPointMake(0, 0)];
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    
//    if([self.ref respondsToSelector:@selector(didMoveToParentController:)])
//    {
//        [self.ref didMoveToParentController:self];
//    }
//}

- (IBAction)pop:(id)sender
{
    UIViewController *actualSuperView = [self getSuperView:(UIViewController *)self.ref];
    
    if([actualSuperView isEqual:self.ref])
    {
        [self.navigationController popViewControllerAnimated:YES];
        [self setActiveTabIndex:0];
        return;
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    transition.delegate = self;
    
    [actualSuperView.view.superview.layer addAnimation:transition forKey:nil];
    [actualSuperView.view removeFromSuperview];
    [actualSuperView willMoveToParentViewController:nil];
    [actualSuperView removeFromParentViewController];
}

- (UIViewController *)getSuperView:(UIViewController *)parent
{
    if(parent.childViewControllers.count > 0)
        return [self getSuperView:[parent.childViewControllers lastObject]];
    
    return parent;
}

@end
