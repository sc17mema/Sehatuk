//
//  MySpace.m
//  espace sante
//
//  Created by abdel ali on 12/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "MySpace.h"

@implementation MySpace

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"My Space");
}

- (void)viewDidLoad
{
    [self initialize];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.dataSource = self;
    self.delegate = self;

    [super viewDidLoad];
    
    CGRect frame = CGRectMake(20, self.band.frame.size.height + 25, 20, 16);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        frame.origin.y += 20;
    }
    
    indicator = [[UIImageView alloc] initWithFrame:frame];
    [indicator setImage:[UIImage imageNamed:@"indicateur.png"]];
    [indicator setContentMode:UIViewContentModeScaleAspectFit];
    [self.view insertSubview:indicator atIndex:[[self.view subviews] count]];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"LanguagesDidChange" object:nil queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      [self reloadData];
                                                      [self initialize];
                                                      [self setActiveTabIndex:0];
                                                  }];
}

#pragma mark - parametres

- (void)initialize
{
    self.datasource = @[@{@"id": @"parametres", @"image" : @"parametres.png", @"titre" : SLocalizedString(@"Paramètres", nil)},
                        @{@"id": @"bookmarks", @"image" : @"bookmarks.png", @"titre" : SLocalizedString(@"Favoris", nil)},
                        @{@"id": @"newprofessionnel", @"image": @"edit.png", @"titre": SLocalizedString(@"Nouvel Enregistrement", nil)},
                        @{@"id": @"contactus", @"image": @"contact-us.png", @"titre": SLocalizedString(@"Contactez-nous", nil)},
                        @{@"id": @"copyrights", @"image" : @"agenda.png", @"titre" : SLocalizedString(@"Mentions Légales", nil)}];
    
    self.name.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.name.text = SLocalizedString(@"Mon espace", nil);
}

#pragma mark - Tabs datasource  

- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return _datasource.count;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UIImageView *tab = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [tab setImage:[UIImage imageNamed:@"bouton_normal.png"]];
    [tab setContentMode:UIViewContentModeScaleAspectFit];
    
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [icon setImage:[UIImage imageNamed:_datasource[index][@"image"]]];
    [icon setContentMode:UIViewContentModeScaleAspectFit];
    
    [tab addSubview:icon];
    icon = nil;
    
    return tab;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    
    UIViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:_datasource[index][@"id"]];
    cvc.title = _datasource[index][@"titre"];
    
    if([cvc isKindOfClass:[Bookmarks class]])
    {
        [(Bookmarks *)cvc setDatasource:[NSMutableDictionary dictionaryWithDictionary:[File getBookMarksFromFile:@"bookmarks"]]];
    }
    
    return cvc;
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index
{
    self.ref = viewPager.pageViewController.viewControllers[0];
    NSLog(@"I changed the tab %@", self.datasource[index]);
}

#pragma mark - Tabs delegate

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value
{
    float y = _band.frame.origin.y + _band.frame.size.height;
    
    switch (option) {
        case ViewPagerOptionTabLocation:
            return y;
            break;
        case ViewPagerOptionTabWidth:
            return 60;
            break;
        case ViewPagerOptionTabHeight:
            return 50;
            break;
        default:
            break;
    }
    
    return value;
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerIndicator:
            return [[UIColor redColor] colorWithAlphaComponent:0.64];
            break;
        case ViewPagerTabsView:
            return [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
        default:
            break;
    }
    
    return color;
}

- (void)viewPager:(ViewPagerController *)viewPager didAnimateTab:(CGFloat)position
{
    float x = position - [self.tabsView contentOffset].x;
    
    CGRect frame = indicator.frame;
    
    [UIView beginAnimations:@"move" context:nil];
    [UIView setAnimationDuration:0.2];
    
    frame.origin.x = x;
    frame.origin.x += 20;
    indicator.frame = frame;
    
    [UIView commitAnimations];
}

#pragma mark - navigation

//- (IBAction)pop:(id)sender
//{
//    if(_ref.childViewControllers.count > 0)
//    {
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.3f;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromLeft;
//        transition.delegate = self;
//        
//        UIViewController *tmp = (UIViewController *)_ref.childViewControllers[0];
//        
//        [tmp.view.superview.layer addAnimation:transition forKey:nil];
//        [tmp.view removeFromSuperview];
//        [tmp willMoveToParentViewController:nil];
//        [tmp removeFromParentViewController];
//        
//        tmp = nil;
//        
//        return;
//    }
//    
//    [self.navigationController popViewControllerAnimated:YES];
//}

- (IBAction)pop:(id)sender
{
    UIViewController *actualSuperView = [self getSuperView:(UIViewController *)self.ref];
    
    if([actualSuperView isEqual:self.ref])
    {
        [self.navigationController popViewControllerAnimated:YES];
        [self setActiveTabIndex:0];
        return;
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    transition.delegate = self;
    
    [actualSuperView.view.superview.layer addAnimation:transition forKey:nil];
    [actualSuperView.view removeFromSuperview];
    [actualSuperView willMoveToParentViewController:nil];
    [actualSuperView removeFromParentViewController];
}

- (UIViewController *)getSuperView:(UIViewController *)parent
{
    if(parent.childViewControllers.count > 0)
        return [self getSuperView:[parent.childViewControllers lastObject]];
    
    return parent;
}

@end
