//
//  Advices.h
//  espace sante
//
//  Created by abdel ali on 8/23/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import <Social/Social.h>
#import "AdviceCell.h"
#import "WS.h"
#import "Message.h"
#import "LocalizationSystem.h"

@interface Advices : UIViewController<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate>
{
    NSInteger _currentPage;
    NSInteger _totalPages;
    NSIndexPath *_indexPath;
}

@property (weak, nonatomic) IBOutlet UILabel *titleView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* datasource;

@end