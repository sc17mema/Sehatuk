//
//  AdviceCell.m
//  espace sante
//
//  Created by abdel ali on 8/23/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "AdviceCell.h"

@implementation AdviceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    // Initialization code
}

- (void)prepareForReuse
{
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if(selected)
    {
        [self.descLabel setNumberOfLines:0];
        [self.descLabel sizeToFit];
        [self.expandImage setImage:[UIImage imageNamed:@"contract.png"]];
        self.postToFacebook.hidden = NO;
    }
    else
    {
        [self.descLabel setNumberOfLines:1];
        [self.expandImage setImage:[UIImage imageNamed:@"expand.png"]];
        [self.descLabel setFrame:CGRectMake(20, 10, 250, 21)];
        self.postToFacebook.hidden = YES;
    }
}

@end
