//
//  ContactUs.m
//  espace sante
//
//  Created by abdel ali on 4/14/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "ContactUs.h"

@interface ContactUs () {
    validation *validate;
}
@end

@implementation ContactUs

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self init_settings];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - settings

- (void)init_settings
{
    self.name.text = self.title;
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    self.lbl_name.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_name.textColor = [UIColor grayColor];
    self.lbl_name.text = SLocalizedString(@"Nom & Prénom", nil);
    self.txt_name.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_name.textColor = [UIColor blackColor];
    
    self.lbl_mail.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_mail.textColor = [UIColor grayColor];
    self.lbl_mail.text = SLocalizedString(@"Email", nil);
    self.txt_mail.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_mail.textColor = [UIColor blackColor];
    
    self.lbl_msg.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_msg.textColor = [UIColor grayColor];
    self.lbl_msg.text = SLocalizedString(@"Message", nil);
    self.txt_msg.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_msg.textColor = [UIColor blackColor];
    self.txt_msg.delegate = self;
    
    self.lbl_tele.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_tele.textColor = [UIColor grayColor];
    self.lbl_tele.text = SLocalizedString(@"Telephone", nil);
    self.txt_tele.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_tele.textColor = [UIColor blackColor];
    self.txt_tele.delegate = self;
    
    [self.container contentSizeToFit];
}

#pragma mark -
#pragma mark action

- (IBAction)sendMail:(id)sender {
    
    validate = [[validation alloc] init];
    [validate Email:self.txt_mail FieldName:@"Mail"];
    [validate Required:self.txt_name FieldName:@"Nom"];
    [validate Required:self.txt_tele FieldName:@"Tele"];
    [validate isValid];
    
    if(![validate textFiledIsValid])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                        message:@"Veuillez vérifier les champs obligatoires"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    NSDictionary *params = @{@"txt" : [NSString stringWithFormat:@"Plateforme :iOS\nFrom : %@\nTelephone : %@\n%@ : %@", self.txt_mail.text, self.txt_tele, self.txt_name.text, self.txt_msg.text],
                             @"obj" : @"Prise de contact"};
    
    NSLog(@"Contact us button tapped");
    
    
    [Message load:self message:nil detail:nil view:nil delay:0];
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://ns29646.ovh.net:8080/"]];
    [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [client getPath:@"InfoServicesSeha/SendMailSeha" parameters:params
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [Message success:self message:nil delay:1];
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [Message failed:self message:nil delay:1];
            }];
    
}

#pragma mark -
#pragma mark textfield delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end
