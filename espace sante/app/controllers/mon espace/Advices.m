//
//  Advices.m
//  espace sante
//
//  Created by abdel ali on 8/23/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "Advices.h"

const int kTextLabelWidth = 250;
const int kLoadingAdviceCellTag = 200;

@implementation Advices

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.titleView.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.titleView.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.titleView.text = SLocalizedString(@"Conseils", nil);
    
    _currentPage = 1;
    self.datasource = [NSMutableArray array];
    
    [self performFetch];
    
    [Flurry logEvent:@"Conseils" timed:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - perform fetch

- (void)performFetch
{
    [Message load:self message:nil detail:nil view:nil delay:0];
    
    NSString *path = [NSString stringWithFormat:@"conseils/%d", _currentPage];
    
    NSMutableURLRequest *request = [[WS api] requestWithMethod:@"get" path:path parameters:nil];
    AFJSONRequestOperation *operation = [[AFJSONRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [Message hide];
        
         _totalPages = [[responseObject objectForKey:@"total_pages"] intValue];
         
         for (id dic in [responseObject objectForKey:@"result"])
         {
             if (![self.datasource containsObject:dic])
                 [self.datasource addObject:dic];
         }
        
        [self.tableView reloadData];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [Message hide];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                         message:SLocalizedString(@"Erreur de connexion", nil)
                                                        delegate:nil cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
         [alert show];
     }];
    
    [operation start];
}

#pragma mark - loading cell

- (UITableViewCell *)loadingCell
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activity.center = cell.center;
    [cell addSubview:activity];
    [cell setTag:kLoadingAdviceCellTag];
    [activity startAnimating];
    
    activity = nil;
    
    return cell;
}

#pragma mark - table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_currentPage == 0)
        return 1;
    
    if (_currentPage < _totalPages)
        return self.datasource.count + 1;
    
    return self.datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.datasource.count)
    {
        static NSString *cellIdentifier = @"advice";
        
        AdviceCell *cell = (AdviceCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil)
        {
            cell = [[AdviceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        cell.descLabel.text = self.datasource[indexPath.row][@"description"];
        cell.descLabel.font = [UIFont fontWithName:@"Droid Sans" size:14];
        cell.descLabel.textColor = [UIColor colorWithRed:0.039 green:0.502 blue:0.608 alpha:1];
        
        return cell;
    }
    else
    {
        return [self loadingCell];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (cell.tag == kLoadingAdviceCellTag)
    {
        _currentPage++;
        [self performFetch];
    }
    if (indexPath.row % 2 == 0)
    {
        cell.backgroundColor = [UIColor colorWithRed:0.537 green:0.537 blue:0.537 alpha:.15];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_indexPath && indexPath.row == _indexPath.row)
    {
        NSString *text = self.datasource[indexPath.row][@"description"];
        CGSize textSize = [text sizeWithFont:[UIFont fontWithName:@"Droid Sans" size:14]
                           constrainedToSize:CGSizeMake(kTextLabelWidth, 300)
                               lineBreakMode:NSLineBreakByWordWrapping];
        
        return (textSize.height + 20);
    }
    
    return 41;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _indexPath = [indexPath copy];
    
    [tableView beginUpdates];
    [tableView endUpdates];
}

- (IBAction)postToFacebook:(UIButton *)sender
{
    NSLog(@"Facebook post");
    AdviceCell *cell = (AdviceCell *)sender.superview.superview;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
        //cell = (AdviceCell *)sender.superview.superview.superview;
        cell = (AdviceCell *)sender.superview.superview;
    
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
    [controller setInitialText:cell.descLabel.text];
    [controller addURL:[NSURL URLWithString:@"http://sehatuk.dialy.net/"]];
    [self presentViewController:controller animated:YES completion:Nil];
}

@end
