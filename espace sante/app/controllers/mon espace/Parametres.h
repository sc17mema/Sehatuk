//
//  Parametres.h
//  espace sante
//
//  Created by abdel ali on 9/4/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import "SwitchOnOff.h"
#import "SSKeychain.h"
#import "Message.h"
#import "LocalizationSystem.h"

@interface Parametres : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *titleView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
