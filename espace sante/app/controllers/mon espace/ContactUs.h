//
//  ContactUs.h
//  espace sante
//
//  Created by abdel ali on 4/14/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "Validation.h"
#import "Message.h"
#import "LocalizationSystem.h"

@interface ContactUs : UIViewController<UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *container;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UITextField *txt_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_mail;
@property (weak, nonatomic) IBOutlet UITextField *txt_mail;
@property (weak, nonatomic) IBOutlet UILabel *lbl_msg;
@property (weak, nonatomic) IBOutlet UITextView *txt_msg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tele;
@property (weak, nonatomic) IBOutlet UITextField *txt_tele;

@end


