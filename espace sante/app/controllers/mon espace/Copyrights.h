//
//  Copyrights.h
//  espace sante
//
//  Created by abdel ali on 12/5/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FTCoreText/FTCoreTextView.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "LocalizationSystem.h"

@interface Copyrights : UIViewController<FTCoreTextViewDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) FTCoreTextView *text;

@end
