//
//  Bookmarks.h
//  espace sante
//
//  Created by abdel ali on 04/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MCSwipeTableViewCell.h>
#import "Item.h"
#import "Resources.h"
#import "Place.h"
#import "Map.h"
#import "File.h"
#import "Message.h"

@interface Bookmarks : UIViewController<UITableViewDataSource, UITableViewDelegate, MCSwipeTableViewCellDelegate>
{
}

@property (nonatomic, strong) NSMutableDictionary *datasource;
@property (nonatomic, strong) Place *from;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

