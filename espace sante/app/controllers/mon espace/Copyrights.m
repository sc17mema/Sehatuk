//
//  Copyrights.m
//  espace sante
//
//  Created by abdel ali on 12/5/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Copyrights.h"


@implementation Copyrights

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = SLocalizedString(@"Mentions Légales", nil);
    
    CGRect rect = _scrollview.bounds;
    rect.origin.y = 0;
    
    //  Create FTCoreTextView
    self.text = [[FTCoreTextView alloc] initWithFrame:CGRectInset(rect, 20.0f, 20.f)];
	self.text.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.text.backgroundColor = [UIColor clearColor];
    //  Add custom styles
    [self.text addStyles:[self coreTextStyle]];
    //  Set the custom-formatted text to FTCoreTextView
    self.text.text = [self textForView];
    self.text.delegate = self;
    
    [self.scrollview addSubview:self.text];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.text fitToSuggestedHeight];
    [self.scrollview setContentSize:CGSizeMake(CGRectGetWidth(self.scrollview.bounds), CGRectGetMaxY(self.text.frame)+20.0f)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - navigation

- (IBAction)pop:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - load static content

- (NSString *)textForView
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"copyrights" ofType:nil];
    return [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}

#pragma mark - styling

- (NSArray *)coreTextStyle
{
    NSMutableArray *result = [NSMutableArray array];
    
    //  default style tag
	FTCoreTextStyle *defaultStyle = [FTCoreTextStyle new];
	defaultStyle.name = FTCoreTextTagDefault;
	defaultStyle.font = [UIFont fontWithName:@"Droid Sans" size:14.f];
	defaultStyle.textAlignment = FTCoreTextAlignementNatural;
    defaultStyle.color = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
	[result addObject:defaultStyle];
	
    //  style using convension tag
	FTCoreTextStyle *titleStyle = [FTCoreTextStyle styleWithName:@"title"];
	titleStyle.font = [UIFont fontWithName:@"DroidSans-Bold" size:16];
	titleStyle.paragraphInset = UIEdgeInsetsMake(10.f, 0, 10.f, 0);
	titleStyle.textAlignment = FTCoreTextAlignementCenter;
    titleStyle.color = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
	[result addObject:titleStyle];
	
    // link style
    FTCoreTextStyle *linkStyle = [defaultStyle copy];
	linkStyle.name = FTCoreTextTagLink;
	linkStyle.color = [UIColor colorWithRed:248/255.0 green:182/255.0 blue:0/255.0 alpha:1.0];
	[result addObject:linkStyle];
	
	FTCoreTextStyle *subtitleStyle = [FTCoreTextStyle styleWithName:@"subtitle"];
	subtitleStyle.font = [UIFont fontWithName:@"DroidSans-Bold" size:14.f];
	subtitleStyle.color = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
	subtitleStyle.paragraphInset = UIEdgeInsetsMake(10, 0, 10, 0);
	[result addObject:subtitleStyle];
	
    //  This will be list of items
    //  You can specify custom style for a bullet
	FTCoreTextStyle *bulletStyle = [defaultStyle copy];
	bulletStyle.name = FTCoreTextTagBullet;
	bulletStyle.bulletFont = [UIFont fontWithName:@"Droid Sans" size:14.f];
	bulletStyle.bulletColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
	bulletStyle.bulletCharacter = @"→";
	bulletStyle.paragraphInset = UIEdgeInsetsMake(0, 20.f, 0, 0);
	[result addObject:bulletStyle];
    
    return  result;
}

#pragma mark - core text delegate

- (void)coreTextView:(FTCoreTextView *)coreTextView receivedTouchOnData:(NSDictionary *)data
{
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Sehatuk"];
        [mailer setToRecipients:@[@"support.dialapp@dialy.net"]];
        
        [self.view.window.rootViewController presentViewController:mailer animated:YES completion:nil];
    }
    
}

#pragma mark - mail delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}


@end
