//
//  NewProfessionnel.h
//  espace sante
//
//  Created by abdel ali on 4/14/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "LPPopupListView.h"
#import "Validation.h"
#import "Message.h"
#import "Annotation.h"
#import "LocalizationSystem.h"

@interface NewProfessionnel : UIViewController<UITextFieldDelegate, UITextViewDelegate, LPPopupListViewDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *container;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *note;
@property (weak, nonatomic) IBOutlet UILabel *lbl_categorie;
@property (weak, nonatomic) IBOutlet UITextField *txt_categorie;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UITextField *txt_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_address;
@property (weak, nonatomic) IBOutlet UITextField *txt_address;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ville;
@property (weak, nonatomic) IBOutlet UITextField *txt_ville;
@property (weak, nonatomic) IBOutlet UILabel *lbl_activity;
@property (weak, nonatomic) IBOutlet UITextField *txt_activity;
@property (weak, nonatomic) IBOutlet UILabel *lbl_phone;
@property (weak, nonatomic) IBOutlet UITextField *txt_phone;
@property (weak, nonatomic) IBOutlet UILabel *lbl_location;
@property (weak, nonatomic) IBOutlet MKMapView *map;

@end
