//
//  Parametres.m
//  espace sante
//
//  Created by abdel ali on 9/4/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "Parametres.h"

@implementation Parametres

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setup];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - settings

- (void)setup
{
    self.titleView.text = self.title;
    self.titleView.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.titleView.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.titleView.text = SLocalizedString(@"Paramètres", nil);
}

#pragma mark - table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *title = nil;
    UITableViewCell *cell = nil;
    
    switch (indexPath.row) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"notification"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"notification"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"Notifications", nil);
            
            SwitchOnOff *switcher = (SwitchOnOff *)[cell viewWithTag:1002];
            [switcher setOn:[self notifications]];
            [switcher addTarget:self action:@selector(updateAction:) forControlEvents:UIControlEventTouchUpInside];
        }
            break;
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"langues"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"langues"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"Langues", nil);
        }
            break;
        case 2:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"optionLangue"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"optionLangue"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"Francais", nil);
        }
            break;
        case 3:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"optionLangue"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"optionLangue"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"Anglais", nil);
        }
            break;
        case 4:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"optionLangue"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"optionLangue"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"parametres.langues.arabe", nil);
        }
            break;
        case 5:
        {
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"langues"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"langues"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"Pays", nil);
        }
            break;
        case 6:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"optionLangue"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"optionLangue"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"Maroc", nil);
        }
            break;
        case 7:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"optionLangue"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"optionLangue"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"Algérie", nil);
        }
            break;
        case 8:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"optionLangue"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"optionLangue"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"Tunisie", nil);
        }
            break;
        case 9:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"optionLangue"];
            
            if(cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"optionLangue"];
            
            title = (UILabel *)[cell viewWithTag:1001];
            title.text = SLocalizedString(@"Sénégal", nil);
        }
            break;
        
    }
    
    title.font = [UIFont fontWithName:@"DroidSans" size:14];
    title.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != 1 && indexPath.row != 5)
        cell.backgroundColor = [UIColor colorWithRed:0.537 green:0.537 blue:0.537 alpha:.15];
    
    switch (indexPath.row) {
        case 2:
        {
            UIImageView *image = (UIImageView *)[cell viewWithTag:1003];
            
            if([LocalizationGetLanguage isEqualToString:@"fr"])
                image.image = [UIImage imageNamed:@"checked.png"];
            else
                image.image = nil;
        }
            break;
        case 3:
        {
            UIImageView *image = (UIImageView *)[cell viewWithTag:1003];
            
            if([LocalizationGetLanguage isEqualToString:@"en"])
                image.image = [UIImage imageNamed:@"checked.png"];
            else
                image.image = nil;
        }
            break;
        case 4:
        {
            UIImageView *image = (UIImageView *)[cell viewWithTag:1003];
            
            if([LocalizationGetLanguage isEqualToString:@"ar"])
                image.image = [UIImage imageNamed:@"checked.png"];
            else
                image.image = nil;
        }
            break;
        
        case 6:
        {
            UIImageView *image = (UIImageView *)[cell viewWithTag:1003];
            
            if([LocalizationGetCountry isEqualToString:@"ma"])
                image.image = [UIImage imageNamed:@"checked.png"];
            else
                image.image = nil;
        }
            break;
            
        case 7:
        {
            UIImageView *image = (UIImageView *)[cell viewWithTag:1003];
            
            if([LocalizationGetCountry isEqualToString:@"ALGE"])
                image.image = [UIImage imageNamed:@"checked.png"];
            else
                image.image = nil;
        }
            break;
            
        case 8:
        {
            UIImageView *image = (UIImageView *)[cell viewWithTag:1003];
            
            if([LocalizationGetCountry isEqualToString:@"TUN"])
                image.image = [UIImage imageNamed:@"checked.png"];
            else
                image.image = nil;
        }
            break;
            
        case 9:
        {
            UIImageView *image = (UIImageView *)[cell viewWithTag:1003];
            
            if([LocalizationGetCountry isEqualToString:@"SEN"])
                image.image = [UIImage imageNamed:@"checked.png"];
            else
                image.image = nil;
        }
            break;
       
                
                
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 2:
            NSLog(@"Language settings changed to French");
            LocalizationSetLanguage(@"fr");
            
            [self.tableView reloadData];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguagesDidChange" object:nil];
            break;
        case 3:
            NSLog(@"Language settings changed to English");
            LocalizationSetLanguage(@"en");
            
            [self.tableView reloadData];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguagesDidChange" object:nil];
            break;
        case 4:
            NSLog(@"Language settings changed to Arabic");
            LocalizationSetLanguage(@"ar");
            
            [self.tableView reloadData];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguagesDidChange" object:nil];
            break;
        
        case 6:
        {
            NSLog(@"Country settings changed to Morocco");
            LocalizationSetCountry(@"ma");
            [self.tableView reloadData];
        }
            break;
        case 7:
        {
            NSLog(@"Country settings changed to Algeria");
            LocalizationSetCountry(@"ALGE");
            [self.tableView reloadData];
        }
            break;
        case 8:
        {
            NSLog(@"Country settings changed to Tunisia");
            LocalizationSetCountry(@"TUN");
            [self.tableView reloadData];
        }
            break;
        case 9:
        {
            NSLog(@"Country settings changed to Senegal");
            LocalizationSetCountry(@"SEN");
            [self.tableView reloadData];
        }
            break;
        default:
            break;
    }
}

#pragma mark - action

- (IBAction)updateAction:(SwitchOnOff *)sender
{
    NSString *UID = [SSKeychain passwordForService:@"781409756" account:@"UID"];
    BOOL flag = sender.isOn;
    
    [Message load:self message:nil detail:nil view:nil delay:0];
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://ns29646.ovh.net:8001/sehatuk/"]];
    [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [client setDefaultHeader:@"Authorization" value:@"Token token=\"afbadb4ff8485c0adcba486b4ca90cc4\""];
    
    [client postPath:@"v1/devices.json" parameters:@{@"device": @{@"uid" : UID, @"flag" : @(flag)}}
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSLog(@"Notification? %d", flag);
                 [Message hide];
                 [[NSUserDefaults standardUserDefaults] setInteger:(sender.isOn) ? 1 : -1 forKey:@"notification"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 [Message hide];
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                                 message:SLocalizedString(@"Erreur de connexion", nil)
                                                                delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 [alert show];
                 
             }];
}

- (BOOL)notifications
{
    switch ([[NSUserDefaults standardUserDefaults] integerForKey:@"notifications"]) {
        case 1:{
            return YES;
        }
        case -1:{
            return NO;
        }
        default:
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"notification"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            return YES;
    }
}

@end
