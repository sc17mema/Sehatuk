//
//  Bookmarks.m
//  espace sante
//
//  Created by abdel ali on 04/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Bookmarks.h"
#import <QuartzCore/QuartzCore.h>

@interface Bookmarks ()
@property (nonatomic, strong) Item *ref;
@end

@implementation Bookmarks

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = SLocalizedString(@"Favoris", nil);
    
    [Flurry logEvent:@"Favoris" timed:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - settings

+ (void)willDisplayCell:(Item *)cell
{
    cell.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:12];
    cell.name.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    
    cell.address.font = [UIFont fontWithName:@"DroidSans" size:11];
    cell.address.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    cell.phone.font = [UIFont fontWithName:@"DroidSans" size:10];
    cell.phone.textColor = [UIColor colorWithRed:187/255.0 green:187/255.0 blue:187/255.0 alpha:1.0];
    
    cell.distance.font = [UIFont fontWithName:@"DroidSans" size:12];
    cell.distance.textColor = [UIColor colorWithRed:187/255.0 green:187/255.0 blue:187/255.0 alpha:1.0];
    
    cell.call.hidden = YES;
    cell.pin.image = [Resources loadImage:@"map_indicateur_notselected"];
}

+ (void)didSelectCell:(Item *)cell
{
    cell.name.textColor = [UIColor colorWithRed:248/255.0 green:182/255.0 blue:0/255.0 alpha:1.0];
    cell.address.textColor = [UIColor whiteColor];
    cell.phone.textColor = [UIColor whiteColor];
    
    cell.pin.image = [Resources loadImage:@"map_indicateur_selected"];
    cell.call.hidden = NO;
}

#pragma mark - pharmacies

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.datasource allKeys].count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.datasource valueForKey:[[[self.datasource allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return [[[self.datasource allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    Item *cell = (Item *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
        cell = [[Item alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    [Bookmarks willDisplayCell:cell];
    
    NSString *key = [[[self.datasource allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
    
    cell.name.text = self.datasource[key][indexPath.row][@"nom"];
    cell.address.text = self.datasource[key][indexPath.row][@"adresse"];
    cell.phone.text = self.datasource[key][indexPath.row][@"tel"];
    
    NSString *distance = self.datasource[key][indexPath.row][@"distance"];
    cell.distance.text = [NSString stringWithFormat:@"%.1f km",  [distance floatValue]];
    
    // We need to set a background to the content view of the cell
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    
    [cell setFirstStateIconName:@"cross.png"
                     firstColor:[UIColor clearColor]
            secondStateIconName:nil
                    secondColor:nil
                  thirdIconName:nil
                     thirdColor:nil
                 fourthIconName:nil
                    fourthColor:nil];

    [cell setDelegate:self];
    [cell setMode:MCSwipeTableViewCellModeExit];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Bookmarks willDisplayCell:_ref];
    self.ref = (Item *)[tableView cellForRowAtIndexPath:indexPath];
    [Bookmarks didSelectCell:_ref];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Item *c = (Item *)cell;
    
    if(indexPath.row %2 == 0)
        c.border.image = [UIImage imageNamed:@"border-even.png"];
    else
        c.border.image = [UIImage imageNamed:@"border-odd.png"];
    
    if(c.selected) [Bookmarks didSelectCell:c];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    view.backgroundColor = [UIColor colorWithRed:10.0/255.0 green:128.0/255.0 blue:155.0/255.0 alpha:1.0];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    
    label.font = [UIFont fontWithName:@"DroidSans-Bold" size:12];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    [label setText:[[[self.datasource allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]];
    
    [view addSubview:label];
    
    return view;
}

#pragma mark - click

- (IBAction)call:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    
    //if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
        //cell = (UITableViewCell *)sender.superview.superview.superview;
    
    NSIndexPath* index = [self.table indexPathForCell:cell];
    
    NSString *key = [[[self.datasource allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:index.section];
    
    NSLog(@"Call has been tapped in section: %@", key);
    
    NSString *tel = _datasource[key][index.row][@"tel"];
    tel = [tel stringByReplacingOccurrencesOfString:@" " withString:@""];
    tel = [NSString stringWithFormat:@"telprompt:%@", tel];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
}

- (IBAction)route:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    
    //if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 )
        //cell = (UITableViewCell *)sender.superview.superview.superview;
    
    NSIndexPath* index = [self.table indexPathForCell:cell];

    NSString *key = [[[self.datasource allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:index.section];
    
    NSLog(@"Route has been tapped in section: %@", key);
    
    Place *to = [[Place alloc] init];
    to.name = _datasource[key][index.row][@"nom"];
    to.description = _datasource[key][index.row][@"adresse"];
    to.latitude = [_datasource[key][index.row][@"latitude"] doubleValue];
    to.longitude = [_datasource[key][index.row][@"longitude"] doubleValue];
    
    Map * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"map"];
    vc.to = to;
    
    if(to.longitude != 0.0 && to.latitude != 0.0)
        [self addChildViewController:vc];
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                        message:SLocalizedString(@"GPS warning", nil)
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        to = nil;
        vc = nil;
    }
}

#pragma mark -
#pragma mark Swipe delegate

- (void)swipeTableViewCell:(MCSwipeTableViewCell *)cell didEndSwipingSwipingWithState:(MCSwipeTableViewCellState)state mode:(MCSwipeTableViewCellMode)mode
{
    if (mode == MCSwipeTableViewCellModeExit)
    {
        NSIndexPath* indexPath = [self.table indexPathForCell:cell];
        
        NSString *key = [[[self.datasource allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        
        [File deleteValue:self.datasource[key][indexPath.row] withKey:key ofFile:@"bookmarks"];
        self.datasource = [File getBookMarksFromFile:@"bookmarks"];
        
        [self.table beginUpdates];
        
        if([self.datasource[key] count] == 0)
            [self.table deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        else
            [self.table deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        
        [self.table endUpdates];
        
        
    }
}

@end
