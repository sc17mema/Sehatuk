//
//  Item.h
//  espace sante
//
//  Created by abdel ali on 10/31/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MCSwipeTableViewCell.h>

@interface Item : MCSwipeTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *distance;
@property (weak, nonatomic) IBOutlet UIButton *call;
@property (weak, nonatomic) IBOutlet UIImageView *pin;
@property (weak, nonatomic) IBOutlet UIImageView *border;

@end
