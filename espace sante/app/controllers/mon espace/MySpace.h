//
//  MySpace.h
//  espace sante
//
//  Created by abdel ali on 12/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "ViewPagerController.h"
#import <QuartzCore/QuartzCore.h>
#import "ViewPagerController.h"
#import "ViewPagerDataSource.h"
#import "ViewPagerDelegate.h"
#import "Message.h"
#import "Bookmarks.h"
#import "File.h"
#import "LocalizationSystem.h"

@interface MySpace : ViewPagerController<ViewPagerDataSource, ViewPagerDelegate>
{
    UIImageView *indicator;
}

@property (weak, nonatomic) IBOutlet UIView *band;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) NSArray *datasource;
@property (nonatomic) id ref;

@end
