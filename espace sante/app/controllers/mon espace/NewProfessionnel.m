//
//  NewProfessionnel.m
//  espace sante
//
//  Created by abdel ali on 4/14/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "NewProfessionnel.h"

@interface NewProfessionnel () {
    LPPopupListView *listView;
    NSArray *datasource;
    validation *validate;
    NSMutableDictionary *params;
}

@end

@implementation NewProfessionnel

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    params = [NSMutableDictionary dictionary];
    
    [self init_settings];
    [self configureListView];
    [self setupMap];
}

- (void)configureListView {
    
    datasource = @[@"Pharmacie", @"Hopital", @"Clinique", @"Medecin", @"Laboratoire", @"Centre de radiologie", @"Para-pharmacie"];
    
    float padding = 20.0f;
    
    float height = self.container.frame.size.height - self.container.frame.origin.y;
    
    CGSize size = CGSizeMake(self.container.frame.size.width - (padding * 2), height - (padding * 4));
    
    listView = [[LPPopupListView alloc] initWithTitle:@"Catégories" list:datasource
                                         selectedList:nil point:CGPointMake(padding, padding)
                                                 size:size multipleSelection:NO];
    listView.delegate = self;
    
    [Flurry logEvent:@"Nouveau professionnel de santé" timed:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - settings

- (void)init_settings
{
    self.name.text = self.title;
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    self.lbl_categorie.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_categorie.textColor = [UIColor grayColor];
    self.lbl_categorie.text = SLocalizedString(@"Categories", nil);
    self.txt_categorie.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_categorie.textColor = [UIColor blackColor];
    
    self.lbl_name.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_name.textColor = [UIColor grayColor];
    self.lbl_name.text = SLocalizedString(@"Nom", nil);
    self.txt_name.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_name.textColor = [UIColor blackColor];
    
    self.lbl_address.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_address.textColor = [UIColor grayColor];
    self.lbl_address.text = SLocalizedString(@"Adresse", nil);
    self.txt_address.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_address.textColor = [UIColor blackColor];
    
    self.lbl_ville.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_ville.textColor = [UIColor grayColor];
    self.lbl_ville.text = SLocalizedString(@"Ville", nil);
    self.txt_ville.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_ville.textColor = [UIColor blackColor];
    
    self.lbl_activity.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_activity.textColor = [UIColor grayColor];
    self.lbl_activity.text = SLocalizedString(@"Activité", nil);
    self.txt_activity.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_activity.textColor = [UIColor blackColor];
    
    self.lbl_phone.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_phone.textColor = [UIColor grayColor];
    self.lbl_phone.text = SLocalizedString(@"Telephone", nil);
    self.txt_phone.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.txt_phone.textColor = [UIColor blackColor];
    
    self.lbl_location.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.lbl_location.textColor = [UIColor grayColor];
    self.lbl_location.text = SLocalizedString(@"Position", nil);
    
    self.note.font = [UIFont fontWithName:@"DroidSans-Bold" size:10];
    self.note.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.note.text =  SLocalizedString(@"Appuyez longuement sur l'indicateur pour le positionner", nil);
    
    [self.container contentSizeToFit];
    self.container.contentSize = CGSizeMake(320, 644);
}

#pragma mark -
#pragma mark - user location

- (void)setupMap {
    
    CLLocationCoordinate2D mapRegion;
    mapRegion.latitude = 33.5827432;
    mapRegion.longitude = -7.6190503;
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = mapRegion;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(mapRegion, 300*METERS_PER_MILE, 300*METERS_PER_MILE);
    
    [self.map addAnnotation:annotation];
    [self.map setRegion:viewRegion animated: YES];
    
    UILongPressGestureRecognizer *gr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    gr.minimumPressDuration = .5;
    gr.delegate = self;
    [self.map addGestureRecognizer:gr];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    DraggableAnnotationView *pinView = nil;
    
    if(annotation != mapView.userLocation)
    {
        static NSString *defaultPin = @"pin";
        
        pinView = (DraggableAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPin];
        
        if ( pinView == nil )
        {
            pinView = [[DraggableAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPin];
            pinView.draggable = YES;
            pinView.mapView = self.map;
        }
    }
    
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        
        [params setValue:@(droppedAt.latitude) forKey:@"latitude"];
        [params setValue:@(droppedAt.longitude) forKey:@"longitude"];
    }
}

- (void)longPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    NSLog(@"Location set on the map");
    CGPoint touchPoint = [gestureRecognizer locationInView:_map];
    
    CLLocationCoordinate2D touchMapCoordinate = [self.map convertPoint:touchPoint toCoordinateFromView:_map];
    [(MKPointAnnotation *)_map.annotations[0] setCoordinate:touchMapCoordinate];
}


#pragma mark -
#pragma mark textfield delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:self.txt_categorie]) {
        [listView showInView:self.view animated:YES];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

#pragma mark -
#pragma mark popup list view

- (void)popupListView:(LPPopupListView *)popUpListView didSelectedIndex:(NSInteger)index
{
    self.txt_categorie.text = datasource[index];
}

- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedList:(NSArray *)list
{
}

#pragma mark -
#pragma mark action

- (IBAction)sendMail:(id)sender {
    validate = [[validation alloc] init];
    [validate Required:self.txt_categorie FieldName:@"Categorie"];
    [validate Required:self.txt_name FieldName:@"Nom"];
    [validate Required:self.txt_address FieldName:@"Adresse"];
    [validate Required:self.txt_ville FieldName:@"Ville"];
    [validate Required:self.txt_activity FieldName:@"Activité"];
    [validate Phone:self.txt_phone FieldName:@"Telephone"];
    [validate isValid];
    
    if(![validate textFiledIsValid])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                        message:SLocalizedString(@"Champs Obligatoires", nil)
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

        
        return;
    }
    
    NSLog(@"New entry submitted under category: %@", self.txt_categorie.text);
    
    NSMutableArray *tmp = [NSMutableArray array];
    
    [params setValue:self.txt_categorie.text forKey:@"Categorie"];
    [params setValue:self.txt_name.text forKey:@"Nom"];
    [params setValue:self.txt_address.text forKey:@"Adresse"];
    [params setValue:self.txt_ville.text forKey:@"Ville"];
    [params setValue:self.txt_activity.text forKey:@"Activité"];
    [params setValue:self.txt_phone.text forKey:@"Telephone"];
    [params setValue:@"iOS" forKey:@"Plateforme"];
    
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [tmp addObject:[NSString stringWithFormat:@"%@ : %@", key, obj]];
    }];
    
    [Message load:self message:nil detail:nil view:nil delay:0];
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://ns29646.ovh.net:8080/"]];
    [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [client getPath:@"InfoServicesSeha/SendMailSeha" parameters:@{@"txt" : [tmp componentsJoinedByString:@"\n"], @"obj" : @"Demande d'ajout", @"from": @"sehatuk@iphone.com"}
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [Message success:self message:nil delay:1];
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [Message failed:self message:nil delay:1];
            }];
    
}

@end
