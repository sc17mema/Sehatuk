//
//  Splash.h
//
//  Created by abdel ali on 31/12/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "Splash.h"
#import <Flurry.h>

@implementation Splash

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self initProgressBar];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.view removeFromSuperview];
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [allViewControllers removeObjectIdenticalTo:self];
    self.navigationController.viewControllers = allViewControllers;
    
    [super viewDidDisappear:NO];
}

#pragma mark - progress view

- (void)initProgressBar
{
    [self.progressBar setBackColor:[UIColor whiteColor]];
    [self.progressBar setFrontColor:[UIColor colorWithRed:246.0/255.0f green:184.0/255.0f blue:0/255.0f alpha:1.0]];
    [self.progressBar setMaskingImage:[UIImage imageNamed:@"sehatuk_smile.png"]];
    [self.progressBar setAnimationTime:4.0f];
    [self.progressBar setHorizontal:YES];
    [self.progressBar setProgress:1.0 animated:YES];
    
    self.progressBar.didFinishAnimation = ^(){
        [self performSegueWithIdentifier:@"home" sender:nil];
    };
}

@end
