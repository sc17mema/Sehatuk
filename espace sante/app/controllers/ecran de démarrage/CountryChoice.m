//
//  CountryChoice.m
//  espace sante
//
//  Created by chbab Mohamed on 29/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "CountryChoice.h"

@interface CountryChoice ()

@end

@implementation CountryChoice

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initSettings
{
    NSArray *countries = @[self.maButton,self.algButton,self.senButton,self.tunButton];
    CGFloat angle = 360/countries.count;
    CGPoint centreImage = self.imageCountries.center;
    float radius = CGRectGetWidth(self.imageCountries.frame) / 2 - 3;
    float startPosition = -135;
    
    int currentItem = 1;
    
    while (currentItem <= countries.count) {
        
        float radianAngle = (angle * (currentItem -1) + startPosition) * (M_PI/180);
        
        float x = round(centreImage.x + radius * cos(radianAngle));
        float y = round(centreImage.y + radius * sin(radianAngle));
        
        UIButton *currentButton = countries[currentItem - 1];
        currentButton.center = CGPointMake(x, y);
        
        currentItem ++;
    }
    
    
    
    self.chooseCountryLabel.font = [UIFont fontWithName:@"DroidSans" size:20];
    self.chooseCountryLabel.text= SLocalizedString(@"Séléctionnez votre pays", nil);
    self.passerButton.titleLabel.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    [self.passerButton setTitle:SLocalizedString(@"Passer", nil) forState:UIControlStateNormal];
    
}

- (IBAction)accessCountry :(id)sender{
    
    UIButton *senderButton = (UIButton*) sender;
    
    NSDictionary *maDict = @{@"country" : @"Morocco"};
    NSDictionary *algeDict = @{@"country" : @"Algeria"};
    NSDictionary *senDict = @{@"country" : @"Senegal"};
    NSDictionary *tunDict = @{@"country" : @"Tunisia"};
    
    switch (senderButton.tag) {
        case 10:
            [Flurry logEvent:@"Country" withParameters:maDict];
            LocalizationSetCountry(@"ma");
            break;
        case 20:
            [Flurry logEvent:@"Country" withParameters:algeDict];
            LocalizationSetCountry(@"ALGE");
            break;
            
        case 30:
            [Flurry logEvent:@"Country" withParameters:senDict];
            LocalizationSetCountry(@"SEN");
            break;
            
        case 40:
            [Flurry logEvent:@"Country" withParameters:tunDict];
            LocalizationSetCountry(@"TUN");
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)passerAction:(id)sender {
    NSDictionary *defaultDict = @{@"country" : @"Default"};
    [Flurry logEvent:@"Country" withParameters:defaultDict];
    LocalizationSetCountry(@"ma");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
