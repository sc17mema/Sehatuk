//
//  CountryChoice.h
//  espace sante
//
//  Created by chbab Mohamed on 29/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalizationSystem.h"

@interface CountryChoice : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageCountries;
@property (strong, nonatomic) IBOutlet UIButton *maButton;
@property (strong, nonatomic) IBOutlet UILabel *chooseCountryLabel;
@property (strong, nonatomic) IBOutlet UIButton *passerButton;

@property (strong, nonatomic) IBOutlet UIButton *algButton;
@property (strong, nonatomic) IBOutlet UIButton *senButton;
@property (strong, nonatomic) IBOutlet UIButton *tunButton;


- (IBAction)accessCountry :(id)sender;
- (IBAction)passerAction:(id)sender;

@end
