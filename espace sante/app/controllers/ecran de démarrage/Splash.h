#import <UIKit/UIKit.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import "WS.h"
#import "Cache.h"
#import "ProgressView.h"

@interface Splash : UIViewController
{
    NSArray *filesToDownload;
}

@property (weak, nonatomic) IBOutlet ProgressView *progressBar;

@end
