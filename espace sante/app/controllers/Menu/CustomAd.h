#import <UIKit/UIKit.h>
#import "Cache.h"
#import "File.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "UIButton+AFNetworking.h"

@protocol CustomADDelegate <NSObject>
@optional
- (void)customAd:(id)controller userDidClickWithAdID:(NSString *)adID;
@required
- (void)customAdLoaded;
@end

@interface CustomAd : UIView
{
    BOOL large_banner;
    int  current_banner;
    
    UIButton *buttonAd;
    UIButton *closeButton;
    
    NSArray *images;
    NSArray *links;
    NSTimer *timer;
}

@property (nonatomic, retain) id <CustomADDelegate> delegate;
@property (assign) int refreshTime;

- (void) initBanner;
- (void) start;
- (void) stop;

@end


@interface UIImageView (AFNetworkingFadeInAdditions)

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage fadeInWithDuration:(CGFloat)duration;

@end

@implementation UIImageView (AFNetworkingFadeInAdditions)

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage fadeInWithDuration:(CGFloat)duration {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPShouldHandleCookies:NO];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    __weak typeof (self) weakSelf = self;
    
    [self setImageWithURLRequest:request placeholderImage:placeholderImage success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        if (!request) // image was cached
            [weakSelf setImage:image];
        else
            [UIView transitionWithView:weakSelf duration:duration options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [weakSelf setImage:image];
            } completion:nil];
    } failure:nil];
}

@end
