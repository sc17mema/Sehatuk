#import "Menu.h"

@implementation Menu

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self.view setNeedsDisplay];
    
    self.navigationItem.backBarButtonItem = nil;
    [self defaultSettings];
    [self iads];
    [self selectCountry];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"LanguagesDidChange" object:nil queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      [self defaultSettings];
                                                  }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - style

- (void)defaultSettings
{
    self.centers.titleLabel.font = [UIFont fontWithName:@"Droid Sans" size:13];
    [self.centers setTitle:SLocalizedString(@"Centres de santé", nil) forState:UIControlStateNormal];
    self.drugs.titleLabel.font = [UIFont fontWithName:@"Droid Sans" size:13];
    [self.drugs setTitle:SLocalizedString(@"Medicaments", nil) forState:UIControlStateNormal];
    self.my_space.titleLabel.font = [UIFont fontWithName:@"Droid Sans" size:13];
    [self.my_space setTitle:SLocalizedString(@"Mon espace", nil) forState:UIControlStateNormal];
    self.news.titleLabel.font = [UIFont fontWithName:@"Droid Sans" size:13];
    [self.news setTitle:SLocalizedString(@"Actu-sante", nil) forState:UIControlStateNormal];
}

#pragma mark - iads

- (void)iads
{
    if([Cache get:@"iads"] == nil)
        [self performFetch];
    else
    {
        iad = [[CustomAd alloc] initWithFrame:self.view.frame];
        iad.delegate = self;
    }

}

- (void)selectCountry
{
    if(LocalizationGetCountry == nil) {
        UIViewController *ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"countries"];
        ctrl.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:ctrl animated:YES completion:nil];
    }
}

- (void)customAdLoaded
{
    [self.view addSubview:iad];
}
         
- (void)performFetch
{
    NSString *path = [NSString stringWithFormat:@"sehatuk/v1/iads"];
    
    NSMutableURLRequest *request = [[WS api] requestWithMethod:@"get" path:path parameters:nil];
    AFJSONRequestOperation *operation = [[AFJSONRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if([Cache get:@"iads"] == nil)
             [Cache setObject:responseObject forKey:@"iads" withExpires:604800];
         
     } failure:nil];
    
    [operation start];
}

#pragma mark - status bar

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
