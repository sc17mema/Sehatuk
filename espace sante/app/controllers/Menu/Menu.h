#import <UIKit/UIKit.h>
#import "CustomAd.h"
#import "Cache.h"
#import <AFNetworking/AFJSONRequestOperation.h>
#import "WS.h"
#import "LocalizationSystem.h"

@interface Menu : UIViewController<CustomADDelegate>
{
    CustomAd *iad;
}
@property (weak, nonatomic) IBOutlet UIButton *centers;
@property (weak, nonatomic) IBOutlet UIButton *drugs;
@property (weak, nonatomic) IBOutlet UIButton *my_space;
@property (weak, nonatomic) IBOutlet UIButton *news;

@end
