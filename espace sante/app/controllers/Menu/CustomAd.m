//
//  CustomAd.m
//
//  Created by abdel ali on 8/23/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "CustomAd.h"

#define IS_IPHONE_5 (((double)[[UIScreen mainScreen] bounds].size.height) == ((double)568))
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

@implementation CustomAd

#pragma mark -
#pragma mark UIButton Class Methods

- (id)initWithFrame:(CGRect)frame
{
    if (IS_IPHONE_5)
        large_banner = YES;
    else
        large_banner = NO;
    
//    if (IS_OS_7_OR_LATER)
//    {
//        frame.origin.y += 10;
//        frame.size.height -= 20;
//    }
    
    self = [super initWithFrame:frame];
    
    if (self)
    {
        buttonAd = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonAd setBackgroundColor:[UIColor clearColor]];
        [buttonAd setTintColor:[UIColor clearColor]];
        [buttonAd setFrame:frame];
        [buttonAd setTitle:@"" forState:UIControlStateNormal];
        [self addSubview:buttonAd];
        
        _refreshTime = 8;
        links  = [[NSArray alloc] init];
        images = [[NSArray alloc] init];
        
        [self initBanner];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    
    return self;
}

#pragma mark -
#pragma mark Custom Init Methods

- (void)initBanner
{
    [self start];
    [self setRandomBanner];
}

#pragma mark -
#pragma mark Custom Methods

- (void)fillBannerWithData:(NSArray *)data
{
    NSMutableArray *links_temp = [[NSMutableArray alloc] init];
    NSMutableArray *images_temp = [[NSMutableArray alloc] init];
    
    for (NSDictionary * information in data)
    {
        [links_temp addObject:[information objectForKey:@"apple_store"]];
        [images_temp addObject:[information objectForKey:@"image"]];
    }
    
    links       = links_temp;
    images      = images_temp;
    links_temp  = nil;
    images_temp = nil;
}

- (void)setRandomBanner
{
    if (images != nil && [images count] > 0)
    {
        int random = arc4random() % [images count];
        current_banner = random;
        NSString *link = [@"http://ns29646.ovh.net:8001/sehatuk/" stringByAppendingString:[images objectAtIndex:random]];
        
        __weak CustomAd *_self = self;
        
        [buttonAd setImageWithURL:[NSURL URLWithString:link]
                 placeholderImage:nil forState:UIControlStateNormal
                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                              [_self.delegate customAdLoaded];
                              [_self performSelector:@selector(setCloseButton) withObject:nil afterDelay:1];
                          }
                          failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                              [_self closeBanner];
                          }];
    }
}

- (void)setCloseButton
{
    closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateHighlighted];
    [closeButton addTarget:self action:@selector(closeBanner) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setTintColor:[UIColor clearColor]];
    [closeButton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [closeButton setFrame:CGRectMake(self.frame.size.width - 39, self.frame.origin.y, 39, 39)];
    [self addSubview:closeButton];
}

#pragma mark -
#pragma mark UIButton and UITimer Handler Methods

- (void)closeBanner
{
    [self stop];
    
    [UIView animateWithDuration:0.2
                     animations:^{self.alpha = 0.0;}
                     completion:^(BOOL finished){ [self removeFromSuperview];
                     }];
}

- (void)userPushed:(id)sender
{
//    [[self delegate] customAd:self userDidClickWithAdID:[links objectAtIndex:current_banner]];
    NSString* launchUrl = [links objectAtIndex:current_banner];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: launchUrl]];
}

#pragma mark -
#pragma mark Activity Methods

- (void)start
{
    [buttonAd addTarget:self action:@selector(userPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self fillBannerWithData:[File getDataFromFile:[Cache getPath:@"iads"]]];
    
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:_refreshTime target:self selector:@selector(closeBanner) userInfo:nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)stop
{
    [timer invalidate];
    timer  = nil;
    links   = nil;
    images  = nil;
}

@end
