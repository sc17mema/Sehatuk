//
//  Prescription.m
//  espace sante
//
//  Created by abdel ali on 1/8/15.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "Prescription.h"

@implementation Prescription{
    AutoCompleteListView *dropDownListView;
    NSArray *drugsList;
    NSMutableArray *suggestions;
    OrdonnanceModel *ordonnance;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
    [self initUI];
    
    suggestions = [NSMutableArray array];
    drugsList = [[File getDataFromFile:[Cache getPath:@"anam.json"]] valueForKey:@"nom_commercial"];
    drugsList = [NSSet setWithArray:drugsList].allObjects;
    
    if(!ordonnance)
        ordonnance = [[OrdonnanceModel alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    lblTotalAmount.text = [NSString stringWithFormat:@"%.1f", ordonnance.prixTotal];
}

#pragma mark - general settings

- (void)initView
{
    lblCtrlName.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    lblCtrlName.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    lblCtrlName.text = SLocalizedString(@"Ordonnances", nil);
    
    lblTotalAmountLabel.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    lblTotalAmountLabel.textColor = [UIColor lightGrayColor];
    lblTotalAmountLabel.text = [SLocalizedString(@"Montant Total", nil) uppercaseString];
    
    lblTotalAmount.font = [UIFont fontWithName:@"DroidSans-Bold" size:16];
    lblTotalAmount.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    lblTotalAmount.text = @"0.0 DH";
    
    txtDrugName.delegate = self;
}

- (void)initUI
{
    dropDownListView = [[AutoCompleteListView alloc] initWithBoundView:txtDrugName dataSource:self delegate:self];
    [txtDrugName addTarget:self action:@selector(textDidChanged:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - autoCompleteListViewDataSource & autoCompleteListViewDelegate

- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    return suggestions.count;
}

- (CGFloat)itemCellHeight:(NSIndexPath *)indexPath
{
    return 44.0f;
}

- (void)clickedListViewAtIndexPath:(NSIndexPath *)indexPath
{
    [txtDrugName setText:suggestions[indexPath.row]];
}

- (UITableViewCell *)itemCell:(NSIndexPath *)indexPath
{
    if (suggestions.count == 0) {
        return nil;
    }
    
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.textLabel.text = suggestions[indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"DroidSans" size:12];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.numberOfLines = 0;
    
    return cell;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textDidChanged:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    
    if (![self isNullOrEmpty:textField.text] && drugsList.count > 0) {
        
        [self createListData:textField.text];
        
        [dropDownListView show];
        [dropDownListView reloadListData];
    }
    
}

#pragma mark - Utils

- (BOOL)isNullOrEmpty:(NSString *)s
{
    if(s == nil || s.length == 0 || [s isEqualToString:@""] || [s isEqualToString:@"(null)"]) {
        return YES;
    }
    
    return NO;
}

- (void)createListData:(NSString*)str
{
    [suggestions removeAllObjects];
    
    if(str.length > 0)
    {
        for(int i=0; i<drugsList.count; i++)
        {
            if([drugsList[i] rangeOfString:str options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [suggestions addObject:drugsList[i]];
            }
        }
    }
}

#pragma mark - tableview delegate & datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ordonnance.medicaments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    MCSwipeTableViewCell *cell = (MCSwipeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[MCSwipeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    MedicamentModel *medicament = ordonnance.medicaments[indexPath.row];
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:7001];
    lblName.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    lblName.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    lblName.text = medicament.nom;
    
    UILabel *lblDosageLabel = (UILabel *)[cell viewWithTag:7002];
    lblDosageLabel.font = [UIFont fontWithName:@"DroidSans" size:12];
    lblDosageLabel.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    UILabel *lblDosage = (UILabel *)[cell viewWithTag:7003];
    lblDosage.font = [UIFont fontWithName:@"DroidSans" size:12];
    lblDosage.textColor = [UIColor lightGrayColor];
    lblDosage.text = medicament.dosage;
    
    UILabel *lblPrice = (UILabel *)[cell viewWithTag:7005];
    lblPrice.font = [UIFont fontWithName:@"DroidSans-Bold" size:16];
    lblPrice.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    lblPrice.text = [NSString stringWithFormat:@"%.1f DH", [medicament.prix floatValue]];
    
    UILabel *lblQuantity = (UILabel *)[cell viewWithTag:7006];
    lblQuantity.font = [UIFont fontWithName:@"DroidSans" size:12];
    lblQuantity.textColor = [UIColor grayColor];
    lblQuantity.text = [NSString stringWithFormat:@"x %d", [medicament.qte integerValue]];
    
    UILabel *lblPresentation = (UILabel *)[cell viewWithTag:7004];
    lblPresentation.font = [UIFont fontWithName:@"DroidSans" size:12];
    lblPresentation.textColor = [UIColor lightGrayColor];
    lblPresentation.text = medicament.presentation;
    
    [cell setDelegate:self];
    [cell setMode:MCSwipeTableViewCellModeExit];
    [cell setFirstStateIconName:@"cross.png"
                     firstColor:[UIColor clearColor]
            secondStateIconName:nil
                    secondColor:nil
                  thirdIconName:nil
                     thirdColor:nil
                 fourthIconName:nil
                    fourthColor:nil];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.textLabel.font = [UIFont fontWithName:@"DroidSans-Bold" size:12];
    cell.textLabel.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}


- (void)willMoveToParentViewController:(UIViewController *)parent
{
    // Add the view to the parent view and position it if you want
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    CGRect rect = CGRectMake(0, 0, parent.view.frame.size.width, parent.view.frame.size.height);
    self.view.frame = rect;
    
    [[parent view] addSubview:[self view]];
}

- (void)preSetOrdonnance:(OrdonnanceModel *)_ordonnance
{
    ordonnance = _ordonnance;
    [tblDrugs reloadData];
}

#pragma mark - search

- (IBAction)performSearch
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nom_commercial LIKE[cd] %@", txtDrugName.text];
    
    AResult * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"aresult"];
    NSArray *list = [[File getDataFromFile:[Cache getPath:@"anam.json"]] filteredArrayUsingPredicate:predicate];
        
    if([list count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                        message:SLocalizedString(@"Aucun resultat", nil)
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    NSLog(@"Searched medicine: %@", txtDrugName.text);
    vc.anam = list;
    vc.delegate = self;
    
    [txtDrugName resignFirstResponder];
    [self addChildViewController:vc];
}

- (IBAction)saveOrdonnanceAction:(id)sender
{
    NSLog(@"Saved prescription");
    OrdonnancesManager *manager = [OrdonnancesManager sharedSingleton];
    
    if(!_isEditing)
        [manager addOrdonnance:ordonnance];
    else
        [manager saveOrdonnances];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    transition.delegate = self;
    
    [self.view.superview.layer addAnimation:transition forKey:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOAD_ORDONNANCE_TABLE" object:nil];
}

#pragma mark - ordonnance delegate

- (void)addMedicamentObjToOrdonnance:(MedicamentModel *)obj
{
    [ordonnance addMedicaments:obj];
    [tblDrugs reloadData];
    lblTotalAmount.text = [NSString stringWithFormat:@"%.1f", ordonnance.prixTotal];
    
    UIViewController *child = (UIViewController *)[self.childViewControllers firstObject];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    transition.delegate = self;
    
    [child.view.superview.layer addAnimation:transition forKey:nil];
    [child.view removeFromSuperview];
    [child removeFromParentViewController];
}



#pragma mark -
#pragma mark Swipe delegate

- (void)swipeTableViewCell:(MCSwipeTableViewCell *)cell didEndSwipingSwipingWithState:(MCSwipeTableViewCellState)state mode:(MCSwipeTableViewCellMode)mode
{

    NSIndexPath* indexPath = [tblDrugs indexPathForCell:cell];
    [ordonnance.medicaments removeObjectAtIndex:indexPath.row];
    
    OrdonnancesManager *manager = [OrdonnancesManager sharedSingleton];
    [manager saveOrdonnances];
    
    lblTotalAmount.text = [NSString stringWithFormat:@"%.1f", ordonnance.prixTotal];
    [tblDrugs reloadData];
    NSLog(@"DELETE ****");
}


@end
