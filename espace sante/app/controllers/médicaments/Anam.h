//
//  Anam.h
//  espace sante
//
//  Created by abdel ali on 11/21/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwitchOnOff.h"
#import "ACEAutocompleteBar.h"
#import "Cache.h"
#import "File.h"
#import "Drugs.h"
#import "WS.h"
#import "Cache.h"
#import "AResult.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.1;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface Anam : UIViewController<ACEAutocompleteDataSource, ACEAutocompleteDelegate, ContainerDelegate>
{
    NSString *url;
    CGFloat animatedDistance;
    float tmp;
}

@property (weak, nonatomic) IBOutlet UIScrollView *container;
@property (weak, nonatomic) IBOutlet SwitchOnOff *turn_on_gps;
@property (weak, nonatomic) IBOutlet UITextField *trade_name;
@property (weak, nonatomic) IBOutlet UITextField *designation;
@property (weak, nonatomic) IBOutlet UITextField *category;
@property (weak, nonatomic) IBOutlet UITextField *dosage;
@property (weak, nonatomic) IBOutlet UIButton *find_by_repayable;
@property (weak, nonatomic) IBOutlet UIButton *find_all;
@property (weak, nonatomic) IBOutlet UIButton *search;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *best_price;
@property (weak, nonatomic) IBOutlet UILabel *repayable;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (strong, nonatomic) NSMutableArray *datasource;
@property (strong, nonatomic) UIViewController *ref;
@property (assign, nonatomic) BOOL lock;

- (IBAction)mode:(id)sender;

@end
