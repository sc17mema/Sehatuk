#import <UIKit/UIKit.h>

// Localization
#import "LocalizationSystem.h"

// Autocomplete UI
#import "AutoCompleteListView.h"
#import "AutoCompleteTextField.h"

// Message UI
#import "Message.h"

// Utils
#import "File.h"
#import "Cache.h"

#import "AResult.h"

#import "OrdonnanceModel.h"
#import "OrdonnancesManager.h"
#import "MedicamentModel.h"
#import <MCSwipeTableViewCell.h>

@interface Prescription : UIViewController<UITextFieldDelegate, PopupListViewDataSource, PopupListViewDelegate, ordonnanceDelegate,MCSwipeTableViewCellDelegate>
{
    __weak IBOutlet AutoCompleteTextField *txtDrugName;
    __weak IBOutlet UITableView *tblDrugs;
    __weak IBOutlet UILabel *lblTotalAmount;
    __weak IBOutlet UILabel *lblTotalAmountLabel;
    __weak IBOutlet UILabel *lblCtrlName;
}

@property (nonatomic) BOOL isEditing;

- (void)preSetOrdonnance:(OrdonnanceModel *)ordonnance;

@end
