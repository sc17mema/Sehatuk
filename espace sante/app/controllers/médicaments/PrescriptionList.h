#import <UIKit/UIKit.h>

// Localization
#import "LocalizationSystem.h"
#import "OrdonnancesManager.h"
#import "Prescription.h"

#import <MCSwipeTableViewCell.h>

@interface PrescriptionList : UIViewController <UITableViewDataSource, UITableViewDelegate,MCSwipeTableViewCellDelegate>
{
    __weak IBOutlet UITableView *tblPresciptions;
    __weak IBOutlet UIButton *btnNewPresciption;
    __weak IBOutlet UILabel *lblCtrlName;
}

@end
