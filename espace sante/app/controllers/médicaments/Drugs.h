//
//  Drugs.h
//  espace sante
//
//  Created by abdel ali on 07/10/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ViewPagerController.h"
#import "ViewPagerDataSource.h"
#import "ViewPagerDelegate.h"
#import "Message.h"
#import "Cache.h"
#import "File.h"
#import "DownloadEngine.h"
#import "LocalizationSystem.h"

@protocol ContainerDelegate <NSObject>
@optional
- (void)didMoveToParentController:(ViewPagerController *)UIViewController;
- (BOOL)didAddViewToParentController:(ViewPagerController *)UIViewController;
@end

@interface Drugs : ViewPagerController<ViewPagerDataSource, ViewPagerDelegate>
{
    UIImageView *indicator;
    MBProgressHUD *progress;
}

@property (nonatomic) id ref;
@property (weak, nonatomic) IBOutlet UIView *band;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) NSArray *datasource;

@end

