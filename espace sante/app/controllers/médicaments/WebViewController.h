//
//  WebViewController.h
//  sample
//
//  Created by abdel ali on 4/8/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIWebView+RemoveShadow.h"

@interface WebViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (nonatomic, strong) NSString *html;

@end
