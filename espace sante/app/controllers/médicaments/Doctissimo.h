//
//  Doctissimo.h
//  espace sante
//
//  Created by abdel ali on 11/21/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACEAutocompleteBar.h"
#import "Cache.h"
#import "File.h"
#import "Drugs.h"
#import "WS.h"
#import "DResult.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.1;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface Doctissimo : UIViewController<ACEAutocompleteDataSource, ACEAutocompleteDelegate, ContainerDelegate>
{
    int identifier;
    NSString *url;
    CGFloat animatedDistance;
}

@property (weak, nonatomic) IBOutlet UITextField *designation;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) NSMutableArray *datasource;
@property (strong, nonatomic) UIViewController *ref;
@property (assign, nonatomic) BOOL lock;

@end
