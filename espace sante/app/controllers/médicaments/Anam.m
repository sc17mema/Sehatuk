//
//  Anam.m
//  espace sante
//
//  Created by abdel ali on 11/21/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Anam.h"

@implementation Anam

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self init_settings];
    [self setupAutoComplete];
    self.view.autoresizesSubviews = YES;
    [self.dosage setHidden:YES];
    
    [Flurry logEvent:@"Anam" timed:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - best price

- (IBAction)bestprice:(id)sender
{
    BOOL isOn = [sender isOn];
    
    if(isOn)
    {
        [self.category setHidden:YES];
        [self.background setHidden:YES];
        [self.designation setHidden:YES];
        [self.dosage setHidden:NO];
        
        tmp = self.repayable.center.y - self.designation.center.y;
        
        [UIView animateWithDuration:0.5 animations:^{
            self.repayable.center = CGPointMake(_repayable.center.x, _repayable.center.y - tmp);
            self.find_all.center = CGPointMake(_find_all.center.x, _find_all.center.y - tmp);
            self.find_by_repayable.center = CGPointMake(_find_by_repayable.center.x, _find_by_repayable.center.y - tmp);
            self.search.center = CGPointMake(_search.center.x, _search.center.y - tmp);
        }];
        
        self.container.contentSize = CGSizeMake(320, 410 - tmp);
    }
    else
    {
        [self.category setHidden:NO];
        [self.background setHidden:NO];
        [self.designation setHidden:NO];
        [self.dosage setHidden:YES];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.repayable.center = CGPointMake(_repayable.center.x, _repayable.center.y + tmp);
            self.find_all.center = CGPointMake(_find_all.center.x, _find_all.center.y + tmp);
            self.find_by_repayable.center = CGPointMake(_find_by_repayable.center.x, _find_by_repayable.center.y + tmp);
            self.search.center = CGPointMake(_search.center.x, _search.center.y + tmp);
        }];
        
        self.container.contentSize = CGSizeMake(320, self.container.contentSize.height + tmp);
    }
}

#pragma mark - settings

- (void)init_settings
{
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = SLocalizedString(@"Recherche avancee", nil);
    
    self.designation.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.designation.textColor = [UIColor blackColor];
    self.designation.placeholder = SLocalizedString(@"Denomination", nil);
    
    self.category.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.category.textColor = [UIColor blackColor];
    self.category.placeholder = SLocalizedString(@"Classe", nil);
    
    self.trade_name.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.trade_name.textColor = [UIColor blackColor];
    self.trade_name.placeholder = SLocalizedString(@"Nom medicament", nil);
    
    self.find_by_repayable.titleLabel.font = [UIFont fontWithName:@"DroidSans" size:12];
    [self.find_by_repayable setTitle:SLocalizedString(@"Oui", nil) forState:UIControlStateNormal];
    self.find_all.titleLabel.font = [UIFont fontWithName:@"DroidSans" size:12];
    [self.find_all setTitle:SLocalizedString(@"Tous", nil) forState:UIControlStateNormal];
    
    self.find_all.selected = YES;
    
    self.best_price.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    self.best_price.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.best_price.text = SLocalizedString(@"Best price", nil);
    
    self.repayable.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    self.repayable.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    self.repayable.text = SLocalizedString(@"Remboursable", nil);
    
    self.container.contentSize = CGSizeMake(320, 410);
}

#pragma mark -

- (IBAction)mode:(id)sender
{
    self.find_by_repayable.selected = !_find_by_repayable.selected;
    self.find_all.selected = !_find_all.selected;
    
    NSLog(@"Repayable: %d", self.find_by_repayable.selected);
    NSLog(@"All: %d", self.find_all.selected);
}

#pragma mark - autocomplete datasource

- (void)setupAutoComplete
{
    [self.trade_name setAutocompleteWithDataSource:self delegate:self customize:^(ACEAutocompleteInputView *inputView) {
        inputView.font = [UIFont fontWithName:@"DroidSans" size:14];
        inputView.textColor = [UIColor whiteColor];
        inputView.backgroundColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    }];
    
    [self.category setAutocompleteWithDataSource:self delegate:self customize:^(ACEAutocompleteInputView *inputView) {
        inputView.font = [UIFont fontWithName:@"DroidSans" size:14];
        inputView.textColor = [UIColor whiteColor];
        inputView.backgroundColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    }];
    
    [self.designation setAutocompleteWithDataSource:self delegate:self customize:^(ACEAutocompleteInputView *inputView) {
        inputView.font = [UIFont fontWithName:@"DroidSans" size:14];
        inputView.textColor = [UIColor whiteColor];
        inputView.backgroundColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    }];
    
    [self.dosage setAutocompleteWithDataSource:self delegate:self customize:^(ACEAutocompleteInputView *inputView) {
        inputView.font = [UIFont fontWithName:@"DroidSans" size:14];
        inputView.textColor = [UIColor whiteColor];
        inputView.backgroundColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    }];
}

- (void)textField:(UITextField *)textField didSelectObject:(id)object inInputView:(ACEAutocompleteInputView *)inputView
{
    textField.text = object;
    [textField resignFirstResponder];
}

- (NSUInteger)minimumCharactersToTrigger:(ACEAutocompleteInputView *)inputView
{
    return 1;
}

- (void)inputView:(ACEAutocompleteInputView *)inputView itemsFor:(NSString *)query result:(void (^)(NSArray *items))resultBlock;
{
    if (resultBlock != nil) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            NSMutableArray *data = [NSMutableArray array];
            
            
            NSArray *src = nil;
            
            if(inputView.textField == self.trade_name)
                src = [[NSSet setWithArray:[self.datasource valueForKey:@"nom_commercial"]] allObjects];
            else if(inputView.textField == self.designation)
                src = [[NSSet setWithArray:[self.datasource valueForKey:@"denomination"]] allObjects];
            else if(inputView.textField == self.category)
                src = [[NSSet setWithArray:[self.datasource valueForKey:@"classe_therapeutique"]] allObjects];
            else
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nom_commercial CONTAINS[cd] %@", self.trade_name.text];
                src = [self.datasource filteredArrayUsingPredicate:predicate];
                
                src = [[NSSet setWithArray:[src valueForKey:@"dosage"]] allObjects];
            }
            
            for (NSString *s in src) {
                
                NSRange range = [s rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
                
                if ([[s stringByReplacingCharactersInRange:range withString:@""] hasPrefix:[query uppercaseString]]) {
                    [data addObject:s];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                resultBlock(data);
            });
        });
    }
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(self.datasource == nil)
        self.datasource = [File getDataFromFile:[Cache getPath:@"anam.json"]];
    
    // Get the rects of the text field being edited and the view that we're going to scroll.
    // We convert everything to window coordinates
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    // So now we have the bounds we need to calculate the fraction between the top and bottom of the middle section for the text field's midline
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}


#pragma mark - search

- (IBAction)performSearch:(id)sender
{
            
    NSMutableArray *subpredicates = [[NSMutableArray alloc] init];
    NSPredicate *subpredicate = nil;
    
    if([self.trade_name.text length] > 0)
    {
        subpredicate = [NSPredicate predicateWithFormat:@"nom_commercial LIKE[cd] %@", self.trade_name.text];
        [subpredicates addObject:subpredicate];
    }
    if(self.turn_on_gps.isOn && subpredicate != nil)
    {
        NSArray *t = [self.datasource filteredArrayUsingPredicate:subpredicate];
        
        [subpredicates removeAllObjects];
        subpredicate = [NSPredicate predicateWithFormat:@"denomination LIKE[cd] %@", t[0][@"denomination"]];
        
        [subpredicates addObject:subpredicate];
    }
    if([self.dosage.text length] > 0)
    {
        subpredicate = [NSPredicate predicateWithFormat:@"dosage CONTAINS[cd] %@", self.dosage.text];
        [subpredicates addObject:subpredicate];
    }
    if([self.category.text length] > 0)
    {
        subpredicate = [NSPredicate predicateWithFormat:@"classe_therapeutique CONTAINS[cd] %@", self.category.text];
        [subpredicates addObject:subpredicate];
    }
    if([self.designation.text length] > 0)
    {
        subpredicate = [NSPredicate predicateWithFormat:@"denomination CONTAINS[cd] %@", self.designation.text];
        [subpredicates addObject:subpredicate];
    }
    if(self.find_by_repayable.selected)
    {
        subpredicate = [NSPredicate predicateWithFormat:@"remboursable == 1"];
        [subpredicates addObject:subpredicate];
    }
    
    if(subpredicates.count > 0)
    {
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:subpredicates];
        
        AResult * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"aresult"];
        NSArray *drugs = [self.datasource filteredArrayUsingPredicate:predicate];
        
        if([drugs count] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                            message:SLocalizedString(@"Aucun resultat", nil)
                                                           delegate:nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            return;
        }
        
        
        if(self.turn_on_gps.isOn) {
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"prix_public"  ascending:YES];
            vc.anam = [drugs sortedArrayUsingDescriptors:@[descriptor]];
            descriptor = nil;
        }
        else {
            vc.anam = drugs;
        }
        
        [self addChildViewController:vc];
        
        self.lock = YES;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                        message:SLocalizedString(@"Champs Obligatoire", nil)
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    subpredicate = nil;
    subpredicates = nil;
}


#pragma mark - container delegate

- (UIViewController *)getRef:(UIViewController *)parent
{
    if(parent.childViewControllers.count > 0)
        return [self getRef:[parent.childViewControllers lastObject]];
    
    return parent;
}

- (void)didMoveToParentController:(ViewPagerController *)UIViewController
{
    if(_lock)
    {
        self.ref = [self getRef:self];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        transition.delegate = self;
        
        [self.ref.view.superview.layer addAnimation:transition forKey:nil];
        [self.ref.view removeFromSuperview];
        [self.ref willMoveToParentViewController:nil];
        [self.ref removeFromParentViewController];
        
        if(self.childViewControllers.count == 0)
            self.lock = NO;
        
        transition = nil;
        self.ref = nil;
    }
}

- (BOOL)didAddViewToParentController:(ViewPagerController *)UIViewController
{
    return _lock;
}

@end
