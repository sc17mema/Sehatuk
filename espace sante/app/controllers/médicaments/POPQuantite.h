//
//  POPQuantite.h
//  espace sante
//
//  Created by chbab Mohamed on 26/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "StepperView.h"
#import "MedicamentModel.h"
#import "LocalizationSystem.h"

@protocol POPQuantiteDelegate <NSObject>

- (void)setMedicamentQte:(NSInteger)qte;
- (void)cancel;

@end


@interface POPQuantite : UIViewController {
    
    __weak IBOutlet StepperView *stepper;
    __weak IBOutlet UILabel *lblDescription;
    __weak IBOutlet UIButton *btnOk;
    __weak IBOutlet UIButton *btnAnnuler;
}

@property (nonatomic) id<POPQuantiteDelegate> delegate;

- (IBAction)ajouter:(id)sender;
- (IBAction)annuler:(id)sender;

@end
