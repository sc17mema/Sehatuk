//
//  PrescriptionList.m
//  espace sante
//
//  Created by abdel ali on 1/8/15.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "PrescriptionList.h"

@implementation PrescriptionList {
    NSMutableArray *prescriptions;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
    [self loadPrescriptionsList];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"RELOAD_ORDONNANCE_TABLE" object:nil queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      prescriptions = [OrdonnancesManager sharedSingleton].ordonnances;
                                                      [tblPresciptions reloadData];
                                                  }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - general settings

- (void)initView
{
    lblCtrlName.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    lblCtrlName.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    lblCtrlName.text = SLocalizedString(@"Ordonnances", nil);
    
    [btnNewPresciption setTitle:SLocalizedString(@"Créer une nouvelle ordonnance",nil) forState:UIControlStateNormal];
    [btnNewPresciption.titleLabel setFont:[UIFont fontWithName:@"DroidSans-Bold" size:14]];
    [btnNewPresciption setTitleColor:[UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0] forState:UIControlStateNormal];
}

- (void)loadPrescriptionsList
{
    prescriptions = [OrdonnancesManager sharedSingleton].ordonnances;
   
}

#pragma mark - tableview delegate & datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return prescriptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    MCSwipeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[MCSwipeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    OrdonnanceModel *ordonance = prescriptions[indexPath.row];
    
    cell.textLabel.text = ordonance.name;
    cell.imageView.image = [UIImage imageNamed:@"medicaments.ordonnances.image.png"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"border-even.png"]];
    
    
    
    [cell setDelegate:self];
    [cell setMode:MCSwipeTableViewCellModeExit];
    [cell setFirstStateIconName:@"cross.png"
                     firstColor:[UIColor clearColor]
            secondStateIconName:nil
                    secondColor:nil
                  thirdIconName:nil
                     thirdColor:nil
                 fourthIconName:nil
                    fourthColor:nil];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.textLabel.font = [UIFont fontWithName:@"DroidSans-Bold" size:12];
    cell.textLabel.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Prescription* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"prescription"];
    [controller preSetOrdonnance:prescriptions[indexPath.row]];
    [controller setIsEditing:YES];
    [self addChildViewController:controller];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}



#pragma mark - actions

- (IBAction)newPrescriptonAction
{
    NSLog(@"New prescription tapped");
    UIViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"prescription"];
    [self addChildViewController:controller];
}



#pragma mark -
#pragma mark Swipe delegate

- (void)swipeTableViewCell:(MCSwipeTableViewCell *)cell didEndSwipingSwipingWithState:(MCSwipeTableViewCellState)state mode:(MCSwipeTableViewCellMode)mode
{
    
    NSIndexPath* indexPath = [tblPresciptions indexPathForCell:cell];
    [prescriptions removeObjectAtIndex:indexPath.row];
    OrdonnancesManager *manager = [OrdonnancesManager sharedSingleton];
    //[manager removeOrdonnance:indexPath.row];
    manager.ordonnances = prescriptions;
    [manager saveOrdonnances];
    
    [tblPresciptions reloadData];
    NSLog(@"DELETE ****");
}

@end
