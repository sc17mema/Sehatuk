//
//  DTitle.h
//  espace sante
//
//  Created by abdel ali on 11/24/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTitle : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headline;
@property (weak, nonatomic) IBOutlet UIImageView *shape;

@end
