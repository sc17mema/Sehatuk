//
//  DDetail.h
//  espace sante
//
//  Created by abdel ali on 11/24/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDetail : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *detail;

@end
