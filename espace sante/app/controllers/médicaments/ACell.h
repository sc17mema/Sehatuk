//
//  ACell.h
//  espace sante
//
//  Created by abdel ali on 11/26/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *dci;
@property (weak, nonatomic) IBOutlet UILabel *category;
@property (weak, nonatomic) IBOutlet UILabel *dosage;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *repayable;
@property (weak, nonatomic) IBOutlet UILabel *presentation;
@property (weak, nonatomic) IBOutlet UILabel *lbldci;
@property (weak, nonatomic) IBOutlet UILabel *lblcategory;
@property (weak, nonatomic) IBOutlet UILabel *lbldosage;
@property (weak, nonatomic) IBOutlet UILabel *lblpresentation;
@property (weak, nonatomic) IBOutlet UIImageView *border;

@end
