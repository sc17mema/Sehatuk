//
//  AData.m
//  espace sante
//
//  Created by abdel ali on 11/27/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "AData.h"

@implementation AData

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = NSLocalizedString(@"Informations", nil);
    
    self.tmp = @{@"nom_commercial":SLocalizedString(@"Nom commercial", nil),
                 @"denomination":SLocalizedString(@"DCI", nil),
                 @"dosage" : SLocalizedString(@"Dosage", nil),
                 @"forme": SLocalizedString(@"Forme", nil),
                 @"presentation": SLocalizedString(@"Presentation", nil),
                 @"prix_public": SLocalizedString(@"Pp remboursable", nil),
                 @"pp_remboursement" : SLocalizedString(@"Prix public", nil),
                 @"classe_therapeutique" :NSLocalizedString(@"Classe therapeutique", nil),
                 @"princeps_generique": SLocalizedString(@"Princeps generique", nil),
                 @"remboursable": SLocalizedString(@"Remboursable", nil)};
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - uitableview

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[_tmp allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont fontWithName:@"DroidSans" size:10];
    cell.textLabel.textColor = [UIColor blackColor];
    
    NSString *key = [NSString stringWithFormat:@"%@", [_tmp allKeys][indexPath.section]];
    
    if([key isEqualToString:@"remboursable"])
        cell.textLabel.text = ([[_anam valueForKey:key] boolValue]) ? SLocalizedString(@"Remboursable", nil) : SLocalizedString(@"Non Remboursable", nil);
    else
        cell.textLabel.text = [self htmlToText:[NSString stringWithFormat:@"%@", [_anam valueForKey:key]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, tableView.frame.size.width, 20)];
    [label setFont:[UIFont fontWithName:@"DroidSans-Bold" size:12]];
    [label setTextColor:[UIColor whiteColor]];
    
    NSString* value = [_tmp allValues][section];
    
    [label setText:[value capitalizedString]];
    [label setBackgroundColor:[UIColor clearColor]];
    
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0]];
    
    label = nil;
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    // Add the view to the parent view and position it if you want
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    self.view.frame = parent.view.frame;
    [[parent view] addSubview:[self view]];
}

#pragma mark - functions

- (NSString *)htmlToText:(NSString *)src
{
    src = [src stringByReplacingOccurrencesOfString:@"&amp;"  withString:@"&"];
    src = [src stringByReplacingOccurrencesOfString:@"&lt;"  withString:@"<"];
    src = [src stringByReplacingOccurrencesOfString:@"&gt;"  withString:@">"];
    src = [src stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    src = [src stringByReplacingOccurrencesOfString:@"&#039;"  withString:@"'"];
    src = [src stringByReplacingOccurrencesOfString:@"&nbsp;"  withString:@""];
    src = [src stringByReplacingOccurrencesOfString:@"&rsquo;"  withString:@"'"];
    src = [src stringByReplacingOccurrencesOfString:@"&raquo;"  withString:@"»"];
    src = [src stringByReplacingOccurrencesOfString:@"&laquo;"  withString:@"«"];
    src = [src stringByReplacingOccurrencesOfString:@"&hellip;"  withString:@"…"];
    
    return src;
}

@end
