//
//  WebViewController.m
//  sample
//
//  Created by abdel ali on 4/8/14.
//  Copyright (c) 2014 abdel ali. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.webview makeTransparentAndRemoveShadow];
    
    self.html = [self.html stringByReplacingOccurrencesOfString:@"<abc>" withString:@""];
    self.html = [self.html stringByReplacingOccurrencesOfString:@"</abc>" withString:@""];
    
    self.html = [NSString stringWithFormat:@"<p style='margin-top : 4em'>%@</p>", self.html];
    [self.webview loadHTMLString:self.html baseURL:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    // Add the view to the parent view and position it if you want
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    self.view.frame = parent.view.frame;
    
    [[parent view] addSubview:[self view]];
}

@end
