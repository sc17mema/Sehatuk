//
//  AData.h
//  espace sante
//
//  Created by abdel ali on 11/27/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalizationSystem.h"

@interface AData : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) NSDictionary *anam;
@property (strong, nonatomic) NSDictionary *tmp;

@end
