//
//  AResult.m
//  espace sante
//
//  Created by abdel ali on 11/26/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "AResult.h"

@implementation AResult

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.drugs.delegate = self;
    self.drugs.dataSource = self;
    
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = SLocalizedString(@"Resultats", nil);
    
    [Flurry logEvent:@"Resultat anam" timed:YES];
}

#pragma mark - table view delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _anam.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    static NSString *cellIdentifier = @"acell";
    
    ACell *cell = (ACell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
        cell = [[ACell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    cell.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
    cell.name.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    cell.name.text = _anam[row][@"nom_commercial"];
    
    cell.lbldci.font = [UIFont fontWithName:@"DroidSans-Bold" size:11];
    cell.lbldci.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    cell.lbldci.text = SLocalizedString(@"DCI", nil);
    cell.dci.font = [UIFont fontWithName:@"DroidSans" size:11];
    cell.dci.textColor = [UIColor colorWithRed:134/255.0 green:138/255.0 blue:139/255.0 alpha:1.0];
    cell.dci.text = _anam[row][@"denomination"];
    
    cell.lblcategory.font = [UIFont fontWithName:@"DroidSans-Bold" size:11];
    cell.lblcategory.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    cell.lblcategory.text = SLocalizedString(@"Classe", nil);
    cell.category.font = [UIFont fontWithName:@"DroidSans" size:11];
    cell.category.textColor = [UIColor colorWithRed:134/255.0 green:138/255.0 blue:139/255.0 alpha:1.0];
    cell.category.text = _anam[row][@"classe_therapeutique"];
    
    cell.lbldosage.font = [UIFont fontWithName:@"DroidSans-Bold" size:11];
    cell.lbldosage.textColor =  [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    cell.lbldosage.text = SLocalizedString(@"Dosage", nil);
    cell.dosage.font = [UIFont fontWithName:@"DroidSans" size:11];
    cell.dosage.textColor = [UIColor colorWithRed:134/255.0 green:138/255.0 blue:139/255.0 alpha:1.0];
    cell.dosage.text = _anam[row][@"dosage"];
    
    cell.lblpresentation.font = [UIFont fontWithName:@"DroidSans-Bold" size:11];
    cell.lblpresentation.textColor =  [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    cell.lblpresentation.text = NSLocalizedString(@"Presentation", nil);
    cell.presentation.font = [UIFont fontWithName:@"DroidSans" size:11];
    cell.presentation.textColor = [UIColor colorWithRed:134/255.0 green:138/255.0 blue:139/255.0 alpha:1.0];
    cell.presentation.text = _anam[row][@"presentation"];
    
    cell.price.font = [UIFont fontWithName:@"DroidSans-Bold" size:12];
    cell.price.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    if([_anam[row][@"prix_public"] integerValue] > 0)
    {
        cell.price.font = [UIFont fontWithName:@"DroidSans-Bold" size:16];
        cell.price.text = [NSString stringWithFormat:@"%.1f DH", [_anam[row][@"prix_public"] floatValue]];
    }
    else
        cell.price.text = @"disponible à l'hopital";
    
    cell.repayable.font = [UIFont fontWithName:@"DroidSans" size:10];
    cell.repayable.textColor = [UIColor colorWithRed:134/255.0 green:138/255.0 blue:139/255.0 alpha:1.0];
    cell.repayable.text = ([_anam[row][@"remboursable"] boolValue]) ? SLocalizedString(@"Remboursable", nil) : SLocalizedString(@"Non Remboursable", nil);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    ACell *c = (ACell *)cell;
    
    if(indexPath.row %2 == 0)
        c.border.image = [UIImage imageNamed:@"border-even.png"];
    else
        c.border.image = [UIImage imageNamed:@"border-odd.png"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.delegate != nil)
    {
        objMedicament = [[MedicamentModel alloc] init];
        objMedicament.nom = _anam[indexPath.row][@"nom_commercial"];
        objMedicament.prix = @([_anam[indexPath.row][@"prix_public"] floatValue]);
        objMedicament.dosage = _anam[indexPath.row][@"dosage"];
        objMedicament.presentation = _anam[indexPath.row][@"presentation"];
        
        POPQuantite *popup = [self.storyboard instantiateViewControllerWithIdentifier:@"qteMedicament"];
        popup.delegate = self;
        [self.view.window.rootViewController presentPopupViewController:popup animationType:PopupViewAnimationFade];
        
        return;
    }
    
    AData * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"adata"];
    NSDictionary *detail = self.anam[indexPath.row];
    vc.anam = detail;
    
    [self addChildViewController:vc];
}

#pragma mark - popup delegates

- (void)cancel
{
    objMedicament = nil;
    [self.view.window.rootViewController dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
}

- (void)setMedicamentQte:(NSInteger)qte
{
    objMedicament.qte = @(qte);
    [self.view.window.rootViewController dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
    [self.delegate addMedicamentObjToOrdonnance:objMedicament];
}

#pragma mark - uiviewcontrolelr container

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    // Add the view to the parent view and position it if you want
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    self.view.frame = parent.view.frame;
    [[parent view] addSubview:[self view]];
}

@end