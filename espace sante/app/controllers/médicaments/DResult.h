//
//  DResult.h
//  espace sante
//
//  Created by abdel ali on 11/22/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Resources.h"
#import "DTitle.h"
#import "DDetail.h"
#import "WebViewController.h"
#import "LocalizationSystem.h"

@interface DResult : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    __block NSMutableArray *matches;
    UITextView *reference;
}

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (nonatomic) NSInteger expandedRowIndex;
@property (strong, nonatomic) NSArray *heading;
@property (strong, nonatomic) NSArray *doctissimo;

@end
