//
//  POPQuantite.m
//  espace sante
//
//  Created by chbab Mohamed on 26/01/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "POPQuantite.h"

@interface POPQuantite ()

@end

@implementation POPQuantite

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)initSettings
{
    btnOk.titleLabel.font = [UIFont fontWithName:@"DroidSans" size:14];
    [btnOk setTitle:SLocalizedString(@"OK", nil) forState:UIControlStateNormal];
    
    btnAnnuler.titleLabel.font = [UIFont fontWithName:@"DroidSans" size:14];
    [btnAnnuler setTitle:SLocalizedString(@"Annuler", nil) forState:UIControlStateNormal];
    
    lblDescription.font = [UIFont fontWithName:@"DroidSans" size:16];
    lblDescription.textColor = [UIColor colorWithRed:0/255.0 green:85/255.0 blue:105/255.0 alpha:1.0];
    lblDescription.text = [lblDescription.text uppercaseString];
    
    [stepper setStepperRange:1 andMaxValue:10];
}

- (IBAction)ajouter:(id)sender {
    [self.delegate setMedicamentQte:[stepper getValue]];
}

- (IBAction)annuler:(id)sender {
    [self.delegate cancel];
}


@end
