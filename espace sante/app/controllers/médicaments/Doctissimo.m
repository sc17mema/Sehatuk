//
//  Doctissimo.m
//  espace sante
//
//  Created by abdel ali on 11/21/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "Doctissimo.h"

@implementation Doctissimo

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    [self init_settings];
    [self setupAutoComplete];

    [Flurry logEvent:@"Doctissimo" timed:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - settings

- (void)init_settings
{
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    self.name.text = SLocalizedString(@"Indications posologie", nil);
    
    self.designation.font = [UIFont fontWithName:@"DroidSans" size:14];
    self.designation.textColor = [UIColor blackColor];
    self.designation.placeholder = SLocalizedString(@"Nom medicament", nil);
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // Get the rects of the text field being edited and the view that we're going to scroll.
    // We convert everything to window coordinates
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    // So now we have the bounds we need to calculate the fraction between the top and bottom of the middle section for the text field's midline
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}


#pragma mark - autocomplete datasource

- (void)setupAutoComplete
{
    //NSLog(@"DOCTI");
    self.datasource = [File getDataFromFile:[Cache getPath:@"doctissimo.json"]];
    //NSLog(@"DOCTI");

    [self.designation setAutocompleteWithDataSource:self delegate:self customize:^(ACEAutocompleteInputView *inputView) {
        inputView.font = [UIFont fontWithName:@"DroidSans" size:14];
        inputView.textColor = [UIColor whiteColor];
        inputView.backgroundColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    }];
}

- (void)textField:(UITextField *)textField didSelectObject:(id)object inInputView:(ACEAutocompleteInputView *)inputView
{
    textField.text = object;
    [textField resignFirstResponder];
}

- (NSUInteger)minimumCharactersToTrigger:(ACEAutocompleteInputView *)inputView
{
    return 1;
}

- (void)inputView:(ACEAutocompleteInputView *)inputView itemsFor:(NSString *)query result:(void (^)(NSArray *items))resultBlock;
{
    if (resultBlock != nil) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            NSMutableArray *data = [NSMutableArray array];
            
            for (NSString *s in [self.datasource valueForKey:@"nom"]) {
                if ([[s lowercaseString] hasPrefix:[query lowercaseString]]) {
                    [data addObject:s];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                resultBlock(data);
            });
        });
    }
}

#pragma mark - search

- (IBAction)performSearch:(id)sender
{
    if(self.designation.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                        message:SLocalizedString(@"Champs Obligatoires", nil)
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    if(![[[self.datasource valueForKey:@"nom"] valueForKey:@"lowercaseString"] containsObject:[self.designation.text lowercaseString]])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                        message:SLocalizedString(@"Champs Obligatoires", nil)
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    int index = [[[self.datasource valueForKey:@"nom"] valueForKey:@"lowercaseString"]
                 indexOfObject:[self.designation.text lowercaseString]];
    
    identifier = [_datasource[index][@"id"] integerValue];
    
    NSLog(@"Designation: %@", self.designation.text);
    
    [Message load:self message:nil detail:nil view:nil delay:0];
    
    [[WS api] getPath:[@"doctissimo/" stringByAppendingFormat:@"%d.json", identifier]
           parameters:nil
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  NSDictionary *response = (NSDictionary *)responseObject;
                  
                  NSString *presentation = [@[[@"- sa présentation" uppercaseString],
                                              response[@"presentation"],
                                              [@"- son aspect et forme" uppercaseString],
                                              response[@"forme_aspect"],
                                              [@"- comment ça marche" uppercaseString],
                                              response[@"comment_ca_marche"],
                                              [@"- sa posologie" uppercaseString],
                                              response[@"poslogie"]] componentsJoinedByString:@"\n\n"];
                  
                  NSString *precautions = [@[[@"- ses précautions d'emploi" uppercaseString],
                                             response[@"precautions_emploi"],
                                             [@"- ses contre-indications" uppercaseString],
                                             response[@"contre_indications"],
                                             [@"- Conduite à tenir pour les conducteurs de véhicules" uppercaseString],
                                             response[@"vehicule_conducteurs"],
                                             [@"- Ses interactions" uppercaseString],
                                             response[@"interactions"],
                                             [@"- Ses incompatibilités" uppercaseString],
                                             response[@"incompatibilites"],
                                             [@"- En cas de surdosage" uppercaseString],
                                             response[@"surdosage"]] componentsJoinedByString:@"\n\n"];
                  
                  DResult * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"dresult"];
                  vc.doctissimo = @[response[@"nom"], presentation, precautions, response[@"grossesse_allaitement"], response[@"effets_indesirables"]];
                  self.ref = vc;
                  
                  [self addChildViewController:vc];
                  
                  self.lock = YES;
                  [Message hide];
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  [Message hide];
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sehatuk"
                                                                  message:SLocalizedString(@"Erreur de connexion", nil)
                                                                 delegate:nil cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                  [alert show];
                  
              }];
    
}

#pragma mark - container delegate

- (UIViewController *)getRef:(UIViewController *)parent
{
    if(parent.childViewControllers.count > 0)
        return [self getRef:[parent.childViewControllers lastObject]];
    
    return parent;
}

- (void)didMoveToParentController:(ViewPagerController *)UIViewController
{
    if(_lock)
    {
        self.ref = [self getRef:self];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        transition.delegate = self;
        
        [self.ref.view.superview.layer addAnimation:transition forKey:nil];
        [self.ref.view removeFromSuperview];
        [self.ref willMoveToParentViewController:nil];
        [self.ref removeFromParentViewController];
        
        if(self.childViewControllers.count == 0)
            self.lock = NO;
        
        self.ref = nil;
    }
}

- (BOOL)didAddViewToParentController:(ViewPagerController *)UIViewController
{
    return _lock;
}

@end
