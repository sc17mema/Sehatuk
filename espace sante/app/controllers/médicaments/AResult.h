//
//  AResult.h
//  espace sante
//
//  Created by abdel ali on 11/26/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACell.h"
#import "Resources.h"
#import "AData.h"
#import "LocalizationSystem.h"

#import "UIViewController+PopViewController.h"
#import "MedicamentModel.h"
#import "POPQuantite.h"

@protocol ordonnanceDelegate
- (void)addMedicamentObjToOrdonnance:(MedicamentModel *)obj;
@end

@interface AResult : UIViewController<UITableViewDataSource, UITableViewDelegate, POPQuantiteDelegate>
{
    MedicamentModel *objMedicament;
}

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) NSArray *anam;
@property (strong, nonatomic) IBOutlet UITableView *drugs;

@property (nonatomic) id <ordonnanceDelegate> delegate;

@end


