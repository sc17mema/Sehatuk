//
//  DResult.m
//  espace sante
//
//  Created by abdel ali on 11/22/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "DResult.h"

@implementation DResult

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.name.font = [UIFont fontWithName:@"DroidSans-Bold" size:18];
    self.name.textColor = [UIColor colorWithRed:10/255.0 green:128/255.0 blue:155/255.0 alpha:1.0];
    
    [Flurry logEvent:@"Resultat doctissimo" timed:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - table view delegate

- (void)awakeFromNib
{
    self.expandedRowIndex = -1;
    
    self.heading = @[SLocalizedString(@"Nom medicament", nil), SLocalizedString(@"Presentation", nil), SLocalizedString(@"Precaution", nil), SLocalizedString(@"Grossesse", nil), SLocalizedString(@"effets indésirable", nil)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _heading.count + (_expandedRowIndex != -1 ? 1 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    
    NSInteger index = [self indexForRow:row];
    NSString *title = _heading[index];
    
    BOOL expanded = _expandedRowIndex != -1 && _expandedRowIndex + 1 == row;
    
    if (!expanded)
    {
        DTitle *cell = [tableView dequeueReusableCellWithIdentifier:@"title"];
        
        if (!cell)
            cell = [[DTitle alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"title"];
        
        cell.headline.text = title;
        cell.headline.font = [UIFont fontWithName:@"DroidSans-Bold" size:14];
        cell.headline.textColor = [UIColor whiteColor];
        
        return cell;
    }
    else
    {
        DDetail *cell = [tableView dequeueReusableCellWithIdentifier:@"detail"];
        
        if (!cell)
            cell = [[DDetail alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"detail"];
        
        NSString *string = [NSString stringWithFormat:@"%@", _doctissimo[index]];
        
        string = [self refine:string];
        
        cell.detail.attributedText = [self attributedMessageFromMessage:string];
        reference = cell.detail;
        
        UITapGestureRecognizer *tap = nil;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iOS7:)];
        }
        else {
            tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iOS6:)];
        }
        
        [reference addGestureRecognizer:tap];
        
        CGSize expectedLabelSize = [cell.detail.text sizeWithFont:[UIFont fontWithName:@"DroidSans-Bold" size:12]
                                                constrainedToSize:CGSizeMake(280, 150)
                                                    lineBreakMode:NSLineBreakByWordWrapping];
        
        CGRect frame = cell.detail.frame;
        frame.size.width = 280;
        frame.size.height = (expectedLabelSize.height > 100) ? 120 : expectedLabelSize.height + 20;
        cell.detail.frame = frame;
        
        return cell;
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    BOOL preventReopen = NO;
    
    if([[tableView cellForRowAtIndexPath:indexPath] isKindOfClass:[DTitle class]])
    {
    
    DTitle *clickedCell = (DTitle *)[tableView cellForRowAtIndexPath:indexPath];
    
    if(_expandedRowIndex != -1)
    {
        clickedCell.shape.image = [UIImage imageNamed:@"minus.png"];
        
        DTitle *expandedCell = (DTitle *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_expandedRowIndex inSection:0]];
        expandedCell.shape.image = [UIImage imageNamed:@"plus.png"];
    }
    else
    {
        clickedCell.shape.image = [UIImage imageNamed:@"minus.png"];
    }
    
    if (row == _expandedRowIndex + 1 && _expandedRowIndex != -1)
        return nil;
    
    [tableView beginUpdates];
    
    if (_expandedRowIndex != -1)
    {
        NSInteger rowToRemove = _expandedRowIndex + 1;
        
        preventReopen = row == _expandedRowIndex;
        
        if (row > _expandedRowIndex)
            row--;
        
        _expandedRowIndex = -1;
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        
    }
    NSInteger rowToAdd = -1;
    if (!preventReopen)
    {
        rowToAdd = row + 1;
        _expandedRowIndex = row;
        [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowToAdd inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        
    }
    [tableView endUpdates];
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([cell isKindOfClass:[DTitle class]])
    {
        if(indexPath.row %2 == 0)
            cell.backgroundColor = [UIColor colorWithRed:10.0/255.0 green:128.0/255.0 blue:155.0/255.0 alpha:1.0];
        else
            cell.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:85.0/255.0 blue:105.0/255.0 alpha:1.0];
    }
    else
        cell.backgroundColor = [UIColor clearColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    if (_expandedRowIndex != -1 && row == _expandedRowIndex + 1)
    {
        DDetail *cell = (DDetail *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        
        CGSize expectedLabelSize = [cell.detail.text sizeWithFont:[UIFont fontWithName:@"DroidSans-Bold" size:12]
                                                constrainedToSize:CGSizeMake(280, 150)
                                                    lineBreakMode:NSLineBreakByWordWrapping];
        
        return (expectedLabelSize.height > 100) ? 140 : expectedLabelSize.height + 40;

    }
    
    return 30;
}

#pragma mark - helper

- (NSInteger)indexForRow:(NSInteger)row
{
    if (_expandedRowIndex != -1 && _expandedRowIndex <= row)
    {
        if (_expandedRowIndex == row)
            return row;
        else
            return row - 1;
    }
    else
        return row;
}

#pragma mark - uiviewcontrolelr container

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    // Add the view to the parent view and position it if you want
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    self.view.frame = parent.view.frame;
    [[parent view] addSubview:[self view]];
}

#pragma mark - attributed string
- (NSAttributedString *)attributedMessageFromMessage:(NSString *)message {
    
    NSArray* messageWords = [message componentsSeparatedByString:@" "];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@""];
    
    for (NSString *word in messageWords) {
        
        NSDictionary * attributes;
        
        if(word.length > 12 && [[word substringWithRange:NSMakeRange(0,12)] isEqualToString:@"voir-tableau"]){
            attributes = @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.0/255.0 green:85.0/255.0 blue:105.0/255.0 alpha:1.0],
                           NSFontAttributeName: [UIFont fontWithName:@"DroidSans-Bold" size:12],
                           NSUnderlineStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle],
                           @"type": @"hashtag",
                           @"value": word};
        } else {
            attributes = @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.0/255.0 green:85.0/255.0 blue:105.0/255.0 alpha:1.0],
                           NSFontAttributeName: [UIFont fontWithName:@"DroidSans" size:12],
                           @"type": @"string"};
        }
        
        NSAttributedString * subString = [[NSAttributedString alloc] initWithString:[word stringByAppendingString:@" "] attributes:attributes];
        [attributedMessage appendAttributedString:subString];
    }
    
    NSMutableAttributedString * attrs = [[NSMutableAttributedString alloc] initWithAttributedString:attributedMessage];
    
    NSRange range = [message rangeOfString:@"- SA POSOLOGIE"];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    range = [message rangeOfString:[@"- son aspect et forme" uppercaseString]];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    range = [message rangeOfString:[@"- sa présentation" uppercaseString]];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    range = [message rangeOfString:[@"- comment ça marche" uppercaseString]];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    range = [message rangeOfString:[@"- Conduite à tenir pour les conducteurs de véhicules" uppercaseString]];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    range = [message rangeOfString:[@"- ses contre-indications" uppercaseString]];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    range = [message rangeOfString:[@"- ses précautions d'emploi" uppercaseString]];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    range = [message rangeOfString:[@"- Ses interactions" uppercaseString]];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    range = [message rangeOfString:[@"- Ses incompatibilités" uppercaseString]];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    range = [message rangeOfString:[@"- En cas de surdosage" uppercaseString]];
    [attrs addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:range];
    [attrs addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"DroidSans-Bold" size:12] range:range];
    [attrs addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:2] range:range];
    
    return attrs;
}

#pragma mark - refine text
- (NSString *)refine:(NSString *)message {
    
    NSString *newString = message;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<abc><table[^<>]*>.+?<\\/table><\\/abc>" options:NSRegularExpressionDotMatchesLineSeparators error:nil];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:message options:0 range:NSMakeRange(0, message.length)];
    matches = [NSMutableArray arrayWithCapacity:numberOfMatches];
    
    [regex enumerateMatchesInString:message options:0 range:NSMakeRange(0, message.length)
                         usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop) {
                             
                             NSRange rangeOfMatch = [match rangeAtIndex:0];
                             NSString *substringForMatches = [message substringWithRange:rangeOfMatch];
                             [matches addObject:substringForMatches];
                         }];
    
    for (uint n = 0; n < numberOfMatches; ++n) {
        
        NSString *link = [NSString stringWithFormat:@"\n\n voir-tableau-%d\n\n ", n];
        
//        newString = [newString stringByReplacingOccurrencesOfString:[matches objectAtIndex:n] withString:link options:NSLiteralSearch range:NSMakeRange(0, [newString length])];
        
        NSTextCheckingResult *tmp = [regex firstMatchInString:newString options:0 range:NSMakeRange(0, [newString length])];
        newString = [newString stringByReplacingCharactersInRange:tmp.range withString:link];
    }
    
    return newString;
}

- (IBAction)iOS7:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:location inTextContainer:textView.textContainer
                             fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        NSDictionary *attributes = [textView.textStorage attributesAtIndex:characterIndex effectiveRange:&range];
        
        if(attributes[@"type"] && [attributes[@"type"] isEqualToString:@"hashtag"]) {
            NSInteger index = [[[attributes[@"value"] componentsSeparatedByString:@"-"] lastObject] integerValue];
            [self browser:index];
        }
    }
}

- (IBAction)iOS6:(id)sender {
    
    CGPoint pos = [sender locationInView:reference];
    
    //get location in text from textposition at point
    UITextPosition *location = [reference closestPositionToPoint:pos];
    //fetch the word at this position (or nil, if not available)
    UITextRange * range = [reference.tokenizer rangeEnclosingPosition:location
                                                     withGranularity:UITextGranularitySentence
                                                         inDirection:UITextLayoutDirectionRight];
    
    if([[reference textInRange:range] characterAtIndex:1] == '#') {
        NSInteger index = [[[[reference textInRange:range] componentsSeparatedByString:@"-"] lastObject] integerValue];
        [self browser:index];
    }
}

- (void)browser:(NSInteger )index {
    
    WebViewController *webview = [self.storyboard instantiateViewControllerWithIdentifier:@"webview"];
    webview.html = [matches objectAtIndex:index];
    
    [self addChildViewController:webview];
}

@end