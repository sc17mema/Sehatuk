//
//  ACell.m
//  espace sante
//
//  Created by abdel ali on 11/26/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "ACell.h"

@implementation ACell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
