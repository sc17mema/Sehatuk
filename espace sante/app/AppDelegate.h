//
//  AppDelegate.h
//  espace sante
//
//  Created by abdel ali on 23/09/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iRate/iRate.h>
#import <PSUpdateApp/PSUpdateApp.h>
#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import <FacebookSDK/FacebookSDK.h>
#import "SSKeychain.h"
#import "UIDevice+IdentifierAddition.h"
#import "NSString+MD5Addition.h"
#import "DownloadEngine.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
}

@property (strong, nonatomic) UIWindow *window;

@end
