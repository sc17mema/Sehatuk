//
//  WS.h
//  espace sante
//
//  Created by abdel ali on 11/22/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import "LocalizationSystem.h"

@interface WS : AFHTTPClient

+ (WS *)api;

@end
