//
//  WSDrugs.h
//  espace sante
//
//  Created by chbab Mohamed on 10/04/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFJSONRequestOperation.h>
#import "LocalizationSystem.h"

@interface WSDrugs : AFHTTPClient

+ (WSDrugs *)api;

@end
