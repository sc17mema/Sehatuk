//
//  WSDrugs.m
//  espace sante
//
//  Created by chbab Mohamed on 10/04/2015.
//  Copyright (c) 2015 abdel ali. All rights reserved.
//

#import "WSDrugs.h"

@implementation WSDrugs

+ (WSDrugs *)api
{
    static WSDrugs *api = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        //NSURL *baseURL = [NSURL URLWithString:@"http://ns29646.ovh.net:8001/sehatuk/v1/"];
        NSURL *baseURL = [NSURL URLWithString:@"http://ns29646.ovh.net:8001/"];
        api = [[WSDrugs alloc] initWithBaseURL:baseURL];
    });
    
    return api;
}

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (self) {
        
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [self setDefaultHeader:@"Authorization" value:@"Token token=\"afbadb4ff8485c0adcba486b4ca90cc4\""];
    }
    
    return self;
}

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters
{
    NSMutableURLRequest * request = [super requestWithMethod:method path:path parameters:parameters];
    
    if (self.networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable)
    {
        request.cachePolicy = NSURLRequestReturnCacheDataDontLoad;
    }
    else if(self.networkReachabilityStatus == AFNetworkReachabilityStatusUnknown)
    {
        request.cachePolicy = NSURLRequestReloadRevalidatingCacheData;
    }
    
    
    return request;
}

- (void)postPath:(NSString *)path parameters:(NSDictionary *)parameters success:(void (^)(AFHTTPRequestOperation *, id))success failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    //    if(![LocalizationGetCountry isEqualToString:@"ma"]) {
    //        if([path isEqualToString:@"centres"] || [path isEqualToString:@"medecins"]) {
    //            path = [path stringByAppendingFormat:@"/%@.json", LocalizationGetCountry];
    //        }
    //    }
    
    
    if([path isEqualToString:@"centres"] || [path isEqualToString:@"medecins"]) {
        path = [path stringByAppendingFormat:@"/%@.json", LocalizationGetCountry];
    }
    
    
    NSLog(@"PATH ::%@",path);
    [super postPath:path parameters:parameters success:success failure:failure];
}


@end
