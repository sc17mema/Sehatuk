//
//  IS.m
//  WAFAASSURANCE
//
//  Created by abdel ali on 19/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import "IS.h"

static NSString * const BaseURL = @"http://ns29646.ovh.net:8080/InfoServicesTest/";

@implementation IS

+ (IS *)api
{
    static IS *client = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[IS alloc] initWithBaseURL:[NSURL URLWithString:BaseURL]];
    });
    
    return client;
}

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (self)
    {
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [self setParameterEncoding:AFJSONParameterEncoding];
        [self setDefaultHeader:@"Accept" value:@"application/json"];
    }
    
    return self;
}

@end
