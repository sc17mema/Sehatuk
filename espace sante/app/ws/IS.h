//
//  IS.h
//  WAFAASSURANCE
//
//  Created by abdel ali on 19/04/13.
//  Copyright (c) 2013 abdel ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFJSONRequestOperation.h>

@interface IS : AFHTTPClient

+ (IS *)api;

@end
