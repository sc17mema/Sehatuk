
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ACEAutocompleteBar
#define COCOAPODS_POD_AVAILABLE_ACEAutocompleteBar
#define COCOAPODS_VERSION_MAJOR_ACEAutocompleteBar 1
#define COCOAPODS_VERSION_MINOR_ACEAutocompleteBar 0
#define COCOAPODS_VERSION_PATCH_ACEAutocompleteBar 0

// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 1
#define COCOAPODS_VERSION_MINOR_AFNetworking 3
#define COCOAPODS_VERSION_PATCH_AFNetworking 3

// ALRadial
#define COCOAPODS_POD_AVAILABLE_ALRadial
#define COCOAPODS_VERSION_MAJOR_ALRadial 0
#define COCOAPODS_VERSION_MINOR_ALRadial 1
#define COCOAPODS_VERSION_PATCH_ALRadial 0

// FTCoreText
#define COCOAPODS_POD_AVAILABLE_FTCoreText
#define COCOAPODS_VERSION_MAJOR_FTCoreText 1
#define COCOAPODS_VERSION_MINOR_FTCoreText 0
#define COCOAPODS_VERSION_PATCH_FTCoreText 0

// FlurrySDK
#define COCOAPODS_POD_AVAILABLE_FlurrySDK
#define COCOAPODS_VERSION_MAJOR_FlurrySDK 4
#define COCOAPODS_VERSION_MINOR_FlurrySDK 3
#define COCOAPODS_VERSION_PATCH_FlurrySDK 0

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 8
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 0

// MCSwipeTableViewCell
#define COCOAPODS_POD_AVAILABLE_MCSwipeTableViewCell
#define COCOAPODS_VERSION_MAJOR_MCSwipeTableViewCell 1
#define COCOAPODS_VERSION_MINOR_MCSwipeTableViewCell 1
#define COCOAPODS_VERSION_PATCH_MCSwipeTableViewCell 1

// PSUpdateApp
#define COCOAPODS_POD_AVAILABLE_PSUpdateApp
#define COCOAPODS_VERSION_MAJOR_PSUpdateApp 1
#define COCOAPODS_VERSION_MINOR_PSUpdateApp 0
#define COCOAPODS_VERSION_PATCH_PSUpdateApp 9

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

// RegexKitLite
#define COCOAPODS_POD_AVAILABLE_RegexKitLite
#define COCOAPODS_VERSION_MAJOR_RegexKitLite 4
#define COCOAPODS_VERSION_MINOR_RegexKitLite 0
#define COCOAPODS_VERSION_PATCH_RegexKitLite 0

// iRate
#define COCOAPODS_POD_AVAILABLE_iRate
#define COCOAPODS_VERSION_MAJOR_iRate 1
#define COCOAPODS_VERSION_MINOR_iRate 9
#define COCOAPODS_VERSION_PATCH_iRate 2

